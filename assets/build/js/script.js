var baseUrl = $('#base_url').val();
// window.setTimeout(function() {
//     $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
//         $(this).remove();
//     });
// }, 3000);



$(document).ready(function () {
// Filter job application by category
      $(document).on('change', '#categories_filter', function () {
         var categoryId = $(this).val();
         if(categoryId ===''){
            
            $.ajax({
                url: baseUrl + 'HomeController/getAllapplications',
                type: 'POST',
                success: function (data) {
                    $('#filter_category_div').show();
                    $('#filter_category_div').html(data);
    
                }
            });
         }else{
        
        
         $.ajax({
            url: baseUrl + 'HomeController/getFilterApplicationsList',
            type: 'POST',
            data: {categoryId: categoryId},
            success: function (data) {
                $('#filter_category_div').show();
                $('#filter_category_div').html(data);

            }
        });
    }
    $('#filter_category_div').hide();
      });
      

    //   logs filter
    $(document).on('change', '#applicant_filter', function () {
        var applicantId = $(this).val();
        if(applicantId ===''){
           
           $.ajax({
               url: baseUrl + 'LogsController/getAlllogs',
               type: 'POST',
               success: function (data) {
                   $('#filter_logs_div').show();
                   $('#filter_logs_div').html(data);
   
               }
           });
        }else{
       
       
        $.ajax({
           url: baseUrl + 'LogsController/getFilterApplicationsLogs',
           type: 'POST',
           data: {applicantId: applicantId},
           success: function (data) {
               $('#filter_logs_div').show();
               $('#filter_logs_div').html(data);

           }
       });
   }
   $('#filter_logs_div').hide();
     });

});
 