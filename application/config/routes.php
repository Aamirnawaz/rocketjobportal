<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'FrontendController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['dashboard'] = 'HomeController/dashboard';

$route['job_details'] = 'FrontendController/job_details';
$route['job_listing'] = 'FrontendController/job_listing';
$route['save_applicant'] = 'FrontendController/save_applicant';

// Admin routes
$route['admin']='HomeController/index';
$route['admin/login']='HomeController/index';


$route['about'] = 'FrontendController/aboutUs';
$route['terms-and-condition'] = 'FrontendController/termsAndCondition';
$route['privacy-policy'] = 'FrontendController/privacyPolicy';
$route['contact'] = 'FrontendController/contact';

//send url to complete application
$route['remaining-application'] = 'FrontendController/remainingApplication';
$route['save-remaining-application'] = 'FrontendController/save_remainingApplication';

$route['category-wise/(:any)']= 'FrontendController/getJobCategoryWise/$1';

$route['filter_jobs'] = 'FrontendController/filter_jobs';