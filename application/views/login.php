<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="<?echo base_url();?>assets/img/favicon.ico">
    <title>Rocket Job Portal </title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url();?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo base_url();?>assets/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url();?>assets/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
      
        <center>
       <a href="<?php echo base_url();?>"> <img height="80"  width="180"src="<?php echo base_url();?>assets/img/logo/LOGO 2-01.png" class=" logo_image"> </a>
        </center>
         <!-- <center><h4> Rocket Job portal</h4></center> -->
        <div class="animate form login_form" style="margin-top: 62px !important;">
          <section class="login_content">
            <form action="<?php echo base_url();?>HomeController/is_login" method="POST">
              <h1>Login Form</h1>
               <?php if($this->session->flashdata('flash_msg_no')){ ?>

                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <i class="fa fa-exclamation-triangle"></i> <?php echo $this->session->flashdata('flash_msg_no'); ?>
                    </div>

                <?php }?>
              <div>
                <input type="text" class="form-control" placeholder="username" name="username" required="required" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" name="password" required="required" />
              </div>
              <div>
                
                <button class="btn btn-default submit" type="submit">Log in </button>
               
              </div>

              <div class="clearfix"></div>

             
                <!--<div> 
                  <p>©2016 All Rights Reserved. Smart Tailor Shope!.<br> Privacy and Terms</p>
                </div> -->
              </div>
            </form>
          </section>
        </div>

      </div>
    </div>
  </body>
</html>

    

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

        <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/build/js/script.js"></script>