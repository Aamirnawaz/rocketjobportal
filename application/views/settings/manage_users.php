<?php $this->load->view('include/header');?>
<?php $this->load->view('include/aside');?>
<?php $this->load->view('include/top_nav');?> 
<!-- page content -->


<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><?php echo $this->title; ?></h3>
            </div>
            
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                   
                    <form class="form-horizontal form-label-left" novalidate method="post" action="<?php echo site_url()?>UserController/set_data">
                       
                            <div class="item form-group">
                                <label class="control-label text-left col-md-2 col-sm-2 col-xs-12" for="name">Full Name <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input id="name" class="form-control col-md-7 col-xs-12" name="name" required="required" type="text" placeholder="Full name">
                                </div>

                            </div>

                            <div class="item form-group">
                                <label class="control-label text-left col-md-2 col-sm-2 col-xs-12" for="name">UserName <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input id="username" class="form-control col-md-7 col-xs-12" name="username" required="required" type="text" placeholder="username">
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label text-left col-md-2 col-sm-2 col-xs-12" for="email">User Email <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input id="email" class="form-control col-md-7 col-xs-12" name="email" required="required" type="text" placeholder="Email">
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label text-left col-md-2 col-sm-2 col-xs-12" for="password">Password<span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input id="password" class="form-control col-md-7 col-xs-12" name="password" required="required" type="password" placeholder="Password">
                                </div>

                            </div>
                            <div class="item form-group">
                                <label class="control-label text-left col-md-2 col-sm-2 col-xs-12" for="user_role">User Role<span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                <select name="user_role" class="form-control" required="required">
                                <option value="">Choose Role</option>
                                <option value="operator">Operator</option>
                                </select>                                   
                                </div>

                            </div>
                           
                         
                     

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-4 col-md-offset-2">
                                    <button id="send" type="submit" class="btn btn-success">Save</button>
                                    <button type="reset" class="btn btn-primary">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                
                    <div class="x_content">
                        <?php if($this->session->flashdata('flash_msg_yes')){ ?>

                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>Congratulation ! </strong> <?php echo $this->session->flashdata('flash_msg_yes'); ?>
                            </div>

                        <?php }?>
                        <?php if($this->session->flashdata('flash_msg_no')){ ?>

                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>Error ! </strong> <?php echo $this->session->flashdata('flash_msg_no'); ?>
                            </div>

                        <?php }?>
                        <form name="del-allowance" id="del-bulk-data" action="<?php echo site_url().$this->path ?>Companies/delete_bulk/" method="post">
                            <div class="table-responsive">
                                <table id="datatable-checkbox" class="table jambo_table table-striped bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th class="column-title">SrNo#</th>
                                        <th class="column-title">Name </th>
                                        <th class="column-title">Email </th>
                                        <th class="column-title">Role </th>
                                        <th class="column-title">status </th>
                                        <th class="column-title">change status </th>
                                        <th class="column-title">Action </th>
                                        <!-- <th class="column-title no-link last"><span class="nobr">Action</span></th>
                                        <th class="bulk-actions" colspan="7">
                                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                        </th> -->
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $count = 0; foreach ($record as $row): $count++;?>
                                        <tr class=" pointer">
                                    
                                            <td class=" "><?php echo $count;?></td>
                                            <td class=" "><?php echo $row['name'];?></td>
                                            <td class=" "><?php echo $row['email'];?></td>
                                            <td class=" "><?php echo $row['user_role'];?></td>
                                            <td class=" "><?php echo $row['status'] ? '<span style="color:green">active</span>' : '<span style="color:red">Blocked</span>'?></td>
                                            <td class=" ">

                                            <?php if ($row['status']){ ?>
                                            <a href="<?php echo site_url()?>UserController/block/<?php echo $row['user_id'];?>" class="btn btn-danger btn-xs">Block</a>
                                            <?php }else{ ?>
                                                <a href="<?php echo site_url()?>UserController/unblock/<?php echo $row['user_id'];?>" class="btn btn-success btn-xs">UnBlock</a>
                                            <?php }?>
                                            
                                            </td>
                                            <td class="last">
                                                <!-- <a href="<?php //echo site_url().$this->path ?>Companies/edit/<?php //echo $row['user_id']?>"><i class="fa fa-pencil"></i></a>
                                                &nbsp; -->
                                                <a class="btn btn-primary btn-xs" href="<?php echo site_url().$this->path ?>UserController/changeUserPassword/<?php echo $row['user_id'] ?>">Change Password</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php $this->load->view('include/footer');?>