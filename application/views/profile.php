<?php $this->load->view('include/header');?>
<?php $this->load->view('include/aside');?>
<?php $this->load->view('include/top_nav');?> 
<!-- page content -->


<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Update Profile</h3>
            </div>
            
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
               
                   
                    <div class="x_content">
                        
                            <?php if($this->session->flashdata('flash_msg_yes')){ ?>

                        <div class="alert alert-success alert-dismissible " role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>Congratulation ! </strong> <?php echo $this->session->flashdata('flash_msg_yes'); ?>
                        </div>

                        <?php }?>
                        <?php if($this->session->flashdata('flash_msg_no')){ ?>

                        <div class="alert alert-danger alert-dismissible " role="alert">
                            <strong>Error ! </strong> <?php echo $this->session->flashdata('flash_msg_no'); ?>
                        </div>

                        <?php }?>
                    

                    <form class="form-horizontal form-label-left" novalidate method="post" action="<?php echo base_url(); ?>HomeController/updateProfile">
                       
                        <div class="item form-group">
                            <label class="control-label text-left col-md-2 col-sm-2 col-xs-12">New Password<span class="required">*</span></label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                            <input id="password" class="form-control col-md-7 col-xs-12" name="password" required="required" type="password">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label text-left col-md-2 col-sm-2 col-xs-12">Confirm Password</label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                            <input id="confirmPassword" class="form-control col-md-7 col-xs-12" name="confirmPassword" type="password">
                            </div>
                        </div>

                    </div>
                           
                            
                            <div class="form-group">
                                <div class="col-md-4 col-md-offset-2">
                                    <button id="send" type="submit" class="btn btn-success">Update</button>
                                    <button type="reset" class="btn btn-primary">Cancel</button>
                                </div>
                            </div>
                        </form>
                    
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php $this->load->view('include/footer');?>