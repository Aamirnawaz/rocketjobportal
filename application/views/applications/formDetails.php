<?php  $this->load->view('include/header'); ?>
<?php  $this->load->view('include/aside'); ?>
<?php  $this->load->view('include/top_nav'); ?>



<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Applicant Details</h3>
            </div>
           
        </div>
        <br>
        <br>
        <div>
        <a href="<?php echo base_url();?>ShowFormController/index/<?php echo @$record[0]['applicant_id'];?>/<?php echo @$record[0]['application_formType'];?>" class="btn btn-warning btn-md float-right">Show data inside form</a>
        </div>
        <div class="clearfix"></div>
        <div class="table-responsive">
        <table id="buttons" class="table jambo_table table-striped bulk_action">
                           <thead>
                              <tr class="headings">
                                 <th class="column-title">Application Fields </th>
                                 <th class="column-title">Details </th>           
                              </tr>
                           </thead>
                           <tbody>
              <!-- View attachment -->
                <tr>
                  <th scope="row">Passport file</th>
                  <td> 
                  <?php if(@$record[0]['passport_file']){?>
                  <a href="<?php echo base_url();?>assets/passport_files/<?php echo @$record[0]['passport_file'];?>" class="btn btn-info btn-xs">View</a>
                  <?php }?>
                </td>
                </tr>
                <!-- View attachment -->
                    <!-- View attachment -->
                    <tr>
                  <th scope="row">Visa file</th>
                  <td> 
                  <?php if(@$record[0]['visa_file']){?>
                  <a href="<?php echo base_url();?>assets/visa_files/<?php echo @$record[0]['visa_file'];?>" class="btn btn-info btn-xs">View</a>
                  <?php }?>
                  </td>
                  
                </tr>
                <!-- View attachment -->

                    <!-- View attachment -->
                    <tr>
                  <th scope="row">Proof of Address file</th>
                  <td> 
                  <?php if(@$record[0]['proof_of_address_file']){?>
                  <a href="<?php echo base_url();?>assets/proof_of_address_file/<?php echo @$record[0]['proof_of_address_file'];?>" class="btn btn-info btn-xs">View</a>
                  <?php }?>
                  </td>
                
                </tr>
                <!-- View attachment -->

                    <!-- View attachment -->
                    <tr>
                  <th scope="row">Driving Licence file</th>
                  <td> 
                  <?php if(@$record[0]['driving_license_file']){?>
                  <a href="<?php echo base_url();?>assets/driving_licence/<?php echo @$record[0]['driving_license_file'];?>" class="btn btn-info btn-xs">View</a>
                  <?php }?>
                  </td>
                </tr>
                <!-- View attachment -->

                    <!-- View attachment -->
                    <tr>
                  <th scope="row">Additional Docs file</th>
                  <td> 
                  <?php if(@$record[0]['additional_docs_file']){?>
                  <a href="<?php echo base_url();?>assets/additional_documents/<?php echo @$record[0]['additional_docs_file'];?>" class="btn btn-info btn-xs">View</a>
                  <?php }?>
                  </td>
                </tr>
                <!-- View attachment -->

                <tr>
                  <th scope="row">Title <span style="color: red;">*</span> </th>
                  <td><?php echo @$record[0]['title'];?></td>
                </tr>
                <tr>
                  <th scope="row">First Name <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['first_name'];?></td>
                </tr>

             
               

                <tr>
                  <th scope="row">1st Middle Name <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['first_middle_name'];?></td>
                </tr>
                <tr>
                  <th scope="row">2nd Middle Name <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['second_middle_name'];?></td>
                </tr>
                <tr>
                  <th scope="row">3rd Middle Name <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['third_middle_name'];?></td>
                </tr>
                <tr>
                  <th scope="row">Present Surname <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['present_sur_name'];?></td>
                </tr>
                <tr>
                  <th scope="row">Have you been known by anyother Forenames<span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['knownByOtherName'];?></td>
                </tr>
                <tr>
                  <th scope="row">have you been knwon by any other Surnames different from the birth and presend one <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['hasOtherSurName'];?></td>
                </tr>
                <tr>
                  <th scope="row">Do you have a valid Driver's Licence <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['hasValidLicence'];?></td>
                </tr>
                <tr>
                  <th scope="row">Do you have a valid Passport <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['hasValidPassport'];?></td>
                </tr>
                <tr>
                  <th scope="row">Gender <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['gender'];?></td>
                </tr>
                <tr>
                  <th scope="row">Date of Birth (MM/DD/YYYY)</th>
                  <td><?php echo @$record[0]['dob'];?></td>
                </tr>
                <tr>
                  <th scope="row">Surname at Birth <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['surname_at_birth'];?></td>
                </tr>
                <tr>
                  <th scope="row">Uses untill (format YYYY)</th>
                  <td><?php echo @$record[0]['usedUntil_surname'];?></td>
                </tr>
                <tr>
                  <th scope="row">Postcode</th>
                  <td><?php echo @$record[0]['postcode'];?></td>
                </tr>
                <tr>
                  <th scope="row">Line 1 <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['currentAddress'];?></td>
                </tr>
                <tr>
                  <th scope="row">Line 2</th>
                  <td><?php echo @$record[0]['streetName'];?></td>
                </tr>
                <tr>
                  <th scope="row">Town / City <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['town'];?></td>
                </tr>
                <tr>
                  <th scope="row">From Date</th>
                  <td><?php echo @$record[0]['fromDate'];?></td>
                </tr>
                <tr>
                  <th scope="row">To Date</th>
                  <td><?php echo @$record[0]['toDate'];?></td>
                </tr>
                <tr>
                  <th scope="row">Country <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['town'];?></td>
                </tr>
                <tr>
                  <th scope="row">First Address Postcode</th>
                  <td><?php echo @$record[0]['postcode_1'];?></td>
                </tr>
                <tr>
                  <th scope="row">First Address Line 1 <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['line_1'];?></td>
                </tr>
                <tr>
                  <th scope="row">First Address Line 2</th>
                  <td><?php echo @$record[0]['line_11'];?></td>
                </tr>
                <tr>
                  <th scope="row">First Address Town/City <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['townCity_1'];?></td>
                </tr>
                <tr>
                  <th scope="row">First Address From Date</th>
                  <td><?php echo @$record[0]['fromDate_1'];?></td>
                </tr>
                <tr>
                  <th scope="row">First Address To Date</th>
                  <td><?php echo @$record[0]['toDate_1'];?></td>
                </tr>
                <tr>
                  <th scope="row">First Living Country <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['country_1'];?></td>
                </tr>
                <tr>
                  <th scope="row">2nd Address Postcode</th>
                  <td><?php echo @$record[0]['postcode_2'];?></td>
                </tr>
                <tr>
                  <th scope="row">2nd Address Line 1 <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['line_2'];?></td>
                </tr>
                <tr>
                  <th scope="row">2nd Address Line 2</th>
                  <td><?php echo @$record[0]['line_22'];?></td>
                </tr>
                <tr>
                  <th scope="row">2nd Address Town/City <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['townCity_2'];?></td>
                </tr>
                <tr>
                  <th scope="row">2nd Address From Date</th>
                  <td><?php echo @$record[0]['fromDate_2'];?></td>
                </tr>
                <tr>
                  <th scope="row">2nd Address To Date</th>
                  <td><?php echo @$record[0]['toDate_2'];?></td>
                </tr>
                <tr>
                  <th scope="row">2nd Living Country <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['country_2'];?></td>
                </tr>
                <tr>
                  <th scope="row">3rd Address Postcode</th>
                  <td><?php echo @$record[0]['postcode_3'];?></td>
                </tr>
                <tr>
                  <th scope="row">3rd Address Line 1 <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['line_3'];?></td>
                </tr>
                <tr>
                  <th scope="row">3rd Address Line 2</th>
                  <td><?php echo @$record[0]['line_33'];?></td>
                </tr>
                <tr>
                  <th scope="row">3rd Address Town/City <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['townCity_3'];?></td>
                </tr>
                <tr>
                  <th scope="row">3rd Address From Date</th>
                  <td><?php echo @$record[0]['fromDate_3'];?></td>
                </tr>
                <tr>
                  <th scope="row">3rd Address To Date</th>
                  <td><?php echo @$record[0]['toDate_3'];?></td>
                </tr>
                <tr>
                  <th scope="row">3rd Living Country <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['country_3'];?></td>
                </tr>
                <tr>
                  <th scope="row">4th Address Postcode</th>
                  <td><?php echo @$record[0]['postcode_4'];?></td>
                </tr>
                <tr>
                  <th scope="row">4th Address Line 1 <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['line_4'];?></td>
                </tr>
                <tr>
                  <th scope="row">4th Address Line 2</th>
                  <td><?php echo @$record[0]['line_44'];?></td>
                </tr>
                <tr>
                  <th scope="row">4th Address Town/City <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['townCity_4'];?></td>
                </tr>
                <tr>
                  <th scope="row">4th Address From Date</th>
                  <td><?php echo @$record[0]['fromDate_4'];?></td>
                </tr>
                <tr>
                  <th scope="row">4th Address To Date</th>
                  <td><?php echo @$record[0]['toDate_4'];?></td>
                </tr>
                <tr>
                  <th scope="row">4th Living Country <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['country_4'];?></td>
                </tr>
                <tr>
                  <th scope="row">5th Address Postcode</th>
                  <td><?php echo @$record[0]['postcode_5'];?></td>
                </tr>
                <tr>
                  <th scope="row">5th Address Line 1 <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['line_5'];?></td>
                </tr>
                <tr>
                  <th scope="row">5th Address Line 2</th>
                  <td><?php echo @$record[0]['line_55'];?></td>
                </tr>
                <tr>
                  <th scope="row">5th Address Town/City <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['townCity_5'];?></td>
                </tr>
                <tr>
                  <th scope="row">5th Address From Date</th>
                  <td><?php echo @$record[0]['fromDate_5'];?></td>
                </tr>
                <tr>
                  <th scope="row">5th Address To Date</th>
                  <td><?php echo @$record[0]['toDate_5'];?></td>
                </tr>
                <tr>
                  <th scope="row">5th Living Country <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['country_5'];?></td>
                </tr>
                <tr>
                  <th scope="row">Birth Country</th>
                  <td><?php echo @$record[0]['birth_country'];?></td>
                </tr>
                <tr>
                  <th scope="row">Birth State</th>
                  <td><?php echo @$record[0]['birth_state'];?></td>
                </tr>
                <tr>
                  <th scope="row">Birth Town</th>
                  <td><?php echo @$record[0]['birth_town'];?></td>
                </tr>
                <tr>
                  <th scope="row">UK National Insurance Number</th>
                  <td><?php echo @$record[0]['uk_insurance_no'];?></td>
                </tr>
                <tr>
                  <th scope="row">Contact Telephone Number</th>
                  <td><?php echo @$record[0]['telephone_number'];?></td>
                </tr>
                <tr>
                  <th scope="row">Contact Mobile Number</th>
                  <td><?php echo @$record[0]['mobile_contact'];?></td>
                </tr>
                <tr>
                  <th scope="row">Passpost Number <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['passportNo'];?></td>
                </tr>
                <tr>
                  <th scope="row">Date of Birth on Passpost (MM/DD/YYYY)</th>
                  <td><?php echo @$record[0]['DOB_on_passport'];?></td>
                </tr>
                <tr>
                  <th scope="row">Passpost Nationality</th>
                  <td><?php echo @$record[0]['passport_nationality'];?></td>
                </tr>
                <tr>
                  <th scope="row">Passport Issue Date (MM/DD/YYYY)</th>
                  <td><?php echo @$record[0]['passport_issue_date'];?></td>
                </tr>
                <tr>
                  <th scope="row">Driver Licence Number <span style="color: red;">*</span></th>
                  <td><?php echo @$record[0]['DrivingLicenceNo'];?></td>
                </tr>
                <tr>
                  <th scope="row">Date of Birth on Driver Licence (MM/DD/YYYY)</th>
                  <td><?php echo @$record[0]['DOB_on_driving_licence'];?></td>
                </tr>
                <tr>
                  <th scope="row">Driver Licence Type</th>
                  <td><?php echo @$record[0]['licence_type'];?></td>
                </tr>
                <tr>
                  <th scope="row">Driver Licence Valid from (MM/DD/YYYY)</th>
                  <td><?php echo @$record[0]['licence_valid_from'];?></td>
                </tr>
                <tr>
                  <th scope="row">Driver Licence Issue Country</th>
                  <td><?php echo @$record[0]['licence_issue_country'];?></td>
                </tr>

              </tbody>
        </table>
      </div>
       
    </div>
</div>
<!-- /page content -->

<?php $this->load->view('include/footer'); ?>
