<div class="x_content">
                            <div class="table-responsive">
                                <table id="datatable-checkbox" class="table jambo_table table-striped bulk_action">
                                <thead>
                                    <tr class="headings">
                                    <th class="column-title">SrNo# </th>
                                        <th class="column-title">Applicant Name </th>
                                        <th class="column-title">Application status </th>
                                        <th class="column-title">Status Changed by</th>
                                        <th class="column-title">Time </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $counter=0; foreach ($record as $row): $counter++;
                                    $applicant_name = $this->Base_model->getAll('job_applications' ,'', 'applicant_id='.$row['applicant_id']);
                                    $user_data = $this->Base_model->getAll('user' ,'', 'user_id='.$row['status_changed_by']);
                                        ?>
                                        <tr class=" pointer">
                                            <td class=" "><?php echo $counter;?></td>
                                            <td class=" "><?php echo @$applicant_name[0]['first_name']?></td>
                                            <td class=" "><?php echo $row['application_status'];?></td>
                                            <td class=" "><?php echo @$user_data[0]['name']?></td>
                                            <td class=" "><?php echo $row['logs_time'];?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
 
                    </div>