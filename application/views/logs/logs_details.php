<?php $this->load->view('include/header');?>
<?php $this->load->view('include/aside');?>
<?php $this->load->view('include/top_nav');?> 
<!-- page content -->


<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><?php echo $this->title; ?></h3>
            </div>
            
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <!-- Categorywise filter -->
               <div class="item form-group">
                  <div class="col-sm-6">
                     <label class="control-label text-left col-sm-4" for="purchase_order_id">Filter by Applicant <span class="required"></span></label>
                     <div class="col-sm-8">
                           <select required="required" class="form-control col-md-7 col-xs-12" name="applicant_filter" id="applicant_filter">
                              <option value="">Choose Applicant</option>
                              <?php foreach($applicant_list as $list){?>
                                 <option value="<?php echo $list['applicant_id']?>"><?php echo $list['first_name']?></option>
                              <?php }?>
                           </select>
                     </div>
                  </div>

               </div>
                    <div class="x_content">
                    <div id="filter_logs_div"> 
                            <div class="table-responsive">
                                <table id="datatable-checkbox" class="table jambo_table table-striped bulk_action">
                                <thead>
                                    <tr class="headings">
                                    <th class="column-title">SrNo# </th>
                                        <th class="column-title">Applicant Name </th>
                                        <th class="column-title">Application status </th>
                                        <th class="column-title">Status Changed by</th>
                                        <th class="column-title">Time </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $counter=0; foreach ($record as $row): $counter++;
                                    $applicant_name = $this->Base_model->getAll('job_applications' ,'', 'applicant_id='.$row['applicant_id']);
                                    $user_data = $this->Base_model->getAll('user' ,'', 'user_id='.$row['status_changed_by']);
                                        ?>
                                        <tr class=" pointer">
                                            <td class=" "><?php echo $counter;?></td>
                                            <td class=" "><?php echo @$applicant_name[0]['first_name']?></td>
                                            <td class=" "><?php echo $row['application_status'];?></td>
                                            <td class=" "><?php echo @$user_data[0]['name']?></td>
                                            <td class=" "><?php echo $row['logs_time'];?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                    </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php $this->load->view('include/footer');?>