
        <!-- footer content -->
        <footer>
          <div class="pull-right">
          <a href="<?php echo base_url();?>">Rocket Job Portal </a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url();?>assets/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="<?php echo base_url();?>assets/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    
    <!-- Flot -->
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url();?>assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url();?>assets/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    
    <script src="<?php echo base_url();?>/assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
     <!-- Datatables -->
     <script src="<?php echo base_url();?>assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    <!-- validator -->
<script src="<?php echo base_url();?>/assets/vendors/validator/validator.js"></script>
   <!-- bootstrap-datetimepicker -->
   
   <!-- Own Script funcitonality -->
   <script src="<?php echo base_url();?>assets/build/js/script.js"></script>
   <!-- Own Script funcitonality -->
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/build/js/custom.min.js"></script>
   
     <!-- jQuery Smart Wizard -->
     <script src="<?php echo base_url();?>assets/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>

       <script>
    $('.timepicker').datetimepicker({
        format: 'HH:mm'
    });
    $('.datepicker').datetimepicker({
        format: 'DD-MM-YYYY'
    });
</script>



   <!-- <script type="text/javascript">
      $(document).ready(function(){

        $(".user_data").click(function(){
            
            var customer_data = $(this).attr('id');

            

            $.ajax({
              url:'<?php base_url();?>get_user_data',
              method:'POST',
              data:{customer_data :customer_data},

              success: function(data){
                var obj = JSON.parse(data);
                // console.log(obj)
                // debugger;
               
                var name = obj.userdata[0].fname;
                var address = obj.userdata[0].address;
                var armhole = obj.userdata[0].armhole;
                var contact = obj.userdata[0].contact;

                var collar = obj.userdata[0].collar;
                var biceps = obj.userdata[0].biceps;
                var back_width = obj.userdata[0].back_width;
                var chest = obj.userdata[0].chest;
                var cuff = obj.userdata[0].cuff;
                var front_width = obj.userdata[0].front_width;
                var shirt_length_back = obj.userdata[0].shirt_length_back;
                var shirt_length_front = obj.userdata[0].shirt_length_front;
                var short_sleev_length = obj.userdata[0].short_sleev_length;
                var short_sleev_opening = obj.userdata[0].short_sleev_opening;
                var shoulder = obj.userdata[0].shoulder;
                var sleev_width = obj.userdata[0].sleev_width;
                var waist = obj.userdata[0].waist;
                var seat = obj.userdata[0].seat;
                 
                $('.modal-header').html('<h4 class="modal-title" id="exampleModalLabel" align="center"><strong>Name:</strong>&nbsp;'+name+'</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title" align="center"><strong>Address :</strong>&nbsp;'+address+'</h4><h4 class="modal-title" align="center"><strong>Contact Number:</strong>&nbsp;'+contact+'</h4>');

                $('#modal-body').html('<tr> <th scope="row">1</th> <td>collar</td> <td><span class="badge">'+collar+'</span></td><td>Cuff </td><td><span class="badge">'+cuff+'</span></td></tr><tr><th scope="row">2</th><td>Chest </td><td><span class="badge">'+chest+'</span></td><td>waist</td><td><span class="badge">'+waist+'</span></td></tr><tr><th scope="row">3</th><td>Front width</td><td><span class="badge">'+front_width+'</span></td><td> Seat </td><td><span class="badge">'+seat+'</span></td></tr> <tr><th scope="row">4</th><td>sleev width</td> <td><span class="badge">'+sleev_width+'</span></td><td>Short Sleev length</td><td><span class="badge">'+short_sleev_length+'</span></td></tr><tr><th scope="row">5</th><td>Short Sleeving</td> <td><span class="badge">'+short_sleev_opening+'</span></td> <td>Biceps </td><td><span class="badge">'+biceps+'</span></td> </tr><tr> <th scope="row">6</th> <td>Armhole </td><td><span class="badge">'+armhole+'</span></td> <td>Shoulder </td><td><span class="badge">'+shoulder+'</span></td> </tr> <tr><th scope="row">7</th><td>Back width</td><td><span class="badge">'+back_width+'</span></td><td>Shirt Length(front)</td><td><span class="badge">'+shirt_length_front+'</span></td> </tr> <tr> <th scope="row">8</th> <td>Shirt Length(Back)</td> <td><span class="badge">'+shirt_length_back+'</span></td></td></tr>');
                // Display the Bootstrap modal
                $('#exampleModal').modal('show');

              }

            });

        });
      });
   </script> -->
  <script type="text/javascript">
    $(document).ready(function(){
      $(".delete_confirmation").click(function(){
             $('#deleteModel').modal('show');
      });
      });

      $(document).on('click', '#btn-del-data', function () {
        $('#del-bulk-data').submit();
    });

    $(document).on('click','#addJob',function(){
    
      $('#toggleSection').toggle();
    })
   </script>

<script type="text/javascript">
        $(document).ready(function() {
            // show the alert
            setTimeout(function() {
                $(".alert").alert('close');
            }, 5000);
        });
    </script>

  </body>
</html>