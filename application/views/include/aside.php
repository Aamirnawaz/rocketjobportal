         <!-- sidebar menu -->
         <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <!-- <h3>General</h3> -->
                <ul class="nav side-menu">
                <li><a href="<?php echo base_url();?>"><i class="fa fa-tv"></i> Frontend </a></li>
             
              

                  <li><a><i class="fa fa-file"></i> Applications <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url();?>HomeController/application_list">Application List</a></li>
                     
                    </ul>
                  </li>

                  <?php if($this->session->userdata('user_role') ==='admin'){?>  
                  <li><a><i class="fa fa-edit"></i> Job Category <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url();?>Job_Category">Categories List</a></li>
                    </ul>
                  </li>
                  <?php }?>
            
                 <li><a><i class="fa fa-briefcase"></i> Jobs <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url();?>Jobs/index">Jobs List</a></li>
                      
                    </ul>
                  </li>

                  <?php if($this->session->userdata('user_role') ==='admin'){?>  
                  <li><a><i class="fa fa-building-o"></i> Companies <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url();?>Companies/index">Companies Details</a></li>
                      
                    </ul>
                  </li>
              
                  <li><a><i class="fa fa-cog"></i> Settings <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url();?>UserController/index">Manage Users</a></li>
                      
                    </ul>
                  </li>
                  <li><a><i class="fa fa-history"></i> Logs <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url();?>LogsController/index">Logs Details</a></li>
                      
                    </ul>
                  </li>
                  <?php }?>
                </ul>
              </div>
        

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <!-- <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a> -->
            
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url();?>HomeController/logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>