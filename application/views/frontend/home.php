<main>
        <!-- slider Area Start-->
        <div class="slider-area ">
            <!-- Mobile Menu -->
            <div class="slider-active">
                <div class="single-slider slider-height d-flex align-items-center"
                    data-background="<?php echo base_url();?>assets/img/hero/h1_hero.jpg">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-6 col-lg-9 col-md-10">
                                <div class="hero__caption">
                                    <h1>Find the most exciting startup jobs</h1>
                                </div>
                            </div>
                        </div>
                        <!-- Search Box -->
                        <div class="row">
                            <div class="col-xl-8">
                                <!-- form -->
                                <form action="<?php echo base_url();?>job_listing" class="search-box" method="get">
                                    <div class="input-form">
                                        <input type="text" placeholder="Job Title or keyword" name="job_title" required>
                                    </div>
                                    <div class="select-form">
                                        <div class="select-itms" >
                                            <select id="job_location" name="job_location" >
                                                <option value="">Job Location</option>
                                                <?php foreach($jobLocations as $locations){?>
                                               
                                                <option value="<?php echo $locations['job_location'];?>"><?php echo $locations['job_location'];?></option>
                                                <?php }?>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="search-form">  
                                        <button type="submit" style="    width: 100%;  height: 70px;  background: #fb246a;   font-size: 20px;    line-height: 1;    text-align: center;
                                            color: #fff;
                                            display: block;
                                            padding: 15px;
                                            border-radius: 0px;
                                            text-transform: capitalize;
                                            font-family: Muli,sans-serif;
                                            border: none !important;
                                            letter-spacing: 0.1em;
                                            line-height: 1.2;
                                            line-height: 38px;
                                            font-size: 14px; ">
                                            Find job</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        <!-- Our Services Start -->
        <div class="our-services section-pad-t30">
            <div class="container">
                <!-- Section Tittle -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle text-center">
                            <h2>Browse Top Categories </h2>
                        </div>
                    </div>
                </div>
                <div class="row d-flex justify-contnet-center">
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                            <span><img width="50px;" src="<?php echo base_url();?>assets/img/healthcare.svg" alt=""></span>  
                            <!-- <span class="flaticon-tour"></span> -->
                            </div>
                            <div class="services-cap">
                                <h5><a href="<?php echo base_url();?>category-wise/1">Health care</a></h5>
                                <!-- <span>(653)</span> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                            <span><img width="50px;" src="<?php echo base_url();?>assets/img/cleaning-service.svg" alt=""></span>   
                            <!-- <span class="flaticon-cms"></span> -->
                            </div>
                            <div class="services-cap">
                                <h5><a href="<?php echo base_url();?>category-wise/2">Cleaning and house keeping</a></h5>
                                <!-- <span>(658)</span> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                            <span><img width="50px;" src="<?php echo base_url();?>assets/img/multidrop.svg" alt=""></span>    
                            <!-- <span class="flaticon-report"></span> -->
                            </div>
                            <div class="services-cap">
                                <h5><a href="<?php echo base_url();?>category-wise/3">Multidrop delivery drivers</a></h5>
                                <!-- <span>(658)</span> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                            <span><img width="50px;" src="<?php echo base_url();?>assets/img/construction.svg" alt=""></span>    
                            <!-- <span class="flaticon-helmet"></span> -->
                            </div>
                            <div class="services-cap">
                                <h5><a href="<?php echo base_url();?>category-wise/4">Construction</a></h5>
                                <!-- <span>(658)</span> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                                <span><img width="50px;" src="<?php echo base_url();?>assets/img/warehouse.svg" alt=""></span>
                                <!-- <span class="flaticon-real-estate"></span> -->
                            </div>
                            <div class="services-cap">
                                <h5><a href="<?php echo base_url();?>category-wise/5">Warehouse operatives</a></h5>
                                <!-- <span>(658)</span> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                            <span><img width="50px;" src="<?php echo base_url();?>assets/img/security.svg" alt=""></span>   
                            <!-- <span class="flaticon-cms"></span> -->
                            </div>
                            <div class="services-cap">
                                <h5><a href="<?php echo base_url();?>category-wise/6">Security management</a></h5>
                                <!-- <span>(658)</span> -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- More Btn -->
                <!-- Section Button -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="browse-btn2 text-center mt-50">
                            <a href="<?php echo base_url();?>job_listing" class="border-btn2">Browse All Sectors</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Our Services End -->
        <!-- How  Apply Process Start-->
        <div class="apply-process-area apply-bg pt-150 pb-150" data-background="<?php echo base_url();?>assets/img/gallery/how-applybg.png">
            <div class="container">
                <!-- Section Tittle -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle white-text text-center">
                            <span>Apply process</span>
                            <h2> How it works</h2>
                        </div>
                    </div>
                </div>
                <!-- Apply Process Caption -->
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="single-process text-center mb-30">
                            <div class="process-ion">
                            <span><img width="50px;" src="<?php echo base_url();?>assets/img/svg/search-job.svg" alt=""></span>  
                            </div>
                            <div class="process-cap">
                                <h5>1. Search a job</h5>
                                <p>1- Apply for a job online <br>
                                2- Upload all necessary documents</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-process text-center mb-30">
                            <div class="process-ion">
                            <span><img width="50px;" src="<?php echo base_url();?>assets/img/svg/apply-job.svg" alt=""></span>  
                            </div>
                            <div class="process-cap">
                                <h5>2. Apply for job <small>(4-6 Weeks)</small></h5>
                                <p>1- Complete DBS application form <br>
                                2- Wait for DBS result</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-process text-center mb-30">
                            <div class="process-ion">
                            <span><img width="50px;" src="<?php echo base_url();?>assets/img/svg/get-job.svg" alt=""></span>  
                            </div>
                            <div class="process-cap">
                                <h5>3. Get your job <small>(2 Weeks)</small></h5>
                                <p>1- All reference checks <br>
                                2- Health Declarations <br>
                                3- Job offer letter</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- How  Apply Process End-->
     
        <!-- Support Company Start-->
        <div class="support-company-area support-padding fix">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-6">
                        <div class="right-caption">
                            <!-- Section Tittle -->
                            <div class="section-tittle section-tittle2">
                            <br><br>
                                <span>What we are doing</span>
                            </div>
                            <div class="support-caption">
                                <p class="pera-top">Welcome to Rocket jobs, we are one of London’s leading Recruitment Agencies providing Temporary, Contract & Permanent Recruitment Solutions to all.</p>
                                <p>Rocket jobs is providing various jobs management services such as office cleaning services, maintenance and support services to businesses, security management, temporary part/full time jobs closely working with NHS contractors, subcontractors and suppliers all over London. We have delivered high quality services in this pandemic Covid-19 time.</p>
                                <p>We are proud to say we have delivered high quality services during this pandemic. Our services go security patrolling, nurses, carers, cleaning, maintenance services, window cleaning, washroom services. Whatever you need, simply ask your Rockets Jobs manager and it will be sorted we are there to help.</p>
                                <p>Your next opportunity is waiting to start with us, so join one of the reputable organisations in London, only just one Call / Email way.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="support-location-img">
                            <img src="<?php echo base_url();?>assets/img/service/support-img.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Support Company End-->
        <!-- Blog Area Start -->
       
      
        <!-- Blog Area End -->
        <div class="modal fade" id="cookieModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="notice d-flex justify-content-between align-items-center">
                        <div class="cookie-text">This website uses cookies to personalize content and analyse traffic in
                            order to offer you a better experience.</div>
                        <div class="buttons d-flex flex-column flex-lg-row">
                            <a href="#a" onclick="accept_cookie();" class="btn btn-danger btn-sm" data-dismiss="modal">I accept</a>
                            <a href="#a" class="btn btn-danger btn-sm" data-dismiss="modal">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    </main>

