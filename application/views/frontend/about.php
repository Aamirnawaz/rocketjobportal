<main>

        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider section-overly slider-height2 d-flex align-items-center"
                data-background="assets/img/hero/about.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>About us</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero Area End -->
        <!-- Support Company Start-->
        <div class="support-company-area fix section-padding2">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-6">
                        <div class="right-caption">
                            <!-- Section Tittle -->
                            <div class="section-tittle section-tittle2">
                                <span>What we are doing</span>
                                <!-- <h2>24k Talented people are getting Jobs</h2> -->
                            </div>
                            <div class="support-caption">
                                <p class="pera-top">
                                Welcome to Rocket jobs, we are one of London’s leading Recruitment Agencies providing Temporary, Contract & Permanent Recruitment Solutions to all.
                                <br><br>
                                Rocket jobs is providing various jobs management services such as office cleaning services, maintenance and support services to businesses, security management, temporary part/full time jobs closely working with NHS contractors, subcontractors and suppliers all over London. We have delivered high quality services in this pandemic Covid-19 time.
                                <br><br>
                                We are proud to say we have delivered high quality services during this pandemic. Our services go security patrolling, nurses, carers, cleaning, maintenance services, window cleaning, washroom services. Whatever you need, simply ask your Rockets Jobs manager and it will be sorted we are there to help.
                                </p><br>
                                <p>Your next opportunity is waiting to start with us, so join one of the reputable organisations in London, only just one Call / Email way.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="support-location-img">
                            <img src="assets/img/service/support-img.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Support Company End-->
        <!-- How  Apply Process Start-->
        <div class="apply-process-area apply-bg pt-150 pb-150" data-background="assets/img/gallery/how-applybg.png">
            <div class="container">
                <!-- Section Tittle -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle white-text text-center">
                            <span>Apply process</span>
                            <h2> How it works</h2>
                        </div>
                    </div>
                </div>
                <!-- Apply Process Caption -->
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="single-process text-center mb-30">
                            <div class="process-ion">
                                <span class="flaticon-search"></span>
                            </div>
                            <div class="process-cap">
                                <h5>1. Search a job</h5>
                                <p>1- Apply for a job online <br>
                                2- Upload all necessary documents</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-process text-center mb-30">
                            <div class="process-ion">
                                <span class="flaticon-curriculum-vitae"></span>
                            </div>
                            <div class="process-cap">
                                <h5>2. Apply for job <small>(4-6 Weeks)</small></h5>
                                <p>1- Complete DBS application form <br>
                                2- Wait for DBS result</p>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-process text-center mb-30">
                            <div class="process-ion">
                                <span class="flaticon-tour"></span>
                            </div>
                            <div class="process-cap">
                                <h5>3. Get your job <small>(2 Weeks)</small></h5>
                                <p>1- All reference checks <br>2- Health Declarations <br>3- Job offer letter</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </main>