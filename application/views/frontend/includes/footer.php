<footer>
        <!-- Footer Start-->
        <div class="footer-area footer-bg footer-padding">
            <div class="container">
                <div class="row d-flex justify-content-between">
                    <div class="col-xl-4 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-footer-caption mb-50">
                            <div class="single-footer-caption mb-30">
                                <div class="footer-tittle">
                                    <h4>About Us</h4>
                                    <div class="footer-pera">
                                        <p>Welcome to Rocket jobs, we are one of London’s leading Recruitment Agencies providing Temporary, Contract & Permanent Recruitment Solutions to all.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-5">
                        <div class="single-footer-caption mb-50">
                            <div class="footer-tittle">
                                <h4>Contact Info</h4>
                                <ul>
                                    <li>
                                        <p>Address :Ground Floor, The Bower, Stockley Park, 4 Roundwood Ave, London UB11 1AF</p>
                                    </li>
                                    <li><a href="mailto :02080883260">Phone : 02080883260</a></li>
                                    <li><a href="mailto:info@rocketjobs.co.uk">Email : info@rocketjobs.co.uk</a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-5">
                        <div class="single-footer-caption mb-50">
                            <div class="footer-tittle">
                                <h4>Important Link</h4>
                                <ul>
                                    <!-- <li><a href="#"> View Project</a></li> -->
                                    <li><a href="<?php echo base_url() ?>contact">Contact Us</a></li>
                                    <!-- <li><a href="#">Testimonial</a></li> -->
                                    <li><a href="<?php echo base_url(); ?>privacy-policy">Privacy and Policy</a></li>
                                    <li><a href="<?php echo base_url(); ?>terms-and-condition">Terms and Condition</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  -->
                <div class="row footer-wejed justify-content-between">
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                        <!-- logo -->
                        <div class="footer-logo mb-20">
                            <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/logo/logo-white.png" alt=""></a>
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>
        <!-- footer-bottom area -->
        <div class="footer-bottom-area footer-bg">
            <div class="container">
                <div class="footer-border">
                    <div class="row d-flex justify-content-between align-items-center">
                        <div class="col-xl-10 col-lg-10 ">
                            <div class="footer-copy-right">
                                <p>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;
                                    <script>document.write(new Date().getFullYear());</script> All rights reserved.
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer End-->
    </footer>

    <!-- JS here -->

    <!-- All JS Custom Plugins Link Here here -->
    <script src="<?php echo base_url();?>assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="<?php echo base_url();?>assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="<?php echo base_url();?>assets/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/slick.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/price_rangs.js"></script>

    <!-- One Page, Animated-HeadLin -->
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/animated.headline.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.magnific-popup.js"></script>

    <!-- Scrollup, nice-select, sticky -->
    <script src="<?php echo base_url();?>assets/js/jquery.scrollUp.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.nice-select.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.sticky.js"></script>

    <!-- contact js -->
    <script src="<?php echo base_url();?>assets/js/contact.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.form.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/mail-script.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.ajaxchimp.min.js"></script>

    <!-- Jquery Plugins, main Jquery -->
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            // show the alert
            setTimeout(function() {
                $(".alert").alert('close');
            }, 12000);
        });
    </script>

<script type="text/javascript">

        if(!(document.cookie)){
            $(document).ready(function () {
                $('#cookieModal').modal('show');
            });
        }

        function accept_cookie()
        {
            $.ajax({
                url: '<?php echo base_url();?>HomeController/set_user_cookie',
                cache: false,
                success: function(html){
                   
                }
            });
        }
    </script>

</body>

</html>