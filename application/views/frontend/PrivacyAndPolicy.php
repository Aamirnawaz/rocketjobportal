<main>

        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider section-overly slider-height2 d-flex align-items-center"
                data-background="assets/img/hero/about.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>Privacy and Policy</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero Area End -->
        <!-- Support Company Start-->
        <div class="support-company-area fix section-padding2">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-12 col-lg-12">
                        <div class="right-caption">
                            <div class="support-caption">
                                <div class="text-center">
                                    <h1 class="pb-4">Privacy notice for clients and applicants</h1>
                                </div>
                                <p class="pera-top">
                                Rocket jobs understands that your privacy is important to you and that you care about how your personal data is used. We respect and value the privacy of all of our Clients and applicants and we will only collect and use personal data in ways that are described here, and in a way that is consistent with our obligations and your rights under the law.
                                </p>
                                <h3>Information About Us</h3>
                                <p class="pera-top">
                                Rocket Jobs<br>
                                Main trading and registered address: Ground Floor, The Bower, Stockley Park, 4 Roundwood Ave, London UB11 1AF
                                Recruitment Manager: <br>
                                Email address: info@rocketjobs.co.uk <br>Telephone number: 02080883260

                                </p>
                                <h3>What Does This Notice Cover?</h3>
                                <p class="pera-top">
                                This Privacy Information explains how we use your personal data: how it is collected, how it is held, and how it is processed. It also explains your rights under the law relating to your personal data.
                                </p>
                                <h3>What is Personal Data?</h3>
                                <p class="pera-top">
                                Personal data is defined by the General Data Protection Regulation (EU Regulation 2016/679) (the “GDPR”) as ‘any information relating to an identifiable person who can be directly or indirectly identified in particular by reference to an identifier’.<br><br>
                                Personal data is, in simpler terms, any information about you that enables you to be identified. Personal data covers obvious information such as your name and contact details, but it also covers less obvious information such as identification numbers, electronic location data, and other online identifiers.<br><br>
                                The personal data that we use is set out in Part 5, below.

                                </p>
                                <h3>What Are My Rights?</h3>
                                <p class="pera-top">
                                Under the GDPR, you have the following rights, which we will always work to uphold:<br><br>
                                a) The right to be informed about our collection and use of your personal data. This Privacy Notice should tell you everything you need to know, but you can always contact us to find out more or to ask any questions using the details in Part 11.
                                <br><br>
                                b) The right to access the personal data we hold about you. Part 10 will tell you how to do this.
                                <br><br>
                                c) The right to have your personal data rectified if any of your personal data held by us is inaccurate or incomplete. Please contact us using the details in Part 11 to find out more. <br><br>
                                d) The right to be forgotten, i.e. the right to ask us to delete or otherwise dispose of any of your personal data that we have. Please contact us using the details in Part 11 to find out more.<br><br>
                                e) The right to restrict (i.e. prevent) the processing of your personal data.<br><br>
                                f) The right to object to us using your personal data for a particular purpose or purposes.
                                <br><br>
                                g) The right to data portability. This means that, if you have provided personal data to us directly, we are using it with your consent or for the performance of a contract. <br><br>
                                h) Rights relating to automated decision-making and profiling. We do not use your personal data in this way.<br><br>
                                For more information about our use of your personal data or exercising your rights as outlined above, please contact us using the details provided in Part 11.<br><br>
                                Further information about your rights can also be obtained from the Information Commissioner’s Office or your local Citizens Advice Bureau.<br><br>
                                If you have any cause for complaint about our use of your personal data, you have the right to lodge a complaint with the Information Commissioner’s Office.

                                </p>
                                <h3>What Personal Data Do We Collect from applicants for a criminal record check?
                                </h3>
                                <p class="pera-top">
                                We may collect some or all of the following personal data (this may vary according to your relationship with us):
                                <ul class="pp-ul">
                                    <li>Names;</li>
                                    <li>Date of birth;</li>
                                    <li>Place of birth;</li>
                                    <li>Gender;</li>
                                    <li>Address;</li>
                                    <li>Identity document detail;</li>
                                    <li>Trusted government document detail;</li>
                                    <li>Financial and social history document detail;</li>
                                    <li>Nationality;</li>
                                    <li>Email address;</li>
                                    <li>Telephone number;</li>
                                    <li>Organisation/Business name;</li>
                                    <li>Job title;</li>
                                    <li>Profession;</li>
                                    <li>Payment information via a third party (we do not store payment information);
                                    </li>
                                    <li>Personal data, or criminal offence data, may be obtained from the following
                                        third</li>
                                    <li>parties:</li>
                                    <li>The employer or recruiting organisation;</li>
                                    <li>The applicant;</li>
                                    <li>The Disclosure and Barring Service;</li>
                                    <li>Disclosure Scotland;</li>
                                    <li>Access Northern Ireland;</li>
                                </ul>
                                </p>
                                <h3>How Do You Use My Personal Data?</h3>
                                <p class="pera-top">
                                Under the GDPR, we must always have a lawful basis for processing personal data. This may be because the data is necessary for our performance of a contract with you, because you have consented to our use of your personal data, or because it is in our legitimate business interests to use it. Your personal data will be used for one or more of the following purposes:

                                <ul class="pp-ul">
                                    <li>Providing and managing your account (clients).</li>
                                    <li>Applying for a Disclosure to the relevant disclosure body (applicants).</li>
                                    <li>Applying for a Disclosure to the relevant disclosure body (applicants).
                                    </li>
                                    <li>Personalising and tailoring our services for you.</li>
                                    <li>Communicating with you. This may include responding to emails or calls from you.
                                    </li>
                                    <li>Supplying you with information by email and/or post that you have opted-in to (you may unsubscribe or opt-out of emails at any time by clicking on the email unsubscribe link).<br>
                                    With your permission, we may also use your personal data for marketing purposes, which may include contacting you by email and/or telephone and/or post with information, news, and offers on our services. You will not be sent any unlawful marketing or spam. We will always work to fully protect your rights and comply with our obligations under the GDPR and you will always have the opportunity to opt-out.
                                    </li>
                                </ul>
                                </p>
                                <p class="pera-top">
                                    <b>Conditions for processing:</b> <br><br>
                                    Personal data – to enable the company to review data entered by the data subject for accuracy. The data subject has given consent for the processing of his/her personal data. Article 6.1 of the GDPR.<br>
                                    Criminal record data – application for a criminal record check as consented to by the data subject. The data subject has given consent for the processing of his/her personal data. Article 6.1 of the GDPR.<br>
                                    Carried out only under the control of official authority. Article 10 of the GDPR.

                                </p>
                                <h3>How Long Will You Keep My Personal Data?</h3>
                                <p class="pera-top">
                                We will not keep your personal data for any longer than is necessary in light of the reason(s) for which it was first collected. Your personal or business data will therefore be kept for the following periods (or, where there is no fixed period, the following factors will be used to determine how long it is kept):

                                <ul class="pp-ul">
                                    <li>Applicant Personal Data will be retained for five years and three months from completed application for a criminal record check. This is for the purposes of renewal and for enabling the client (company who you may work for) to be able to prove that they have had criminal record checks undertaken.
                                    </li>
                                    <li>Business (company) information will be retained for seven years after our services to you have ended for financial reporting purposes.
                                    </li>
                                </ul>
                                </p>
                                <h3>How and Where Do You Store or Transfer My Personal Data?</h3>
                                <p class="pera-top">
                                We primarily store personal data in the UK. Our servers are based in London, United Kingdom.
                                </p>
                                <h3>Do You Share My Personal Data?</h3>
                                <p class="pera-top">
                                If you are applying for a criminal record check, we will share your details with the following sub-processors:<br><br>
                                DBS Criminal Record Checks – the Disclosure and Barring Service<br>
                                Disclosure Scotland Criminal Record Checks – Disclosure Scotland<br>
                                Disclosure Scotland Criminal Record Checks – Disclosure Scotland<br>
                                We do not share your personal or business data with anyone else unless in some limited circumstances, we may be legally required to share certain personal data, which might include yours if we are involved in legal proceedings or complying with legal obligations, a court order, or the instructions of a government authority.

                                </p>
                                <h3>How Can I Access My Personal Data?</h3>
                                <p class="pera-top">
                                If you want to know what personal data we have about you, you can ask us for details of that personal data and for a copy of it (where any such personal data is held). This is known as a “subject access request”.<br>
                                All subject access requests should be made in writing and sent to the email or postal addresses shown in Part 11. A form is available from us for this purpose.<br>
                                There is not normally any charge for a subject access request. If your request is ‘manifestly unfounded or excessive’ (for example, if you make repetitive requests) a fee may be charged to cover our administrative costs in responding.<br>
                                We will respond to your subject access request within thirty days. Normally, we aim to provide a complete response, including a copy of your personal data within that time. In some cases, however, particularly if your request is more complex, more time may be required up to a maximum of three months from the date we receive your request.

                                </p>
                                <h3>How Do I Contact Rocket Jobs Ltd?</h3>
                                <p class="pera-top">
                                To contact us about anything to do with your personal data and data protection, including how to make a subject access request, please use the following details (for the attention of Line manager)<br><br>
                                Email address: info@rocketjobs.co.uk<br>   
                                Telephone number: 02080883260<br>
                                Postal Address: Ground Floor, The Bower, Stockley Park, 4 Roundwood Ave, London UB11 1AF      
                                </p>

                                <h3>Changes to this Privacy Notice</h3>
                                <p class="pera-top">
                                We may change this Privacy Notice from time to time. This may be necessary, for example, if the law changes, or if we change our business in a way that affects personal data protection.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Support Company End-->



    </main>