<main>

<!-- Hero Area Start-->
<div class="slider-area ">
    <div class="single-slider section-overly slider-height2 d-flex align-items-center"
        data-background="<?php echo base_url();?>assets/img/hero/about.jpg">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="hero-cap text-center">
                        <h2>Get your job</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Hero Area End -->
<!-- Job List Area Start -->
<div class="job-listing-area pt-120 pb-120">
    <div class="container">
        <div class="row">
            <!-- Left content -->
            <div class="col-xl-3 col-lg-3 col-md-4">
                <div class="row">
                    <div class="col-12">
                        <div class="small-section-tittle2 mb-45">
                            <div class="ion"> <svg xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="12px">
                                    <path fill-rule="evenodd" fill="rgb(27, 207, 107)"
                                        d="M7.778,12.000 L12.222,12.000 L12.222,10.000 L7.778,10.000 L7.778,12.000 ZM-0.000,-0.000 L-0.000,2.000 L20.000,2.000 L20.000,-0.000 L-0.000,-0.000 ZM3.333,7.000 L16.667,7.000 L16.667,5.000 L3.333,5.000 L3.333,7.000 Z" />
                                </svg>
                            </div>
                            <h4>Filter Jobs</h4>
                        </div>
                    </div>
                </div>
                <!-- Job Category Listing start -->
                <form action="<?php echo base_url();?>filter_jobs" method="get">
                <div class="job-category-listing mb-70">
                    <!-- single one -->
                    <div class="single-listing">
                        <div class="small-section-tittle2">
                            <h4>Job Category</h4>
                        </div>
                        <!-- Select job items start -->
                        <div class="select-job-items2">
                            <select name="job_category" id="job_category">
                                <option value="">All Category</option>
                                <?php foreach($categories as $category){?>
                                    <option value="<?php echo $category['categoryId'];?>">
                                    <?php echo $category['categoryName'];?></option>
                                <?php } ?>
                            </select>
                            <span id="errorCategory" style="color:red;font-size: 12px;display:none;">* Category is required!</span>
                        </div>
               
                    </div>
                    <!-- single two -->
                    <div class="single-listing mt-80">
                        <div class="small-section-tittle2">
                            <h4>Job Location</h4>
                        </div>
                        <!-- Select job items start -->
                        <div class="select-job-items2">
                            <select class="form-control" name="job_location" id="job_location">
                            <option value="">Choose Location</option>
                            <?php foreach($jobLocations as $locations){?>
                                <option value="<?php echo $locations['job_location'];?>"><?php echo $locations['job_location'];?></option>
                            <?php }?>
                            </select>
                            <span id="errorLocation" style="color:red;font-size: 12px;display:none;">* Location is required!</span>
                        </div>
                        <!--  Select job items End-->
                        <!-- select-Categories start -->
                        <div class="select-Categories pt-80 pb-50">
                                <button id="searchCategoryBtn" style="color: #8b92dd; display: block; border: 1px solid #8b92dd; border-radius: 30px; padding: 4px 33px;text-align: center;margin-bottom: 25px;margin-left:46px;">Search</button>
                        </div>
                        <!-- select-Categories End -->
                    </div>
               
                   
                </div>
                </form>
                <!-- Job Category Listing End -->
            </div>
            <!-- Right content -->
            <div class="col-xl-9 col-lg-9 col-md-8">
                <!-- Featured_job_start -->
                <section class="featured-job-area">
                    <div class="container">
                        <!-- Count of Job list Start -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="count-job mb-35">
                                    <span><?php echo count($all_jobs);?> Jobs found</span>
                                </div>
                            </div>
                        </div>
                        <!-- Count of Job list End -->
                        <!-- single-job-content -->
                        <?php if(count($all_jobs) > 0){ ?>
                        <?php foreach ($all_jobs as $jobs) { 
                            ?>
                             <?php $categoryName=$this->Base_model->getAll('categories','',"categoryId   = ".$jobs['job_categoryId']);?>
                        <div class="single-job-items mb-30">
                            <div class="job-items">
                                <div class="company-img">
                                    
                     <a href="<?php echo base_url(); ?>job_details/?job_id=<?php echo $jobs['job_id']; ?>">
                     <img src="<?php echo base_url();?>assets/img/logo/rocketlogo.jpg" alt="" style="height: 40px; border-radius: 5px; width: 110px;">
                     </a>
                     

                                </div>
                                <div class="job-tittle job-tittle2">
                                    <a href="<?php echo base_url(); ?>job_details/?job_id=<?php echo $jobs['job_id']; ?>">
                                        <h4><?php echo $jobs['job_title']; ?></h4>
                                    </a>
                                    <ul>
                                        <li><?php echo wordwrap(@$categoryName[0]['categoryName'], 20, "<br />\n");?></li>
                                        <li><i class="fas fa-map-marker-alt"></i><?php echo $jobs['job_location']; ?></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="items-link items-link2 f-right">
                                <a href="<?php echo base_url(); ?>job_details/?job_id=<?php echo $jobs['job_id']; ?>">Apply Now</a>
                                
                            </div>
                        </div>
                        <?php } ?>
                        <?php } else { ?>
                            <div>No Job Found</div>
                        <?php }?>
                        
                    </div>
                </section>
                <!-- Featured_job_end -->
            </div>
        </div>
    </div>
</div>
<!-- Job List Area End -->


<!--Pagination Start  -->
<div class="pagination-area pb-115 text-center">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="single-wrap d-flex justify-content-center">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-start">
                            <!-- <li class="page-item active"><a class="page-link" href="#">01</a></li>
                            <li class="page-item"><a class="page-link" href="#">02</a></li>
                            <li class="page-item"><a class="page-link" href="#">03</a></li>
                            <li class="page-item"><a class="page-link" href="#"><span
                                        class="ti-angle-right"></span></a></li> -->
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Pagination End  -->

</main>


<script src="<?php echo base_url();?>assets/js/vendor/jquery-1.12.4.min.js"></script>

<script>
$(document).ready(function() {

    $("#searchCategoryBtn").click(function(){
            var job_category = $('#job_category').val();
            var job_location = $('#job_location').val();

            if(job_category==='' && job_location==='' ){
                $('#errorCategory').show();
                $('#errorLocation').show();
                return false;
            }else if(job_category && job_location===''){
                $('#errorCategory').hide();
                $('#errorLocation').show();
                return false;
            }else if(job_location && job_category===''){
                $('#errorLocation').hide();
                $('#errorCategory').show();
                return false;
            }else{
                $('#errorCategory').hide();
                $('#errorLocation').hide();
            }
    
        });

});

    

</script>