<main>

<!-- Hero Area Start-->
<div class="slider-area ">

</div>
<!-- Hero Area End -->
<!-- job post company Start -->
<div class="job-post-company pt-5 pb-120">
    <div class="container">

    
    <?php if($this->session->flashdata('success_message')){ ?>
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Congratulation ! </strong> <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php }?> 

    <?php if($this->session->flashdata('error_message')){ ?>
        <div class="alert alert-danger " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Error ! </strong> <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php }?>
        <div class="row justify-content-between">
            <!-- Left Content -->
            <div class="col-xl-7 col-lg-8">
                <!-- job single -->
                <div class="single-job-items mb-50">
                    <div class="job-items">
                        <div class="company-img company-img-details">
                            <a href="#">
                            <img src="<?php echo base_url();?>assets/img/logo/rocketlogo.jpg" alt="" style="height: 40px; border-radius: 5px; width: 110px;"></a>
                        </div>
                        <div class="job-tittle">
                            <a href="#">
                                <h4><?php echo @$jobDetail[0]['job_title']; ?></h4>
                            </a>
                            <ul>
                            <?php ?>
                                <li><?php echo @$categoryName[0]['categoryName'];?></li>
                                <li><i class="fas fa-map-marker-alt"></i><?php echo @$jobDetail[0]['job_location']; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- job single End -->

                <div class="job-post-details">
                    <div class="post-details1 mb-50">
                        <!-- Small Section Tittle -->
                        <div class="small-section-tittle">
                            <h4>Job Description</h4>
                        </div>
                        <p><?php echo @$jobDetail[0]['job_description']; ?></p>
                    </div>
                    <!-- <div class="post-details2  mb-50">
                        
                        <div class="small-section-tittle">
                            <h4>Required Knowledge, Skills, and Abilities</h4>
                        </div>
                        <ul><?php echo @$jobDetail[0]['job_requirements']; ?>
                        </ul>
                    </div> -->
                    <!-- <div class="post-details2  mb-50">
                        
                        <div class="small-section-tittle">
                            <h4>Education + Experience</h4>
                        </div>
                        <ul>Education: <?php echo @$jobDetail[0]['job_education']; ?> <br>
                        <?php echo @$jobDetail[0]['job_experience']; ?> Years of experience
                        </ul>
                    </div> -->
                </div>

            </div>
            <!-- Right Content -->
            <div class="col-xl-4 col-lg-4">
                <div class="post-details3  mb-50">
                
                    <!-- Small Section Tittle -->
                    <div class="small-section-tittle">
                        <h4>Job Overview</h4>
                    </div>
                    <ul>
                        <li>Posted date : <span><?php echo date("d M Y", strtotime(@$jobDetail[0]['posted_date'])) ?> </span></li>
                        <li>Location : <span><?php echo @$jobDetail[0]['job_location']; ?></span></li>
                        <li>Vacancy : <span><?php echo @$jobDetail[0]['no_of_vacancies'] === '0' ? 'No Vacany':@$jobDetail[0]['no_of_vacancies']; ?></span></li>
                        <li>Job nature : <span><?php echo @$jobDetail[0]['job_nature'] ==='full_time' ?'Full Time' :'Part Time'; ?></span></li>
                        <li>Salary : <span>£<?php echo @$jobDetail[0]['job_salary']; ?> P/h</span></li>
                        <li>Application date : <span><?php echo date("d F Y", strtotime(@$jobDetail[0]['application_date'])) ?></span></li>
                    </ul>
                    <div class="apply-btn2">
                        <a href="#" class="btn" data-toggle="modal" data-target="#applyModal">Apply Now</a>
                        <!-- The Modal -->
                        <form method="post" enctype="multipart/form-data" action ="<?php echo base_url(); ?>save_applicant">
                        <div class="modal" id="applyModal">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Apply Now</h4>
                                        <button type="button" class="close"
                                            data-dismiss="modal">&times;</button>
                                    </div>
                                        <?php if($this->session->flashdata('success_message')){ ?>
                                            <div class="alert alert-success" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                                </button>
                                                <strong>Congratulation ! </strong> <?php echo $this->session->flashdata('success_message'); ?>
                                            </div>
                                        <?php }?> 

                                        <?php if($this->session->flashdata('error_message')){ ?>
                                            <div class="alert alert-danger " role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                                </button>
                                                <strong>Error ! </strong> <?php echo $this->session->flashdata('error_message'); ?>
                                            </div>
                                        <?php }?>

                                    <!-- Modal body -->
                                    
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input class="form-control valid" name="first_name" required="required" type="text"
                                                        onfocus="this.placeholder = ''"
                                                        onblur="this.placeholder = 'First Name'"
                                                        placeholder="First Name">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input class="form-control valid" name="last_name" type="text"
                                                        onfocus="this.placeholder = ''"
                                                        onblur="this.placeholder = 'Last Name'"
                                                        placeholder="Last Name">
                                                </div>
                                            </div>
                                      
                                           
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <input class="form-control valid" required="required" name="applicant_email" type="email"
                                                        onfocus="this.placeholder = ''"
                                                        onblur="this.placeholder = 'Enter email address'"
                                                        placeholder="Enter email address">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <input class="form-control valid" required="required" name="applicant_contact"
                                                        type="text" onfocus="this.placeholder = ''"
                                                        onblur="this.placeholder = 'Enter Phone Number'"
                                                        placeholder="Enter phone number">
                                                </div>
                                            </div>
                                            <input type="hidden" name="job_id" value="<?php echo $_GET['job_id'];?>">
                                            <input type="hidden" name="categoryId" value="<?php echo @$categoryName[0]['categoryId'];?>">
                                            <div class="col-sm-12">
                                            
                                                <div class="form-group">
                                                <label>Upload CV</label> &nbsp;&nbsp;&nbsp;
                                                    <input required="required" name="resume_path" type="file" id="upload-cv">
                                                </div>
                                                
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group text-right">
                                                    <button type="submit" value="submit" class="btn btn-danger btn-rounded">
                                                        Send</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                



                                </div>
                            </div>
                                
                        </div>
                        </form>
                    </div>
                </div>
                <div class="post-details4  mb-50">
                    <!-- Small Section Tittle -->
                    <div class="small-section-tittle">
                        <h4>Company Information</h4>
                    </div>
                    <span><?php echo @$companyDetails[0]['company_title'];?></span>
                    <p><?php echo @$companyDetails[0]['company_desc'];?></p>
                    <ul>                    
                        <li>Name: <span><?php echo @$companyDetails[0]['company_title'];?> </span></li>
                        <li>Web : <span> <?php echo @$companyDetails[0]['company_weblink'];?></span></li>
                        <li>Email: <span><?php echo @$companyDetails[0]['company_email'];?></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- job post company End -->

</main>
