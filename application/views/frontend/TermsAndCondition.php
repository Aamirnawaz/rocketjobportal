<main>

<!-- Hero Area Start-->
<div class="slider-area ">
    <div class="single-slider section-overly slider-height2 d-flex align-items-center"
        data-background="assets/img/hero/about.jpg">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="hero-cap text-center">
                        <h2>Terms and Conditions</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Hero Area End -->
<!-- Support Company Start-->
<div class="support-company-area fix section-padding2">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-12 col-lg-12">
                <div class="right-caption">
                    <div class="support-caption">
                        <h3>1. THESE TERMS</h3>
                        <p class="pera-top">
                            <b>What these terms cover.</b>These are the terms and conditions on
                            which we supply our products to you associated with seeking a DBS check certificate and such other services as are displayed on our website from time to time. For further
                            information regarding our services, please have reference to our website
                            <a href="#" target="_blank" class="tac-links">https://rocketjobs.co.uk/ </a>(and the
                            Terms of Use which apply to its usage, accessible
                            via <a href="https://rocketjobs.co.uk/terms-and-condition" target="_blank" class="tac-links">https://rocketjobs.co.uk/terms-and-condition</a> 
                            <br><br><b>Why you should read them.</b> Please read these terms carefully
                            before you submit your order to us. These terms tell you who we are, how we will provide
                            products to you, how you and we may change or end the contract, what to do if there
                            is a problem and other important information. If you think that there is a mistake
                            in these terms, please contact us to <br><br>
                            <b>Agreement.</b> Upon submitting your order to us, you shall be
                            required to click on the button marked “I agree” so as to indicate that you accept these terms. A refusal to do so will prevent you from receiving any of our services.

                        </p>
                        <h3>2. INFORMATION ABOUT US AND HOW TO CONTACT US</h3>
                        <p class="pera-top">
                            <b>Who we are.</b> We are Rocket jobs and we are base in West London at Ground Floor, The Bower Stockley Park 4 Round wood Ave, London UB11 1AF, Telephone number: 02080883260. <br>

                            <b>Who we are not.</b> Please be aware that we are not the UK
                            Government, or
                            anybody or a department of it including the Department for Work and Pensions (DWP),
                            HMRC, the Disclosure & Barring service or the Job Centre Plus, nor do we have any
                            affiliation with the aforementioned. For the UK Government and its application
                            services, please access 
                            <a href="#" target="_blank" class="tac-links">https://www.gov.uk/.</a>
                            <br><br>
                            <b>How to contact us.</b> You can contact our customer service team via any of the methods set out on 
                            <a href="https://rocketjobs.co.uk/" target="_blank" class="tac-links">https://rocketjobs.co.uk/</a>
                            <br><br>
                            How we may contact you. If we have to contact you, we will do so by
                            telephone or by writing to you at the email address or postal address you provided
                            to us in your order. <br><br>
                            <b>"Writing" includes emails.</b> When we use the words "writing" or
                            "written" in these terms, this includes emails.
                        </p>
                        <h3>3. OUR CONTRACT WITH YOU</h3>
                        <p class="pera-top">
                            <b>How we will accept your order.</b> Our acceptance of your order will
                            take place when you have selected you agree to our “terms of service” on our website,
                            made a payment and received a notification confirmation email, at which point a
                            contract will come into existence between you and us. <br><br>
                            <b>If we cannot accept your order.</b> If we are unable to accept your
                            order, we will inform you of this and will not charge you for the services. You May still
                            be charged if the application has begun processing and does not meet the eligibility
                            requirements. This might be because of: <br><br>
                            you are under the age of 16; <br><br>
                            unexpected limits on our resources which we could not reasonably plan
                            for; <br><br>
                            because a credit reference we have obtained for you does not meet our
                            minimum requirements; <br><br>
                            because you do not meet the eligibility requirements (e.g. because you
                            are under the age of 16) or for the services (e.g. because the information that you
                            provide in the Application Form is entirely accurate and correct); <br><br>
                            because we reasonably deem that you are committing a criminal or
                            fraudulent act, causing nuisance, annoyance or an inconvenience to any third party;
                            <br><br>
                            because we have identified an error in the price or description of the
                            services; or <br><br>
                            because we are unable to meet a delivery deadline you have specified.
                            <br><br>
                            <b>Your order number.</b> We will assign an order number to your order
                            and tell you what it is when we accept your order. It will help us if you can tell us
                            the order number whenever you contact us about your order.
                            <b>We only sell to the UK.</b> Whilst we accept orders from outside of
                            the UK, our products are solely related to the UK. <br><br>
                            <b>Acting on behalf of a third party.</b>  If you are making an
                            application to
                            receive anything from us for and on behalf of a third party, we require confirmation
                            of that individual’s consent and that the individual (were s/he submitting the
                            application in their own right) is not in breach of any of the requirements of these
                            terms.
                        </p>
                        <h3>4. YOUR RIGHTS TO MAKE CHANGES</h3>
                        <p class="pera-top">
                            If you wish to make a change to anything you have ordered, please contact us. We
                            will let you know if the change is possible. If it is possible, we will let you know
                            about any changes to the price, the timing of supply or anything else which would be
                            necessary as a result of your requested change and ask you to confirm whether you
                            wish to go ahead with the change. If we cannot make the change or the consequences
                            of making the change are unacceptable to you, you may want to end the contract (see
                            clause 8- Your rights to end the contract).
                        </p>
                        <h3>5. OUR RIGHTS TO MAKE CHANGES</h3>
                        <p class="pera-top">
                            <b>Minor changes to the services.</b> We may change our products:
                            <br><br>
                            to reflect changes in relevant laws and regulatory requirements; and
                            <br><br>
                            to implement minor technical adjustments and improvements, and where
                            these changes will not affect you. <br><br>
                            <b>More significant changes to the products and these terms.</b> In
                            addition,
                            if we have to make more significant changes, we will notify you and you may then
                            contact us to end the contract before the changes take effect and receive a refund
                            for any services paid for but not received.

                        </p>
                        <h3>6. PROVIDING THE SERVICES</h3>
                        <p class="pera-top">
                            <b>When we will provide the services.</b><br><br>
                            We offer a fully online application service, available 24/7; however,
                            during the order process you will have the option to select the relevant processing
                            timescales for when we will provide the services to you should we accept your order.
                            <br><br>
                            Your identity will be checked by us prior to it being submitted to our
                            Third Party (Aaron’s department ) then to the Disclosure & Barring Service.  All
                            certificates are processed at the final stage by the Disclosure & Barring Service .
                            Thereafter, we shall assist you searching for the relevant Dbs documents you have
                            selected. <br><br>
                            The estimated completion date for the services is as told to you
                            during the order process.  Please be aware that the Fast Track Service offered on
                            our website only provides for us to expedite your application. It will enable us to
                            prioritise in checking, preparing, processing and reviewing your application. It
                            will not provide you with any expedited service by the Disclosure & Barring service.
                            <br><br>
                            We shall contact you through the process including to advise you when
                            we have begun the process, submitted an application and completed processing your
                            information. <br><br>
                            <b>We are not responsible for:</b> <br><br>
                            <b>For delays outside our control.</b> If our supply of the services is
                            delayed by an event outside our control (including the actions or omissions of you
                            or a third party) then we will contact, you as soon as possible to let you know and
                            we will take steps to minimise the effect of the delay. Provided we do this we will
                            not be liable for delays caused by the event; <br><br>
                            <b>Where you do not allow us access to provide services (where applicable).</b> If you do not allow us to perform the services as arranged (and you do not have a good reason for this) we may charge you additional costs incurred by us
                            as a result. If, despite our reasonable efforts, we are unable to contact you or
                            re-arrange performance of the services we may end the contract and clause 9.2 will
                            apply; and <br><br>
                            <b>For the ultimate decision of the Disclosure & Barring service.</b> If  the
                            Disclosure & Barring service finally determine that you are not entitled to receive
                            the requested Dbs documents, then you agree and acknowledge that this falls outside
                            of our determination and that we shall not be liable to issue you with any refund of
                            monies paid for the services to date. <br><br>
                            Rocket jobs offer a fast track service. <br><br>
                            This service offers a 3 working hours processing rate. <br><br>
                           If you have selected the fast track service this will not commence until
                            you have uploaded documents. <br><br>
                            Please note if you have uploaded documents outside of our 9am -5pm working
                            hours, Monday to Friday,  the 3 hour fast track service will not begin till the
                            working hours commence. <br><br>
                            When you have submitted your documents and there is an error with what has
                            been submitted, Rocket jobs will not take responsibility for delays outside the
                            3 hour time frame. <br><br>
                            The fast track service is for the processing of your application, You will
                            not receive your check back in this time frame.  <br><br>
                            <b>What will happen if you do not give required information to us?</b>
                            We may
                            need certain information from you so that we can supply the services to you, for
                            example, that set out in the order process. We will contact you to ask for this
                            information where it is not provided, appears incorrect or incomplete. If you do not
                            give us this information within a reasonable time of us asking for it, or if you
                            give us incomplete or incorrect information, we may either end the contract (and
                            clause 9.2 will apply) or make an additional charge of a reasonable sum to
                            compensate us for any extra work that is required as a result. We will not be
                            responsible for supplying the services late or not supplying any part of them if
                            this is caused by you not giving us the information, we need within a reasonable
                            time of us asking for it. <br><br>
                            <b>Reasons we may suspend the supply of services to you.</b> We may have
                            to
                            suspend the supply of services to: <br><br>
                            deal with technical problems or make minor technical changes;
                            update the services to reflect changes in relevant laws and regulatory
                            requirements; <br><br>
                            make changes to the services as requested by you or notified by us to
                            you (see clause 5). <br><br>
                            <b>Your rights if we suspend the supply of the services.</b> We will
                            contact
                            you in advance to tell you we will be suspending the supply of the services, unless
                            the problem is urgent or an emergency. You may contact us to end the contract for
                            the services if we suspend it, or tell you we are going to suspend it, in each case
                            for a period of more than two weeks and we will refund any sums you have paid in
                            advance for the services in respect of the period after you end the contract.
                            <br><br>
                            <b>We may also suspend supply of the services if you do not pay.</b> If
                            you do
                            not pay us for the services when you are supposed to (see clause 11.4) and you still
                            do not make payment within 7 days of us reminding you that payment is due, we may
                            suspend supply of the services until you have paid us the outstanding amounts. We
                            will contact you to tell you we are suspending supply of the services. We will not
                            suspend the services where you dispute any unpaid invoice (see clause 11.6). As well
                            as suspending the services we can also charge you interest on your overdue payments
                            (see clause 11.5). <br><br>
                            <b>Completion.</b> Our services to you will be completed: <br><br>
                            when your application has been reviewed, processed and submitted to
                            Disclosure and Barring Service. <br><br>
                            DBS certificates are created and sent to you by the Disclosure and
                            Barring Service. As we do not create or send them to you, we are not responsible for
                            any delay in your receiving them. We do not provide a follow up or chasing service.
                            If you do not receive your DBS certificate within 14 days of your check being
                            submitted, please contact Disclosure and Barring Service directly on 020280883260.
                            You may request your unique reference number or a summary certificate from our
                            Processing Team once it is available.
                        </p>
                        <h3>7. DELIVERY POLICY</h3>
                        <p class="pera-top">
                            <b>All Scottish certificates</b> will be sent via 1st class tracked post
                            within 21
                            working days of the check being completed <br><br>
                            We will hold all tracking information in regards to any certificates sent
                            directly from Rocket jobs, you may request this at any time within 3 months of
                            the check being completed. <br><br>
                            <b>All certificates for England, Wales, the channel Islands and the Isle of
                            Man,</b>  will be sent directly from our umbrella body, DBS Check Online does
                            not take
                            responsibility for any certificates not dispatched in this process. <br><br>
                            We can not guarantee a time frame for the arrival of certificates outside of
                            England, Wales, the channel Islands and the Isle of Man. <br><br>
                            Rocket jobs does not take responsibility for postage of certificates
                            outside of the England, Wales, the channel Islands and the Isle of Man. <br><br>
                            On completion of your check , A certificate will be dispatched via our
                            umbrella body. DBS check online do not take responsibility for any delays within the
                            postal service or undelivered mail. <br><br>
                            <b>All certificates</b> will be delivered after completion of checks to the
                            stated
                            delivery address at the time the order is placed. <br><br>
                            DBS Check Online reserves the right to change the delivery time frame where
                            required. <br><br>
                            <b>The delivery time frame</b> may change based on the following factors,
                            Length of
                            time of check, type of check required and queries regarding the check.

                        </p>
                        <h3>8.YOUR RIGHTS TO END THE CONTRACT </h3>
                        <p class="pera-top">
                            Ending your contract with us. You as a consumer loses the right to
                            cancel a service contract that has begun processing within the cancellation period,
                            as you agreed to this acknowledgement that you would lose your right to cancel once
                            the contract has begun processing . This is agreed when agreeing to our terms of
                            service. see our Refunds Policy, available via <a href="https://rocketjobs.co.uk/"
                            target="_blank" class="tac-links">https://rocketjobs.co.uk/</a> <br><br>
                            <b>If you want to end the contract because of something we have done
                            or have told you we are going to do,</b> see our Refunds Policy, available
                            via <a href="https://rocketjobs.co.uk/terms-of-service" target="_blank"
                            class="tac-links">https://rocketjobs.co.uk/terms-of-service</a> (Policy); <br><br>
                            <b>If you have just changed your mind,</b> you as a consumer normally
                            have no automatic right to change your mind and to cancel a contract, if this happens you
                            are in breach of contract. see our Refunds Policy, available
                            via <a href="https://rocketjobs.co.uk/terms-of-service" target="_blank"
                                class="tac-links">https://rocketjobs.co.uk/terms-of-service</a> <br><br>
                            In all other cases (if we are not at fault and there is no right to
                            change your mind), see Policy.
                        </p>
                        <h3>9. OUR RIGHTS TO END THE CONTRACT</h3>
                        <p class="pera-top">
                            <b>We may end the contract if you break it.</b> We may end the contract
                            at any time by writing to you if: <br><br>
                            you do not make any payment to us when it is due, and you still do not
                            make payment within 7 days of us reminding you that payment is due or <br><br>
                            you do not, within a reasonable time of us asking for it, provide us
                            with information that is necessary for us to provide our products. <br><br>
                            <b>You must compensate us if you break the contract.</b> If we end the
                            contract in the situations set out in clause 9.1 we will refund any money you have
                            paid in advance for any products we have not provided but we may deduct or charge
                            you a percentage of the price calculated depending on the date on which we end the
                            contract, as compensation for the net costs we will incur as a result of your
                            breaking the contract. <br><br>
                            <b>We may withdraw the products.</b> We may write to you to let you
                            know that
                            we are going to stop providing the products. We will let you know as soon as
                            reasonably practicable in advance of the supply of the products and will refund any
                            sums you have paid in advance for that which will not be provided.

                        </p>
                        <h3>10. IF THERE IS A PROBLEM</h3>
                        <p class="pera-top">
                            <b>How to tell us about problems.</b> If you have any questions or comments,
                            see the
                            Policy.
                        </p>
                        <h3>11. PRICE AND PAYMENT</h3>
                        <p class="pera-top">
                            Where to find the price for the product. The price of the products
                            (which includes VAT) will be the price indicated on the order pages when you placed
                            your order. We take all reasonable care to ensure that the prices advised to you are
                            correct. However please see clause 11.2 for what happens if we discover an error in
                            the price. <br><br>
                            <b>What happens if we got the price wrong?</b> It is always possible
                            that,
                            despite our best efforts, some of our products may be incorrectly priced. We will
                            normally check prices before accepting your order so that, where the correct price
                            at your order date is less than our stated price at your order date, we will charge
                            the lower amount. If the correct price at your order date is higher than the price
                            stated to you, we will contact you for your instructions before we accept your
                            order. If we accept and process your order where a pricing error is obvious and
                            unmistakeable and could reasonably have been recognised by you as a mispricing, we
                            may end the contract, refund you any sums you have paid. <br><br>
                            <b>When you must pay and how you must pay.</b> We accept payment via any
                            of the
                            methods displayed on the website at the time of your order. Unless otherwise agreed
                            between the parties you must pay for the services in full upon placing your order.
                            <br><br>
                            <b>We can charge interest if you pay late.</b> If you do not make any
                            payment to
                            us by the due date, we may charge interest to you on the overdue amount at the rate
                            of 8% a year above the base lending rate of the Bank of England from time to time.
                            This interest shall accrue on a daily basis from the due date until the date of
                            actual payment of the overdue amount, whether before or after judgment. You must pay
                            us interest together with any overdue amount. <br><br>
                            <b>What to do if you think an invoice is wrong.</b> If you think an
                            invoice is
                            wrong, please contact us promptly to let us know. You will not have to pay any
                            interest until the dispute is resolved. Once the dispute is resolved we will charge
                            you interest on correctly invoiced sums from the original due date.


                        </p>
                        <h3>12. OUR RESPONSIBILITY FOR LOSS OR DAMAGE SUFFERED BY YOU</h3>
                        <p class="pera-top">
                            We are responsible to you for foreseeable loss and damage caused by us.
                            If we fail to comply with these terms, we are responsible for loss or damage you
                            suffer that is a foreseeable result of our breaking this contract or our failing to
                            use reasonable care and skill, but we are not responsible for any loss or damage
                            that is not foreseeable. Loss or damage is foreseeable if either it is obvious that
                            it will happen or if, at the time the contract was made, both we and you knew it
                            might happen, for example, if you discussed it with us during the sales process.
                            <br><br>
                            <b>We do not exclude or limit in any way our liability to you where it
                            would be unlawful to do so.</b> This includes liability for death or personal
                            injury
                            caused by our negligence or the negligence of our employees, agents or
                            subcontractors; for fraud or fraudulent misrepresentation; for breach of your legal
                            rights in relation to our products. <br><br>
                            <b>We are not liable for business losses.</b> We only supply our
                            products for
                            domestic and private use. We will therefore have no liability to you for any loss of
                            profit, loss of business, business interruption, or loss of business opportunity.

                        </p>
                        <h3>13. HOW WE MAY USE YOUR PERSONAL INFORMATION</h3>
                        <p class="pera-top">
                            <b>How we may use your personal information.</b>  We will only use your
                            personal
                            information as set out in our Privacy and Cookies Policy, available via
                            <a href="https://rocketjobs.co.uk/" target="_blank"
                                class="tac-links">https://rocketjobs.co.uk/</a>
                        </p>
                        <h3>14. OTHER IMPORTANT TERMS</h3>
                        <p class="pera-top">
                            <b>We may transfer this agreement to someone else.</b> We may transfer
                            our
                            rights and obligations under these terms to another organisation. <br><br>
                            <b>Nobody else has any rights under this contract.</b> This contract is
                            between
                            you and us. No other person shall have any rights to enforce any of its terms.
                            Neither of us will need to get the agreement of any other person in order to end the
                            contract or make any changes to these terms. <br><br>
                            <b>If a court finds part of this contract illegal, the rest will
                            continue in force.</b> Each of the paragraphs of these terms operates separately. If any
                            court or
                            relevant authority decides that any of them are unlawful, the remaining paragraphs
                            will remain in full force and effect. <br><br>
                            <b>Even if we delay in enforcing this contract, we can still enforce it
                            later.</b> If we do not insist immediately that you do anything you are required
                            to do
                            under these terms, or if we delay in taking steps against you in respect of your
                            breaking this contract, that will not mean that you do not have to do those things
                            and it will not prevent us taking steps against you at a later date. For example, if
                            you miss a payment and we do not chase you, but we continue to provide the products,
                            we can still require you to make the payment at a later date. <br><br>
                            <b>Which laws apply to this contract and where you may bring legal
                                proceedings.</b> These terms are governed by English law and (subject to your
                            rights as
                            a consumer to bring proceedings elsewhere) you can bring legal proceedings in
                            respect of the products in the English courts.

                        </p>
                        <h3>15. COMPLAINTS PROCEDURE </h3>
                        <p class="pera-top">
                            <b>Complaints regarding refunds.</b> <br><br>
                            Rocket Jobs operate a three stage resolution process <br><br>
                            You must submit your complaint regarding the refund where the case will be
                            investigated by a finance administrator. A response will then be issued within 3
                            working days. <br><br>
                            If the response is not satisfactory you must request a second stage dispute,
                            this will then be passed to a senior member of finance to review. A response will be
                            issued with in 5 working days.
                            When an issued second stage dispute is not satisfactory you may request a
                            final stage dispute. <br><br>
                            You will be required to supply a full factual encounter and detailed reason
                            for dispute. <br><br>
                            Once has been completed, a new investigation will open with Rocket
                            Jobs ltd Quality and Compliance team. <br><br>
                            1They will issue a resolution/ response within 1 calendar month, Please be
                            advised this result is final.

                        </p>
                        <h3>COMPLAINTS REGARDING SERVICE.</h3>
                        <p class="pera-top">
                            Rocket Jobs operate a three stage resolution process. <br>
                            You must submit your complaint regarding the refund where the case will be
                            investigated by a Complaints handler . A response will then be issued within 3
                            working days. <br>
                           If the response is not satisfactory you must request a second stage
                            dispute, this will then be passed to a senior member of the Complaints team. A
                            response will be issued with in 5 working days. <br>
                            When  an issued second stage dispute is not satisfactory you may request a
                            final stage dispute. <br>
                            You will be required to supply a full factual encounter and detailed
                            reason for dispute. <br>
                            Once has been completed, a new investigation will open with Rocket
                            Jobs ltd Quality and Compliance team. <br>
                            They will issue a resolution/ response within 1 calendar month, Please be
                            advised this result is final. <br>
                            We require all complaints in writing, please email or post your complaint to
                            the following; <br>
                            info@rocketjobs.uk <br>
                            OR <br>
                            Rocket Jobs, <br>
                            Address: <br>
                            All correspondence regarding complaints will be handled in writing for record
                            purposes. <br>
                            Rocket Jobs follow The information Commissioners Office (ICO) guidelines in
                            regards to personal data, Please refer to the ICO guidelines for any queries.
                            <br>
                            If you feel we have violated your rights in regards to your personal data,
                            Please email the following; <br>
                            info@Rocketjobs.uk <br>
                            All Monetary complaints can be resolved through banking disputes, however only
                            when Rocket jobs policy has been upheld. <br>
                            If you require the data we hold in regards to your application, you will need
                            to request a subject access request (SAR). <br>
                            We require all Subject Access Requests in writing to the above email or
                            address.  <br>
                            You will receive an acceptance email which will contain a form. <br>
                            You are required to complete the form and return it , We will then comply.
                            <br>
                            Rocket Jobs ltd have one calendar month from receiving your completed form to
                            comply with the request.<br>
                            All requests to remove your personal data must be in writing. <br>
                            Rocket Jobs ltd will be unable to remove your data for the following reasons;
                            <br>
                            You have an open complaints case. <br>
                            There is a refund pending on your application. <br>
                            The application is under review and has not been cancelled.  <br>
                            Please note all time frames are subject to change , Rocket Jobs will
                            inform you if there is a change in the response time. <br>
                            We will only store call recordings for 3 months, any request that are received
                            outside this time we will be unable to meet.
                        </p>
                        <h3>16. FEES RELATING TO SERVICE.</h3>
                        <p class="pera-top">
                            <b>Basic DBS check</b> <br>
                            Rocket Jobs is charged £3.60 per Basic application <br>
                            Rocket jobs is charged £23.00 Inc VAT per Basic application by the
                            Disclosure and Barring service. <br>
                            Rocket jobs will add a service charge of £22.70 per application. <br>
                            An application processing fee of £20.70 will be added per application. <br>
                            VAT will be added per Basic application of £14.00 (if applicable ) <br>
                            <b>Basic DBS check (Employer)</b> <br>
                            Rocket jobs is charged £3.60 per Basic application <br>
                           Rocket jobs is charged £23.00 Inc VAT per Basic application by the
                            Disclosure and Barring service. <br>
                            Rocket jobs will add a service charge of £16.20 per application. <br>
                            An application processing fee of £22.20 will be added per application. <br>
                            VAT will be added per Basic application of £13.00. (if applicable)
                            <b>Standard DBS check</b> <br>
                            Rocket jobs is charged £3.60 Inc VAT per Standard application <br>
                            Rocket jobs is charged £23.00 Inc VAT per Standard application by the
                            Disclosure and Barring service. <br>
                            Rocket jobs will add a service charge of £26 per application. <br>
                            An application processing fee of £24.40 will be added per application. <br>
                            VAT will be added per Standard  application of £15.00. (if applicable)
                            <b>Enhanced DBS Check</b> <br>
                            Rocket jobs Online is charged £3.60 Inc VAT per Enhanced application by
                            our third party processor. <br>
                            Rocket jobs is charged £40.00 Inc VAT per Enhanced application by the
                            Disclosure and Barring service. <br>
                            Rocket jobs will add a service charge of £19.20 per application. <br>
                            An application processing fee of £22.20 will be added per application. <br>
                            VAT will be added per Enhanced application of £17.00. <br>
                            Please be advised this is a breakdown of the cost for above applications and
                            all charges are subject to change. <br>
                            This is not the only charge you may incur , as there are optional additional
                            services available.

                        </p>
                        <h3>Refunds Policy</h3>
                        <p class="pera-top">
                            This Policy should be read in conjunction with our standard Terms and Conditions of
                            service <b>(Terms)</b> and all defined terms used in this Policy shall take the
                            meanings
                            given to them in the Terms. <br> <br>
                            <b>1. YOUR RIGHTS TO END THE CONTRACT</b> <br><br>
                            Ending the contract because of something we have done or are going to
                            do. If you are ending a contract for a reason set out at clauses 1.1(a) to 1.1(e)
                            below the contract will end immediately and we will not refund you in full for any
                            products which have not been provided. The reasons are: <br><br>
                            (a)       we have told you about an upcoming change to our products or these terms
                            which you do not agree to (see clause 5.2 of the Terms); <br>
                            (b)       we have told you about an error in the price or description of the
                            products you have ordered, and you do not wish to proceed; <br>
                            (c)        there is a risk that supply of our products may be significantly delayed
                            because of events outside our control; <br>
                            (d)       we have suspended supply of the products for technical reasons, or notify
                            you we are going to suspend them for technical reasons, in each case for a period of
                            more than two weeks; or <br>
                            (e)       you have a legal right to end the contract because of something we have
                            done wrong. <br><br>
                            <b>Exercising your right to change your mind (Consumer Contracts
                                Regulations 2013).</b> For most products bought online you have a legal right to
                            change
                            your mind within 14 days.  These rights, under the Consumer Contracts Regulations
                            2013, are explained in more detail in these terms. <br><br>
                            <b>When you don't have the right to change your mind.</b> <br>
                            (a)        You do not have a right to change your mind in respect of services once
                            we have begun processing or the services have been completed, even if the
                            cancellation period is still running. <br><br>
                            <b>How long do I have to change my mind?</b>
                            Where you have purchased services, you have 14 days after the day we email you to
                            confirm we accept your order. However, once we have begun or completed the services
                            you cannot change your mind, even if the period is still running.  <br><br>
                            <b>Ending the contract where we are not at fault and there is no right
                                to
                                change your mind.</b> <br>
                            (a)       Even if we are not at fault and you do not have a right to change your
                            mind (see clause 8 of the Terms), you can still end the contract before it is
                            completed, but you may have to pay us compensation. <br>
                            (b)       The contract for services is completed when we have finished providing the
                            services and you have paid for them. If you want to end a contract before it is
                            completed where we are not at fault and you have not changed your mind, just contact
                            us to let us know. <br>
                            (c)        The contract will end immediately and we will refund any sums paid by you
                            for services not provided but we may deduct from that refund (or, if you have not
                            made an advance payment, charge you) reasonable compensation for the net costs we
                            will incur as a result of your ending the contract depending on the date on which
                            you end the contract, as compensation for the net costs we will incur as a result of
                            your doing so. <br><br>
                            <b>2. PROBLEMS – SUMMARY OF YOUR LEGAL RIGHTS</b> <br><br>
                            We are under a legal duty to supply products that are in conformity
                            with this contract. See the box below for a summary of your key legal rights in
                            relation to the products. Nothing in these terms will affect your legal rights.
                            <br><br>
                            This summary is subject to certain exceptions. For detailed information
                            please visit the Citizens Advice website <a href="www.adviceguide.org.uk"
                                target="_blank" class="tac-links">www.adviceguide.org.uk</a>. The Consumer
                            Rights
                            Act 2015 says:
                            (a)       you can ask us to repeat or fix a service if it's not carried out with
                            reasonable care and skill, or get some money back if we can't fix it;
                            (b)        if you haven't agreed a price beforehand, what you're asked to pay must
                            be reasonable; and
                            (c)       if you haven't agreed a time beforehand, it must be carried out within a
                            reasonable time.
                            See also Exercising your right to change your mind (Consumer Contracts
                            Regulations 2013) under clause 1.2 above. <br><br>
                            <b>3. TAKING ACTION TO END THE CONTRACT</b> <br><br>
                            Tell us you want to end the contract. To end the contract with us,
                            please let us know by contacting via email on info@https://rocketjobs.co.uk/ You
                            will need to include the name used on the application, email address and preferably
                            your order number.
                            Please note that all the cancellations must be in writing.
                            or where you are exercising your right to change your mind by using the Model
                            Cancellation Form set out in the Schedule to this Policy. Please provide your name,
                            home address, details of the order and, where available, your phone number and email
                            address. <br><br>
                            <b>How we will refund you.</b> We will refund you the price you paid for the
                            products (we do not refund delivery costs and sms update charges), by the method you
                            used for payment. However, we may make deductions from the price, as described
                            below. <br><br>
                            <b>Deductions from refunds if you are exercising your right to change your
                            mind.</b> If you are exercising your right to change your mind, we may deduct from any
                            refund an amount for the supply of the service for the period for which it was
                            supplied, ending with the time when you told us you had changed your mind. The
                            amount will be in proportion to what has been supplied, in comparison with the full
                            coverage of the contract.  Where termination arises through any other means
                            (including where you have breached the Terms), please be aware that no refunds will
                            be made. <br><br>
                            <b>When your refund will be made.</b> We will make any refunds due to you as
                            soon as possible. Please be aware that to compensate us for our time and the
                            administration in processing refunds, an administrative fee will be applied and
                            deducted in advance of the balance of any refund being credited to you. <br><br>
                            The refund will adhere to the same as above, however the standard refund
                            will incur a minimum administrative fee of £9.00 varied up to £50.00. The
                            administrative fee will be varied based on the level of check. <br><br>
                            For prepaid applications an admin fee may apply of £30.00 per application,
                            however this will be up to a maximum of £100.00. <br><br>
                            Please be advised if you have uploaded documents you will have void the refund
                            policy and you will not be eligible for a refund.  <br><br>
                            Rocket Jobs will not refund any SMS charges or postal costs.
                             
                            Schedule – Model Cancellation Form
                            (Complete and return this form only if you wish to withdraw from the contract) <br>
                            To: Rocket jobs<br>
                            Address <br><br>

                            I hereby give notice that I cancel my contract for the supply of DBS check Online’s
                            services, particularised as follows: <br>
                            Ordered on :     ……………………………… <br>
                            Order Number:              ……………………………… <br>
                            Name of consumer:       ……………………………… <br>
                            Address of consumer:    ……………………………… <br>
                            Signed:             ……………………………… <br>
                            Dated:              ……………………………… <br>

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Support Company End-->



</main>