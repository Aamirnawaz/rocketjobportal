<?php $this->load->view('include/header');?>
<?php $this->load->view('include/aside');?>
<?php $this->load->view('include/top_nav');?> 
<!-- page content -->


<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><?php echo $this->title; ?></h3>
            </div>
            
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                   
                    <form class="form-horizontal form-label-left" novalidate method="post" action="<?php echo site_url().$this->path;echo ($this->uri->segment(3)) ? 'Job_Category/edit_data/' : 'Job_Category/set_data/'; ?>">
                       
                            <?php
                            if ($this->uri->segment(3)){
                                ?>
                                <input type="hidden" value="<?=$this->uri->segment(3)?>" name="edit_id">
                                <?php
                            }
                            ?>
                            <div class="item form-group">
                                <label class="control-label text-left col-md-2 col-sm-2 col-xs-12" for="categoryName">Category Name <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input id="categoryName" class="form-control col-md-7 col-xs-12" name="categoryName" required="required" type="text" value="<?php echo ($this->uri->segment(3)) ? $edit_record[0]['categoryName'] : ''; ?>">
                                </div>

                            </div>
                           
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-4 col-md-offset-2">
                                    <button id="send" type="submit" class="btn btn-success">Save</button>
                                    <button type="reset" class="btn btn-primary">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="x_title">
                        <h2>
                            <a href="javascript:void(0)" type="button" class="btn btn-round btn-danger" id="btn-del-data">Delete</a>
                        </h2>
                        <!--ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>

                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <?php if($this->session->flashdata('flash_msg_yes')){ ?>

                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>Congratulation ! </strong> <?php echo $this->session->flashdata('flash_msg_yes'); ?>
                            </div>

                        <?php }?>
                        <?php if($this->session->flashdata('flash_msg_no')){ ?>

                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>Error ! </strong> <?php echo $this->session->flashdata('flash_msg_no'); ?>
                            </div>

                        <?php }?>
                        <form name="del-allowance" id="del-bulk-data" action="<?php echo site_url().$this->path ?>Job_Category/delete_bulk/" method="post">
                            <div class="table-responsive">
                                <table id="datatable-checkbox" class="table jambo_table table-striped bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th>
                                            <input type="checkbox" id="check-all" class="flat">
                                        </th>
                                        <th class="column-title">Item Name </th>
                                        <th class="column-title no-link last"><span class="nobr">Action</span></th>
                                        <th class="bulk-actions" colspan="7">
                                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($record as $row): ?>
                                        <tr class=" pointer">
                                            <td class="a-center ">
                                                <input type="checkbox" class="flat" name="table_records[]" value="<?=$row['categoryId']?>">
                                            </td>
                                            <td class=" "><?php echo $row['categoryName'];?></td>
                                            <td class="last">
                                                <a href="<?php echo site_url().$this->path ?>Job_Category/edit/<?php echo $row['categoryId'] ?>"><i class="fa fa-pencil"></i></a>
                                                &nbsp;
                                                <a href="<?php echo site_url().$this->path ?>Job_Category/delete/<?php echo $row['categoryId'] ?>"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php $this->load->view('include/footer');?>