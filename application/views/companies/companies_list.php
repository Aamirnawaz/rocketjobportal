<?php $this->load->view('include/header');?>
<?php $this->load->view('include/aside');?>
<?php $this->load->view('include/top_nav');?> 
<!-- page content -->


<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><?php echo $this->title; ?></h3>
            </div>
            
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                   
                    <form class="form-horizontal form-label-left" novalidate method="post" action="<?php echo site_url().$this->path;echo ($this->uri->segment(3)) ? 'Companies/edit_data/' : 'Companies/set_data/'; ?>">
                       
                            <?php
                            if ($this->uri->segment(3)){
                                ?>
                                <input type="hidden" value="<?=$this->uri->segment(3)?>" name="edit_id">
                                <?php
                            }
                            ?>
                            <div class="item form-group">
                                <label class="control-label text-left col-md-2 col-sm-2 col-xs-12" for="company_title">Company Name <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input id="company_title" class="form-control col-md-7 col-xs-12" name="company_title" required="required" type="text" value="<?php echo ($this->uri->segment(3)) ? $edit_record[0]['company_title'] : ''; ?>">
                                </div>

                            </div>
                            <div class="item form-group">
                                <label class="control-label text-left col-md-2 col-sm-2 col-xs-12" for="company_email">Company Email <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input id="company_email" class="form-control col-md-7 col-xs-12" name="company_email" required="required" type="text" value="<?php echo ($this->uri->segment(3)) ? $edit_record[0]['company_email'] : ''; ?>">
                                </div>

                            </div>

                            <div class="item form-group">
                                <label class="control-label text-left col-md-2 col-sm-2 col-xs-12" for="company_weblink">Web Url<span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input id="company_weblink" class="form-control col-md-7 col-xs-12" name="company_weblink" required="required" type="text" value="<?php echo ($this->uri->segment(3)) ? $edit_record[0]['company_weblink'] : ''; ?>">
                                </div>

                            </div>
                           
                            <div class="item form-group">
                    
                                    <label class="control-label text-left col-md-2 col-sm-2 col-xs-12" for="company_desc">Description </label>
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <textarea  class="form-control col-md-12 col-xs-12" required="required" name="company_desc" id="company_desc"><?php echo ($this->uri->segment(3)) ? $edit_record[0]['company_desc'] : ''; ?></textarea>
                                        <script>
                                                CKEDITOR.replace( 'company_desc' );
                                        </script>
                                    </div>
                           
                            </div>
                     

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-4 col-md-offset-2">
                                    <button id="send" type="submit" class="btn btn-success">Save</button>
                                    <button type="reset" class="btn btn-primary">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="x_title">
                        <h2>
                            <a href="javascript:void(0)" type="button" class="btn btn-round btn-danger" id="btn-del-data">Delete</a>
                        </h2>
                        <!--ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>

                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <?php if($this->session->flashdata('flash_msg_yes')){ ?>

                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>Congratulation ! </strong> <?php echo $this->session->flashdata('flash_msg_yes'); ?>
                            </div>

                        <?php }?>
                        <?php if($this->session->flashdata('flash_msg_no')){ ?>

                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>Error ! </strong> <?php echo $this->session->flashdata('flash_msg_no'); ?>
                            </div>

                        <?php }?>
                        <form name="del-allowance" id="del-bulk-data" action="<?php echo site_url().$this->path ?>Companies/delete_bulk/" method="post">
                            <div class="table-responsive">
                                <table id="datatable-checkbox" class="table jambo_table table-striped bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th>
                                            <input type="checkbox" id="check-all" class="flat">
                                        </th>
                                        <th class="column-title">Company ID </th>
                                        <th class="column-title">Company Name </th>
                                        <th class="column-title">Company Email </th>
                                        <th class="column-title">Company url </th>
                                        <th class="column-title no-link last"><span class="nobr">Action</span></th>
                                        <th class="bulk-actions" colspan="7">
                                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($record as $row): ?>
                                        <tr class=" pointer">
                                            <td class="a-center ">
                                                <input type="checkbox" class="flat" name="table_records[]" value="<?php echo $row['companyId']?>">
                                            </td>
                                            <td class=" "><?php echo $row['companyId'];?></td>
                                            <td class=" "><?php echo $row['company_title'];?></td>
                                            <td class=" "><?php echo $row['company_email'];?></td>
                                            <td class=" "><?php echo $row['company_weblink'];?></td>
                                            <td class="last">
                                                <a href="<?php echo site_url().$this->path ?>Companies/edit/<?php echo $row['companyId']?>"><i class="fa fa-pencil"></i></a>
                                                &nbsp;
                                                <a href="<?php echo site_url().$this->path ?>Companies/delete/<?php echo $row['companyId'] ?>"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php $this->load->view('include/footer');?>