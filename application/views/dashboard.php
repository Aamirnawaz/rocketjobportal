<?php $this->load->view('include/header');?>
<?php $this->load->view('include/aside');?>
<?php $this->load->view('include/top_nav');?> 
  

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row top_tiles">
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon" style="color:#76bb76"><i class="fa fa-check-square-o"></i></div>
                  <div class="count"><?php echo $this->db->from("orders")->count_all_results();?></div>
                  <h3>Total Orders</h3>
                </div>
              </div>

              <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon" style="color:skyblue"><i style="font-size:30px;" class="fa fa-hourglass"></i></div>
                  <div class="count"><?php echo $this->db->where(['order_status'=>'pending'])->from("orders")->count_all_results();?></div>
                  <h4>Pending Orders</h4>
                  
                </div>
              </div>

              <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon" style="color:orange"><i  style="font-size:30px;" class="fa fa-spinner"></i></div>
                  <div class="count"><?php echo $this->db->where(['order_status'=>'process'])->from("orders")->count_all_results();?></div>
                  <h4>In Process Orders</h4>
                  
                </div>
              </div>
              <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon" style="color:#76bb76;"><i style="font-size:30px;" class="fa fa-check-circle"></i></div>
                  <div class="count"><?php echo $this->db->where(['order_status'=>'delivered'])->from("orders")->count_all_results();?></div>
                  <h4>Deliverd Orders</h4>
                  
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon" style="color:#bb5a5a;"><i class="fa fa-ban"></i></div>
                  <div class="count"><?php echo $this->db->where(['order_status'=>'rejected'])->from("orders")->count_all_results();?></div>
                  <h3>Rejected Orders</h3>
                  
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-3">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Recent <small>Orders</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <!-- <?php
                   foreach ($recent_orders as $recents){?>
                  
                    <article class="media event">
                      <a class="pull-left date" style="background-color:#2675b9">
                        <p class="month"><?php echo date('M',strtotime($recents['order_placed_date']))?></p>
                        <p class="day"><?php echo date('d',strtotime($recents['order_placed_date']))?></p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="<?php echo base_url();?>HomeController/orders"><?php echo $recents['fname']?></a>
                        <p><?php echo $recents['address']?></p>
                      </div>
                    </article>
                   <?php }?> -->
                   
                 
                  </div>
                </div>
                
              </div>
              <div class="col-md-3">
              <div class="x_panel">
                  <div class="x_title">
                    <h2>In Process<small>Orders</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                <!--   <?php foreach ($inprocess_orders as $process){?>
                    <article class="media event">
                      <a class="pull-left date" style="background-color:#d28f14">
                        <p class="month"><?php echo date('M',strtotime($process['process_date']))?></p>
                        <p class="day"><?php echo date('d',strtotime($process['process_date']))?></p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#"><?php echo $process['fname']?></a>
                        <p><?php echo $process['address']?></p>
                      </div>
                    </article>
                  <?php }?> -->
                 
                  </div>
                </div>
              </div>

                  <div class="col-md-3">
              <div class="x_panel">
                  <div class="x_title">
                    <h2>Deliverd<small>Orders List</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
              <!--     <?php foreach ($delivered_orders as $delivered){?>
                    <article class="media event">
                      <a class="pull-left date" style="background-color:#76bb76">
                        <p class="month"><?php echo date('M',strtotime($delivered['rejected_date']))?></p>
                        <p class="day"><?php echo date('d',strtotime($delivered['rejected_date']))?></p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#"><?php echo $delivered['fname']?></a>
                        <p><?php echo $delivered['address']?></p>
                      </div>
                    </article>
                  <?php }?> -->
                  </div>
                </div>
              </div>


              <div class="col-md-3">
              <div class="x_panel">
                  <div class="x_title">
                    <h2>Rejected<small>Orders List</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                <!--   <?php foreach ($rejected_orders as $rejected){?>
                    <article class="media event">
                      <a class="pull-left date" style="background-color:#bb5a5a">
                        <p class="month"><?php echo date('M',strtotime($rejected['rejected_date']))?></p>
                        <p class="day"><?php echo date('d',strtotime($rejected['rejected_date']))?></p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#"><?php echo $rejected['fname']?></a>
                        <p><?php echo $rejected['address']?></p>
                      </div>
                    </article>
                  <?php }?> -->
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php $this->load->view('include/footer');?>