<?php $this->load->view('include/header');?>
<?php $this->load->view('include/aside');?>
<?php $this->load->view('include/top_nav');?> 
<!-- page content -->


<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><?php echo $this->title; ?></h3>
            </div>
            
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                    <?php if($this->uri->segment(3)){?>
                        <div id="toggleSection" style="display:block">  
                    <?php }else{ ?>
                        <div id="toggleSection" style="display:none">  
                    <?php }?>
                                 
                    <form enctype="multipart/form-data" class="form-horizontal form-label-left" novalidate method="post" action="<?php echo site_url().$this->path;echo ($this->uri->segment(3)) ? 'Jobs/edit_data/' : 'Jobs/set_data/'; ?>">
                       
                            <?php
                            if ($this->uri->segment(3)){
                                ?>
                                <input type="hidden" value="<?=$this->uri->segment(3)?>" name="edit_id">
                                <?php
                            }
                            ?>
                          <div class="item form-group">
                                <div class="col-sm-6">
                                    <label class="control-label text-left col-sm-4" for="purchase_order_id">Job title  <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                       <input type="text" name="job_title" required="required" placeholder="Job title" class="form-control" id="job_title" value="<?php echo ($this->uri->segment(3)) ? $edit_record[0]['job_title'] : ''; ?>">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <label class="control-label text-left col-sm-4" for="purchase_order_id">Choose Job Nature <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        <select required="required" class="form-control col-md-7 col-xs-12" required="required" name="job_nature" id="job_nature" value="<?php echo ($this->uri->segment(3)) ? $edit_record[0]['job_nature'] : ''; ?>">
                                            <option value="">Choose</option>
                                            <option value="part_time" <?php echo @$edit_record[0]['job_nature']=='part_time' ? 'selected' :''?>>Part Time</option>
                                            <option value="full_time" <?php echo @$edit_record[0]['job_nature']=='full_time' ? 'selected' :''?>>Full Time</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="item form-group">
                                <div class="col-sm-6">
                                    <label class="control-label text-left col-sm-4" for="purchase_order_id">Job Location <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                       <input type="text" name="job_location" required="required" placeholder="Job location" class="form-control" id="job_location" value="<?php echo ($this->uri->segment(3)) ? $edit_record[0]['job_location'] : ''; ?>">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <label class="control-label text-left col-sm-4" for="purchase_order_id">Job Salary per hour <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                       <input type="text" name="job_salary" required="required" placeholder="Job Salary" class="form-control" id="job_salary" value="<?php echo ($this->uri->segment(3)) ? $edit_record[0]['job_salary'] : ''; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="item form-group">
                                <div class="col-sm-6">
                                    <label class="control-label text-left col-sm-4" for="purchase_order_id">No of Vacancies <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                       <input type="text" name="no_of_vacancies" required="required" placeholder="No of Vacancies" class="form-control" id="no_of_vacancies" value="<?php echo ($this->uri->segment(3)) ? $edit_record[0]['no_of_vacancies'] : ''; ?>">
                                    </div>
                                </div>

                              
                            </div>

                            <div class="item form-group">
                                <div class="col-sm-6">
                          
                                <label class="control-label text-left col-sm-4" for="purchase_order_id">Application Date <span class="required">*</span></label>
                                <div class="col-sm-8">
                                    <div class="input-group date datepicker" id="datepicker">
                                        <input type="text" class="form-control" name="application_date" value="<?php echo ($this->uri->segment(3)) ? $edit_record[0]['application_date'] : ''; ?>">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>                     
                                </div>

                                <div class="col-sm-6">
                                    <label class="control-label text-left col-sm-4" for="purchase_order_id">Choose Category <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        <select required="required" class="form-control col-md-7 col-xs-12" name="job_categoryId" id="job_categoryId">
                                            <option value="">Job Category</option>
                                            <?php foreach($categories as $category){?>
                                            <option value="<?php echo $category['categoryId'];?>" <?php echo $category['categoryId'] == @$edit_record[0]['job_categoryId'] ? 'selected':''?>><?php echo $category['categoryName'];?></option>
                                            <?php }?>
                                         </select>
                                    </div>
                                </div>
                            </div>                                           
                            <!-- <div class="item form-group">
                            
                                <div class="col-sm-12">
                                    <label class="control-label text-left col-sm-2" for="job_requirements">Job requirments </label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control col-md-12 col-xs-12" required="required" name="job_requirements" id="job_requirements"><?php echo ($this->uri->segment(3)) ? $edit_record[0]['job_requirements'] : ''; ?></textarea>
                                        
                                        <script>
                                                CKEDITOR.replace( 'job_requirements' );
                                        </script>
                                    </div>
                                </div>              
                             </div> -->
                             <div class="item form-group">
                                <div class="col-sm-12">
                                    <label class="control-label text-left col-sm-2" for="job_description">Job Description </label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control col-md-12 col-xs-12" required="required" name="job_description" id="job_description"><?php echo ($this->uri->segment(3)) ? $edit_record[0]['job_description'] : ''; ?></textarea>
                                        <script>
                                                CKEDITOR.replace( 'job_description' );
                                        </script>
                                    </div>
                                </div>
                            </div>
                     

                            <div class="item form-group">
                            <div class="col-sm-6">
                                    <label class="control-label text-left col-sm-4" for="companyId">Company Name<span class="required">*</span></label>
                                    <div class="col-sm-8">
                                       <select name="companyId" class="form-control" required>
                                       <option value="">Choose Company</option>
                                       <?php  foreach($companies as $company){?>
                                       <option value="<?php echo $company['companyId'];?>" <?php echo $company['companyId'] == @$edit_record[0]['companyId'] ? 'selected':''?>><?php echo $company['company_title'];?></option>
                                       <?php }?>
                                       
                                       </select>
                                    </div>
                                </div>

                              
                                <!-- <div class="col-sm-6">
                                    <label class="control-label text-left col-sm-4" for="purchase_order_id">Company Logo<span class="required">*</span></label>
                                    <div class="col-sm-8">
                                       <input type="file" name="companyLogo" placeholder="Company Logo" required="required" class="form-control" id="companyLogo">
                                    </div>
                                </div> -->
                                
                                </div>
                          
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-4 col-md-offset-2">
                                    <button id="send" type="submit" class="btn btn-success"><?php echo $this->uri->segment(3) ?'Update' :'Save'?></button>
                                    <button type="reset" class="btn btn-primary">Cancel</button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                    <div class="x_title">
                         <h2>
                            <a href="javascript:void(0)" type="button" class="btn btn-round btn-warning" id="addJob">Add Job</a>
                        </h2>

                        <?php if($this->session->userdata('user_role') ==='admin'){?>
                        <h2>
                            <a href="javascript:void(0)" type="button" class="btn btn-round btn-danger" id="btn-del-data">Delete</a>
                        </h2>
                        <?php }?>
                        <!--ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>

                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <?php if($this->session->flashdata('flash_msg_yes')){ ?>

                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>Congratulation ! </strong> <?php echo $this->session->flashdata('flash_msg_yes'); ?>
                            </div>

                        <?php }?>
                        <?php if($this->session->flashdata('flash_msg_no')){ ?>

                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>Error ! </strong> <?php echo $this->session->flashdata('flash_msg_no'); ?>
                            </div>

                        <?php }?>
                        <form name="del-allowance" id="del-bulk-data" action="<?php echo site_url().$this->path ?>Jobs/delete_bulk/" method="post">
                            <div class="table-responsive">
                                <table id="datatable-checkbox" class="table jambo_table table-striped bulk_action">
                                    <thead>
                                    <tr class="headings">
                                        <th>
                                            <input type="checkbox" id="check-all" class="flat">
                                        </th>
                                        <th class="column-title">Job ID</th>
                                        <th class="column-title">Title</th>
                                        <th class="column-title">Location </th>
                                        <th class="column-title">Salary </th>
                                        <th class="column-title">Category </th>
                                        <th class="column-title">Application Date</th>
                                        <th class="column-title">Posted Date</th>
                                        <?php if($this->session->userdata('user_role') ==='admin'){?>
                                        <th class="column-title no-link last"><span class="nobr">Action</span></th>
                                        <th class="bulk-actions" colspan="7">
                                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                        </th>
                                        <?php }?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($record as $row): ?>
                                    <?php $categoryName=$this->Base_model->getAll('categories','',"categoryId   = ".$row['job_categoryId']);?>
                                        <tr class=" pointer">
                                            <td class="a-center ">
                                                <input type="checkbox" class="flat" name="table_records[]" value="<?=$row['job_id']?>">
                                            </td>
                                            <td class=" "><?php echo $row['job_id'];?></td>
                                            <td class=" "><?php echo $row['job_title'];?></td>
                                            <td class=" "><?php echo $row['job_location'];?></td>
                                            <td class=" "><?php echo $row['job_salary'];?></td>
                                            <td class=" "><?php echo @$categoryName[0]['categoryName']?></td>
                                            <td class=" "><?php echo $row['application_date'];?></td>
                                            <td class=" "><?php echo $row['posted_date'];?></td>
                                            <?php if($this->session->userdata('user_role') ==='admin'){?>
                                            <td class="last">
                                                <a href="<?php echo site_url().$this->path ?>Jobs/edit/<?php echo $row['job_id'] ?>"><i class="fa fa-pencil"></i></a>
                                                &nbsp;
                                               
                                                <a href="<?php echo site_url().$this->path ?>Jobs/delete/<?php echo $row['job_id'] ?>"><i class="fa fa-trash-o"></i></a>
                                                
                                            </td>
                                            <?php }?>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php $this->load->view('include/footer');?>

