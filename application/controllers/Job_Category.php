<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job_Category extends CI_Controller {

    public $title = "Job Categories";

    public $path = "";

    public function __construct()
    {
        parent::__construct();

        $this->load->database();

        $this->load->helper(array('url','form'));

        $this->load->library(array('session' , 'user_agent'));

        $this->load->model(array('Base_model'));


        //checking login session
        if(!$this->session->userdata('user_id')){
            redirect('admin/login');
        }
           
    }

    //------------ record list function

    public function index()
    {
      
        $data['record']=$this->Base_model->getAll('categories');

        $this->load->view('category/job_categories',$data);

    }

    //----------- Set new data function

    public function set_data()
    {
 
        $this->Base_model->insert('categories',$this->input->post());

        $this->session->set_flashdata('flash_msg_yes', 'Record has been Added');

        redirect('Job_Category');
    }

    //------------ Edit record View function

    public function edit()
    {

        $data['record']=$this->Base_model->getAll('categories');

        $data['edit_record']=$this->Base_model->getAll('categories','',"categoryId  = ".$this->uri->segment(3));

        $this->load->view('category/job_categories' , $data);


    }

    //----------- Edit record function

    public function edit_data()
    {
        $this->Base_model->update('categories',$this->input->post(),"categoryId = ".$this->input->post('edit_id'), 'edit_id');

        $this->session->set_flashdata('flash_msg_yes', 'Record has been Updated');

        redirect('Job_Category');

    }

    //----------- Delete function

    public function delete()
    {
        if($this->uri->segment(3)){

            $this->Base_model->delete('categories',"categoryId = ".$this->uri->segment(3));

            $this->session->set_flashdata('flash_msg_yes', 'Record has been deleted');

            redirect('Job_Category');
        }
    }

    //----------- Delete Bulk function

    public function delete_bulk()
    {

        if(count($_POST['table_records'])> 0){
            foreach($_POST['table_records'] as $id):

                $this->Base_model->delete('categories',"categoryId = ".$id);
    
            endforeach;
    
            $this->session->set_flashdata('flash_msg_yes', 'Record has been deleted');
    
            redirect('Job_Category');
        }
        else{
            $this->session->set_flashdata('flash_msg_no', 'No Record selected!');
    
            redirect('Job_Category');
        }
        
    }
}