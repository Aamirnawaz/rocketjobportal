<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FrontendController extends CI_Controller {

    public $order_path = 'orders';

    private $_applicant_model;

    private $_login_model;

    public function __construct()
    {
        parent::__construct();
        
        $this->load->database();

        $this->load->helper(array('url','form'));

        $this->load->library(array('user_agent','session','upload','form_validation','encryption'));
        
        $this->load->model(array('Applicant_model','Login_model','Base_model'));
       
        $this->_applicant_model = new Applicant_model();

        $this->_login_model = new Login_model();

    }

    public function index()
    {

        $data['jobLocations']  = $this->_applicant_model->getJobLocations();
        $data['featured_jobs'] = $this->_applicant_model->featuered_jobs();

        $this->load->view('frontend/includes/header');
     
        $this->load->view('frontend/home',$data);
     
        $this->load->view('frontend/includes/footer');
    }


    public function job_listing()
    {
        if((@$_GET['job_title']) || (@$_GET['job_location'])){
       
            $data['all_jobs']=$this->_applicant_model->user_search_jobs(@$_GET['job_title'],@$_GET['job_location']);
            $data['categories']=$this->Base_model->getAll('categories');
            $data['jobLocations']  = $this->_applicant_model->getJobLocations();
        
            $this->load->view('frontend/includes/header');
            
            $this->load->view('frontend/job_listing',$data);
            
            $this->load->view('frontend/includes/footer');
        }else{
                
            $data['all_jobs']=$this->Base_model->getAll('job_details');
            $data['categories']=$this->Base_model->getAll('categories');
            $data['jobLocations']  = $this->_applicant_model->getJobLocations();

            $this->load->view('frontend/includes/header');
        
            $this->load->view('frontend/job_listing',$data);
            
            $this->load->view('frontend/includes/footer');
        }
        
        
    }

    public function job_details()
    {
        $data['jobDetail']=$this->Base_model->getAll('job_details','',"job_id = ".@$_GET['job_id']);

        $data['companyDetails']=$this->Base_model->getAll('companies','',"companyId = ".@$data['jobDetail'][0]['companyId']);

        $data['categoryName']=$this->Base_model->getAll('categories','',"categoryId  = ".@$data['jobDetail'][0]['job_categoryId']);

        $this->load->view('frontend/includes/header');
        
        $this->load->view('frontend/job_details',$data);
        
        $this->load->view('frontend/includes/footer');
    }


    public function save_applicant()
    {

    if($this->input->post('first_name')!=='' || $this->input->post('applicant_email')!=='' || $this->input->post('applicant_contact')!==''){

        if (!empty($_FILES)) {

            $tempFile = $_FILES['resume_path']['tmp_name'];

            $temp = $_FILES["resume_path"]["name"];

            $path_parts = pathinfo($temp);

            $t = preg_replace('/\s+/', '', microtime());

            $fileName = $path_parts['filename'].'_'. $t . '.' . $path_parts['extension'];

            $targetPath = './assets/job_applications/';

            $targetFile = $targetPath . $fileName ;
            
            move_uploaded_file($tempFile, $targetFile);   
            // End file upload


            $data['first_name'] = $this->input->post('first_name');

            $data['middle_name'] = $this->input->post('last_name');
    
            $data['applicant_email'] = $this->input->post('applicant_email') ;
    
            $data['applicant_contact'] = $this->input->post('applicant_contact');
           
            $data['application_status'] = 'pending';
            
            $data['job_id'] = $this->input->post('job_id');
            
            $data['categoryId'] = $this->input->post('categoryId');

            

            $data['resume_path'] = $fileName;

            $result = $this->_applicant_model->save_applicant($data);
        
            if($result===TRUE){
    
            $this->session->set_flashdata('success_message', 'Application submited successfully! Against JobID='.$this->input->post('job_id').' customer support will contact you within 3 days.');    

                //Send Application confirmation email
                $to =$this->input->post('applicant_email');
                $subject = "Application Sent Successfully!";

                $message = '<HTML>
                <BODY style="font-size: 10pt; font-family: Verdana, sans-serif;">
                  Dear '.$this->input->post('first_name').', <br> Thank you for submitting your application. We will review your application and contact you shortly.   
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                      <table>
                      <tbody>
                      <tr>
                        <td style="font-size: 10pt; font-family: Verdana, sans-serif; border-right: 1px solid; border-right-color: #808080; width:180px; padding-right: 10px; vertical-align: top;" valign="top">
                          <img src="'.base_url('assets/img/logo/emailLogo.png').'" alt="photograph" width="116" border="0" style="border:0; height:auto; width:116px"/>
                          <p style="margin-top:30px; margin-bottom:0; line-height:16px">
                          <strong><span style="font-size: 12pt; font-family: Verdana, sans-serif; color:#262626;">RocketJobs <br></span></strong>
                          <span style="font-family: Verdana, sans-serif; font-size:9pt; color:#808080;">HR/Admin Department</span>
                          </p>
                          <p style="margin-top:30px; margin-bottom:0; line-height:16px">    
                        </td>
              
                        <td style="font-size: 10pt; color:#444444; font-family: Verdana, sans-serif; padding-bottom: 10px; padding-left: 20px; vertical-align: top; line-height:14px" valign="top">
                          <span style="color: #808080;"><strong>M</strong></span><span style="font-size: 9pt; font-family: Verdana, sans-serif; color:#808080;"> 02080883260<br></span>
                          <span style="color: #808080;"><strong>E</strong></span><span style="font-size: 9pt; font-family: Verdana, sans-serif; color:#808080;"> info@rocketjob.com <br></span>
                          <span  style="font-size: 10pt; font-family: Verdana, sans-serif; color: #808080;">Ground Floor, The Bower, Stockley Park, <br> 4 Roundwood Ave, London UB11 1AF</span> <br>
                          <a href="https://rocketjobs.co.uk/" target="_blank" rel="noopener" style="text-decoration:none;"><strong style="font-size: 9pt; font-family: Verdana, sans-serif; color: #262626;">RocketJobs</strong></a>
                        </td>
                      </tr>
                    </tbody>
                  
                  </table>
              
                  <tr>
                    <td>
                      <p style="width:550px; font-size:9px; line-height:11px; COLOR: #808080; FONT-FAMILY: Verdana, sans-serif; text-align:justify">The content of this email is confidential and intended for the recipient specified in message only. It is strictly forbidden to share any part of this message with any third party, without a written consent of the sender. If you received this message by mistake, please reply to this message and follow with its deletion, so that we can ensure such a mistake does not occur in the future.</p>
                    </td>
                  </tr>
                  <tr>
                  </body>
              </html>';

                // Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                // More headers
                $headers .= 'From: Rocketjobs<info@rocketjobs.co.uk>' . "\r\n";
                // $headers .= 'Cc: myboss@example.com' . "\r\n";

                mail($to,$subject,$message,$headers);

                redirect('FrontendController/job_details/?job_id='.$this->input->post('job_id'));
            }else{
            
            $this->session->set_flashdata('error_message','Application faild to submit!');
            
            }
    
            $this->load->view('frontend/includes/header');
    
            $this->load->view('frontend/job_details',$data);
    
            $this->load->view('frontend/includes/footer');
            }
        }else{
            
            $this->session->set_flashdata('error_message','Failed submission, please fill required fields!');
            redirect('FrontendController/job_details/?job_id='.$this->input->post('job_id'));
        }
    }


   

    public function remainingApplication()
    {

        $applicantHashing = $_GET['details'];
        // Db query for checking hashing of applicant
       $data=$this->_applicant_model->getHasing($applicantHashing);      
       $applicantId = @$data[0]['applicant_id'];
        
        // $data['record']=$this->Base_model->getAll('job_applications','',"applicant_id = ".$applicantId);
        $data['title'] = 'Complete Application Form';
        $data['applicant_id']=$applicantId;

        
        $form_type = @$data[0]['application_formType'];
     
   
        if($form_type==="standard_form"){
            $data['title'] = 'Standard Form';
            $this->load->view('frontend/includes/header');
            $this->load->view('frontend/standard_form',$data);
            $this->load->view('frontend/includes/footer');
        }else {
            $data['title'] = 'DBS / Right to Work';
            $this->load->view('frontend/includes/header');
            $this->load->view('frontend/enhanced_form',$data);
            $this->load->view('frontend/includes/footer');
        }

    }

    public function saveStandardForm()
    {
      $applicantId = md5($this->input->post('applicant_id'));

        if($this->input->post('applicant_id')!=='')
        {
      
          // Passport file
          $tempFile1 = $_FILES['passport_file']['tmp_name'];
          $temp1 = $_FILES["passport_file"]["name"];
          $path_parts1 = pathinfo($temp1);
          $t1 = preg_replace('/\s+/', '', microtime());
          $passport_file = $path_parts1['filename'].'_'. $t1 . '.' . $path_parts1['extension'];
          $targetPath1 = './assets/passport_files/';
          $targetFile1 = $targetPath1 . $passport_file ;
          move_uploaded_file($tempFile1, $targetFile1);   
          

          // Driving licence file
          $tempFile2 = $_FILES['driving_license_file']['tmp_name'];
          $temp2 = $_FILES["driving_license_file"]["name"];
          $path_parts2 = pathinfo($temp2);
          $t2 = preg_replace('/\s+/', '', microtime());
          $driving_license_file = $path_parts2['filename'].'_'. $t2 . '.' . $path_parts2['extension'];
          $targetPath2 = './assets/driving_licence/';
          $targetFile2 = $targetPath2 . $driving_license_file ;
          move_uploaded_file($tempFile2, $targetFile2);   
          

          // visa files
          $tempFile3 = $_FILES['visa_file']['tmp_name'];
          $temp3 = $_FILES["visa_file"]["name"];
          $path_parts3 = pathinfo($temp3);
          $t3 = preg_replace('/\s+/', '', microtime());
          $visa_file = $path_parts3['filename'].'_'. $t3 . '.' . $path_parts3['extension'];
          $targetPath3 = './assets/visa_files/';
          $targetFile3 = $targetPath3 . $visa_file ;
          move_uploaded_file($tempFile3, $targetFile3);   
          

          // Additional docs
          $tempFile4 = $_FILES['additional_docs_file']['tmp_name'];
          $temp4 = $_FILES["additional_docs_file"]["name"];
          $path_parts4 = pathinfo($temp4);
          $t4 = preg_replace('/\s+/', '', microtime());
          $additional_docs_file = $path_parts4['filename'].'_'. $t4 . '.' . $path_parts4['extension'];
          $targetPath4 = './assets/additional_documents/';
          $targetFile4 = $targetPath4 . $additional_docs_file ;
          move_uploaded_file($tempFile4, $targetFile4);   
          

          // proof of address
          $tempFile5 = $_FILES['proof_of_address_file']['tmp_name'];
          $temp5 = $_FILES["proof_of_address_file"]["name"];
          $path_parts5 = pathinfo($temp5);
          $t5 = preg_replace('/\s+/', '', microtime());
          $proof_of_address_file = $path_parts5['filename'].'_'. $t5 . '.' . $path_parts5['extension'];
          $targetPath5 = './assets/proof_of_address_file/';
          $targetFile5 = $targetPath5 . $proof_of_address_file ;
          move_uploaded_file($tempFile5, $targetFile5);   

              $data = array(
                'passport_file'=>$passport_file,
                'driving_license_file'=>$driving_license_file ,
                'visa_file'=> $visa_file,
                'additional_docs_file'=> $additional_docs_file,
                'proof_of_address_file'=> $proof_of_address_file,

                'title' => $this->input->post('title'),
                'first_name'=>$this->input->post('first_name'),
                'first_middle_name'=>$this->input->post('first_middle_name'),
                'second_middle_name'=>$this->input->post('second_middle_name'),
                'third_middle_name'=>$this->input->post('third_middle_name'),
                'present_sur_name'=>$this->input->post('present_sur_name'),
                'knownByOtherName'=>$this->input->post('knownByOtherName'),
                'hasValidLicence'=>$this->input->post('hasValidLicence'),
                'hasValidPassport'=>$this->input->post('hasValidPassport'),
                'gender'=>$this->input->post('gender'),
                'dob'=>$this->input->post('dob'),
                'country'=>$this->input->post('country'),
                'postcode'=>$this->input->post('postcode'),
                'currentAddress'=>$this->input->post('currentAddress'),
                'streetName'=>$this->input->post('streetName'),
                'town'=>$this->input->post('town'),
                'country'=>$this->input->post('country'),
                'start_living_date'=>$this->input->post('start_living_date'),
                'DBS_profile_no'=>$this->input->post('DBS_profile_no'),
                'view_DBS_certificate'=>$this->input->post('view_DBS_certificate'),
                'concent_thirdparty_email'=>$this->input->post('concent_thirdparty_email'),
                'request_paper_certificate'=>$this->input->post('request_paper_certificate'),
                'birth_town'=>$this->input->post('birth_town'),
                'birth_country'=>$this->input->post('birth_country'),
                'uk_insurance_avaliable'=>$this->input->post('uk_insurance_avaliable'),
                'applicant_email'=>$this->input->post('applicant_email'),
                'mobile_contact'=>$this->input->post('mobile_contact'),
                'passportNo'=>$this->input->post('passportNo'),
                'passport_issue_country'=>$this->input->post('hasNationalityChanged'),
                'DrivingLicenceNo'=>$this->input->post('DrivingLicenceNo'),
                'licence_issue_country'=>$this->input->post('licence_issue_country'),
                'fromDate' => $this->input->post('fromDate'),
                'toDate' => $this->input->post('toDate'),
                'postcode_1' => $this->input->post('postcode_1'),
                'line_1' => $this->input->post('line_1'),
                'line_11' => $this->input->post('line_11'),
                'townCity_1' => $this->input->post('townCity_1'),
                'fromDate_1' => $this->input->post('fromDate_1'),
                'toDate_1' => $this->input->post('toDate_1'),
                'country_1' => $this->input->post('country_1'),
                'postcode_2' => $this->input->post('postcode_2'),
                'line_2' => $this->input->post('line_2'),
                'line_22' => $this->input->post('line_22'),
                'townCity_2' => $this->input->post('townCity_2'),
                'fromDate_2' => $this->input->post('fromDate_2'),
                'toDate_2' => $this->input->post('toDate_2'),
                'country_2' => $this->input->post('country_2'),
                'postcode_3' => $this->input->post('postcode_3'),
                'line_3' => $this->input->post('line_3'),
                'line_33' => $this->input->post('line_33'),
                'townCity_3' => $this->input->post('townCity_3'),
                'fromDate_3' => $this->input->post('fromDate_3'),
                'toDate_3' =>$this->input->post('toDate_3'),
                'country_3' => $this->input->post('country_3'),
                'postcode_4' => $this->input->post('postcode_4'),
                'line_4' => $this->input->post('line_4'),
                'line_44' => $this->input->post('line_44'),
                'townCity_4' => $this->input->post('townCity_4'),
                'fromDate_4' => $this->input->post('fromDate_4'),
                'toDate_4' =>$this->input->post('toDate_4'),
                'country_4' => $this->input->post('country_4'),
                'postcode_5' => $this->input->post('postcode_5'),
                'line_5' => $this->input->post('line_5'),
                'line_55' => $this->input->post('line_55'),
                'townCity_5' => $this->input->post('townCity_5'),
                'fromDate_5' => $this->input->post('fromDate_5'),
                'toDate_5' => $this->input->post('toDate_5'),
                'country_5' => $this->input->post('country_5')
            );
       
            // $this->Base_model->update('job_applications',$data,"applicant_id  = ".$this->input->post('applicant_id'), 'edit_id');
            $this->Base_model->update('job_applications',$data,"applicant_id  = ".$this->input->post('applicant_id'), 'edit_id');
            

            // send email to user your form has been saved successfuly
           /* $to =$this->input->post('applicant_email');
            $subject = "Application Sent Successfully!";

            $message = '<HTML>
            <BODY style="font-size: 10pt; font-family: Verdana, sans-serif;">
              Dear '.$this->input->post('first_name').', <br> Thank you you form has been submitted successfuly. We will contact you shortly.           
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                  <table>
                  <tbody>
                  <tr>
                    <td style="font-size: 10pt; font-family: Verdana, sans-serif; border-right: 1px solid; border-right-color: #808080; width:180px; padding-right: 10px; vertical-align: top;" valign="top">
                      <img src="'.base_url('assets/img/logo/emailLogo.png').'" alt="photograph" width="116" border="0" style="border:0; height:auto; width:116px"/>
                      <p style="margin-top:30px; margin-bottom:0; line-height:16px">
                      <strong><span style="font-size: 12pt; font-family: Verdana, sans-serif; color:#262626;">RocketJobs <br></span></strong>
                      <span style="font-family: Verdana, sans-serif; font-size:9pt; color:#808080;">HR/Admin Department</span>
                      </p>
                      <p style="margin-top:30px; margin-bottom:0; line-height:16px">    
                    </td>
          
                    <td style="font-size: 10pt; color:#444444; font-family: Verdana, sans-serif; padding-bottom: 10px; padding-left: 20px; vertical-align: top; line-height:14px" valign="top">
                      <span style="color: #808080;"><strong>M</strong></span><span style="font-size: 9pt; font-family: Verdana, sans-serif; color:#808080;"> 02080883260<br></span>
                      <span style="color: #808080;"><strong>E</strong></span><span style="font-size: 9pt; font-family: Verdana, sans-serif; color:#808080;"> info@rocketjob.com <br></span>
                      <span  style="font-size: 10pt; font-family: Verdana, sans-serif; color: #808080;">Ground Floor, The Bower, Stockley Park, <br> 4 Roundwood Ave, London UB11 1AF</span> <br>
                      <a href="https://rocketjobs.co.uk/" target="_blank" rel="noopener" style="text-decoration:none;"><strong style="font-size: 9pt; font-family: Verdana, sans-serif; color: #262626;">RocketJobs</strong></a>
                    </td>
                  </tr>
                </tbody>
              
              </table>
          
              <tr>
                <td>
                  <p style="width:550px; font-size:9px; line-height:11px; COLOR: #808080; FONT-FAMILY: Verdana, sans-serif; text-align:justify">The content of this email is confidential and intended for the recipient specified in message only. It is strictly forbidden to share any part of this message with any third party, without a written consent of the sender. If you received this message by mistake, please reply to this message and follow with its deletion, so that we can ensure such a mistake does not occur in the future.</p>
                </td>
              </tr>
              <tr>
              </body>
          </html>';
          // Always set content-type when sending HTML email
          $headers = "MIME-Version: 1.0" . "\r\n";
          $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
          // More headers
          $headers .= 'From: info@rocketjobs.co.uk' . "\r\n";
          // $headers .= 'Cc: myboss@example.com' . "\r\n";
          mail($to,$subject,$message,$headers);
          */

            $this->session->set_flashdata('flash_msg_yes', 'Application submited successfully');  

            // echo $applicantId;exit;
            redirect('remaining-application/?details='.$applicantId);
        }
        else
        {

            $this->session->set_flashdata('flash_msg_no', 'Application submission faild! the link is expired, please contact administrator for new link');  
            redirect('remaining-application/?details='.$applicantId);
        }
    }


    public function saveEnhancedForm()
    {
      $applicantId = md5($this->input->post('applicant_id'));

        if($this->input->post('applicant_id')!=='')
        {
          
          // Passport file
          $tempFile1 = $_FILES['passport_file']['tmp_name'];
            $temp1 = $_FILES["passport_file"]["name"];
            $path_parts1 = pathinfo($temp1);
            $t1 = preg_replace('/\s+/', '', microtime());
            $passport_file = $path_parts1['filename'].'_'. $t1 . '.' . $path_parts1['extension'];
            $targetPath1 = './assets/passport_files/';
            $targetFile1 = $targetPath1 . $passport_file ;
            move_uploaded_file($tempFile1, $targetFile1);   
            

            // Driving licence file
            $tempFile2 = $_FILES['driving_license_file']['tmp_name'];
            $temp2 = $_FILES["driving_license_file"]["name"];
            $path_parts2 = pathinfo($temp2);
            $t2 = preg_replace('/\s+/', '', microtime());
            $driving_license_file = $path_parts2['filename'].'_'. $t2 . '.' . $path_parts2['extension'];
            $targetPath2 = './assets/driving_licence/';
            $targetFile2 = $targetPath2 . $driving_license_file ;
            move_uploaded_file($tempFile2, $targetFile2);   
            

            // visa files
            $tempFile3 = $_FILES['visa_file']['tmp_name'];
            $temp3 = $_FILES["visa_file"]["name"];
            $path_parts3 = pathinfo($temp3);
            $t3 = preg_replace('/\s+/', '', microtime());
            $visa_file = $path_parts3['filename'].'_'. $t3 . '.' . $path_parts3['extension'];
            $targetPath3 = './assets/visa_files/';
            $targetFile3 = $targetPath3 . $visa_file ;
            move_uploaded_file($tempFile3, $targetFile3);   
            

            // Additional docs
            $tempFile4 = $_FILES['additional_docs_file']['tmp_name'];
            $temp4 = $_FILES["additional_docs_file"]["name"];
            $path_parts4 = pathinfo($temp4);
            $t4 = preg_replace('/\s+/', '', microtime());
            $additional_docs_file = $path_parts4['filename'].'_'. $t4 . '.' . $path_parts4['extension'];
            $targetPath4 = './assets/additional_documents/';
            $targetFile4 = $targetPath4 . $additional_docs_file ;
            move_uploaded_file($tempFile4, $targetFile4);   
            

            // proof of address
            $tempFile5 = $_FILES['proof_of_address_file']['tmp_name'];
            $temp5 = $_FILES["proof_of_address_file"]["name"];
            $path_parts5 = pathinfo($temp5);
            $t5 = preg_replace('/\s+/', '', microtime());
            $proof_of_address_file = $path_parts5['filename'].'_'. $t5 . '.' . $path_parts5['extension'];
            $targetPath5 = './assets/proof_of_address_file/';
            $targetFile5 = $targetPath5 . $proof_of_address_file ;
            move_uploaded_file($tempFile5, $targetFile5);   

            $data = array(
                'passport_file'=>$passport_file,
                'driving_license_file'=>$driving_license_file ,
                'visa_file'=> $visa_file,
                'additional_docs_file'=> $additional_docs_file,
                'proof_of_address_file'=> $proof_of_address_file,

                'title' => $this->input->post('title'),
                'first_name'=>$this->input->post('first_name'),
                'first_middle_name'=>$this->input->post('first_middle_name'),
                'second_middle_name'=>$this->input->post('second_middle_name'),
                'third_middle_name'=>$this->input->post('third_middle_name'),
                'present_sur_name'=>$this->input->post('present_sur_name'),
                'hasOtherSurName'=>$this->input->post('hasOtherSurName'),
                'knownByOtherName'=>$this->input->post('knownByOtherName'),
                'hasValidLicence'=>$this->input->post('hasValidLicence'),
                'hasValidPassport'=>$this->input->post('hasValidPassport'),
                'surname_at_birth'=> $this->input->post('surname_at_birth'),
                'gender'=>$this->input->post('gender'),
                'dob'=>$this->input->post('dob'),
                'usedUntil_surname'=>$this->input->post('usedUntil_surname'),
                'postcode'=>$this->input->post('postcode'),
                'currentAddress'=>$this->input->post('currentAddress'),
                'streetName'=>$this->input->post('streetName'),
                'town'=>$this->input->post('town'),
                'country'=>$this->input->post('country'),
                'start_living_date'=>$this->input->post('start_living_date'),
                'birth_country'=>$this->input->post('birth_country'),
                'birth_state'=>$this->input->post('birth_state'),
                'birth_town'=>$this->input->post('birth_town'),
                'uk_insurance_no'=>$this->input->post('uk_insurance_no'),
                'telephone_number'=>$this->input->post('telephone_number'),
                'mobile_contact'=>$this->input->post('mobile_contact'),
                'anyConvictions'=>$this->input->post('anyConvictions'),
                'passportNo'=>$this->input->post('passportNo'),
                'DOB_on_passport'=>$this->input->post('DOB_on_passport'),
                'passport_nationality'=>$this->input->post('passport_nationality'),
                'passport_issue_date'=>$this->input->post('passport_issue_date'),
                'DrivingLicenceNo'=>$this->input->post('DrivingLicenceNo'),
                'DOB_on_driving_licence'=>$this->input->post('DOB_on_driving_licence'),
                'licence_type'=>$this->input->post('licence_type'),
                'licence_valid_from'=>$this->input->post('licence_valid_from'),
                'licence_issue_country'=>$this->input->post('licence_issue_country'),
                // 'fromDate' => $this->input->post('fromDate'),
                // 'toDate' => $this->input->post('toDate'),
                'postcode_1' => $this->input->post('postcode_1'),
                'line_1' => $this->input->post('line_1'),
                'line_11' => $this->input->post('line_11'),
                'townCity_1' => $this->input->post('townCity_1'),
                'fromDate_1' =>$this->input->post('fromDate_1'),
                'toDate_1' => $this->input->post('toDate_1'),
                'country_1' => $this->input->post('country_1'),
                'postcode_2' => $this->input->post('postcode_2'),
                'line_2' => $this->input->post('line_2'),
                'line_22' => $this->input->post('line_22'),
                'townCity_2' => $this->input->post('townCity_2'),
                'fromDate_2' => $this->input->post('fromDate_2'),
                'toDate_2' => $this->input->post('toDate_2'),
                'country_2' => $this->input->post('country_2'),
                'postcode_3' => $this->input->post('postcode_3'),
                'line_3' => $this->input->post('line_3'),
                'line_33' => $this->input->post('line_33'),
                'townCity_3' => $this->input->post('townCity_3'),
                'fromDate_3' => $this->input->post('fromDate_3'),
                'toDate_3' => $this->input->post('toDate_3'),
                'country_3' => $this->input->post('country_3'),
                'postcode_4' => $this->input->post('postcode_4'),
                'line_4' => $this->input->post('line_4'),
                'line_44' => $this->input->post('line_44'),
                'townCity_4' => $this->input->post('townCity_4'),
                'fromDate_4' => $this->input->post('fromDate_4'),
                'toDate_4' => $this->input->post('toDate_4'),
                'country_4' => $this->input->post('country_4'),
                'postcode_5' => $this->input->post('postcode_5'),
                'line_5' => $this->input->post('line_5'),
                'line_55' => $this->input->post('line_55'),
                'townCity_5' => $this->input->post('townCity_5'),
                'fromDate_5' => $this->input->post('fromDate_5'),
                'toDate_5' => $this->input->post('toDate_5'),
                'country_5' => $this->input->post('country_5')
            );


            // $this->Base_model->update('job_applications',$data,"applicant_id  = ".$this->input->post('applicant_id'), 'edit_id');
            $this->Base_model->update('job_applications',$data,"applicant_id  = ".$this->input->post('applicant_id'), 'edit_id');

            // sending email to user on successfuly subbmiting form 

            $to =$this->input->post('applicant_email');
            $subject = "Application Sent Successfully!";

            $message = '<HTML>
            <BODY style="font-size: 10pt; font-family: Verdana, sans-serif;">
              Dear '.$this->input->post('first_name').', <br> Thank you you form has been submitted successfuly. We will contact you shortly.           
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                  <table>
                  <tbody>
                  <tr>
                    <td style="font-size: 10pt; font-family: Verdana, sans-serif; border-right: 1px solid; border-right-color: #808080; width:180px; padding-right: 10px; vertical-align: top;" valign="top">
                      <img src="'.base_url('assets/img/logo/emailLogo.png').'" alt="photograph" width="116" border="0" style="border:0; height:auto; width:116px"/>
                      <p style="margin-top:30px; margin-bottom:0; line-height:16px">
                      <strong><span style="font-size: 12pt; font-family: Verdana, sans-serif; color:#262626;">RocketJobs <br></span></strong>
                      <span style="font-family: Verdana, sans-serif; font-size:9pt; color:#808080;">HR/Admin Department</span>
                      </p>
                      <p style="margin-top:30px; margin-bottom:0; line-height:16px">    
                    </td>
          
                    <td style="font-size: 10pt; color:#444444; font-family: Verdana, sans-serif; padding-bottom: 10px; padding-left: 20px; vertical-align: top; line-height:14px" valign="top">
                      <span style="color: #808080;"><strong>M</strong></span><span style="font-size: 9pt; font-family: Verdana, sans-serif; color:#808080;"> 02080883260<br></span>
                      <span style="color: #808080;"><strong>E</strong></span><span style="font-size: 9pt; font-family: Verdana, sans-serif; color:#808080;"> info@rocketjob.com <br></span>
                      <span  style="font-size: 10pt; font-family: Verdana, sans-serif; color: #808080;">Ground Floor, The Bower, Stockley Park, <br> 4 Roundwood Ave, London UB11 1AF</span> <br>
                      <a href="https://rocketjobs.co.uk/" target="_blank" rel="noopener" style="text-decoration:none;"><strong style="font-size: 9pt; font-family: Verdana, sans-serif; color: #262626;">RocketJobs</strong></a>
                    </td>
                  </tr>
                </tbody>
              
              </table>
          
              <tr>
                <td>
                  <p style="width:550px; font-size:9px; line-height:11px; COLOR: #808080; FONT-FAMILY: Verdana, sans-serif; text-align:justify">The content of this email is confidential and intended for the recipient specified in message only. It is strictly forbidden to share any part of this message with any third party, without a written consent of the sender. If you received this message by mistake, please reply to this message and follow with its deletion, so that we can ensure such a mistake does not occur in the future.</p>
                </td>
              </tr>
              <tr>
              </body>
          </html>';
          // Always set content-type when sending HTML email
          $headers = "MIME-Version: 1.0" . "\r\n";
          $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
          // More headers
          $headers .= 'From: info@rocketjobs.co.uk' . "\r\n";
          // $headers .= 'Cc: myboss@example.com' . "\r\n";
          mail($to,$subject,$message,$headers);

            $this->session->set_flashdata('flash_msg_yes', 'Application submited successfully');  
        

            redirect('remaining-application/?details='.$applicantId);
            
        }
        else
        {
            $this->session->set_flashdata('flash_msg_no', 'Application submission faild! the link is expired, please contact administrator for new link');  
            redirect('remaining-application/?details='.$applicantId);
        }
    }

    
    public function aboutUs()
    {
        $data['title'] = 'About Us Page';
        $this->load->view('frontend/includes/header');
        $this->load->view('frontend/about',$data);
        $this->load->view('frontend/includes/footer');
    }

    public function termsAndCondition()
    {
        $data['title'] = 'Terms and Condition';
        $this->load->view('frontend/includes/header');
        $this->load->view('frontend/TermsAndCondition',$data);
        $this->load->view('frontend/includes/footer');
    }

    public function privacyPolicy()
    {
        $data['title'] = 'Privacy and Policy';
        $this->load->view('frontend/includes/header');
        $this->load->view('frontend/PrivacyAndPolicy',$data);
        $this->load->view('frontend/includes/footer');
    }

    public function contact()
    {
        $data['title'] = 'Contact Us';
        $this->load->view('frontend/includes/header');
        $this->load->view('frontend/contact',$data);
        $this->load->view('frontend/includes/footer');
    }


    public function saveContactUs()
    {
        if($this->input->post()!==''){
            $this->Base_model->insert('contact_us',$this->input->post());
            $this->session->set_flashdata('flash_msg_yes', 'Message has been sent, We will contact you shortly.');
            $to = 'info@rocketjobs.co.uk';
            $subject = $this->input->post('subject');
            $message = $this->input->post('message');
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            // More headers
            $headers .= 'From:'.$this->input->post('email')."\r\n";
            // $headers .= 'Cc: myboss@example.com' . "\r\n";
            mail($to,$subject,$message,$headers);
            redirect('contact');
        }else{
            $this->session->set_flashdata('flash_msg_no', 'Email sending faild!');
            redirect('contact');
        }
        
    }

    public function getJobCategoryWise()
    {
        $categoryId = $this->uri->segment(2);

        if($categoryId){
       
            $data['all_jobs']=$this->_applicant_model->getCategoryWiseJobs($categoryId);
            $data['categories']=$this->Base_model->getAll('categories');
            $data['jobLocations']  = $this->_applicant_model->getJobLocations();

            $this->load->view('frontend/includes/header');
            
            $this->load->view('frontend/job_listing',$data);
            
            $this->load->view('frontend/includes/footer');
        }
    }

    public function filter_jobs()
    {
       $categoryId =  $_GET['job_category'];
       $job_location =  $_GET['job_location'];
        
        $data['all_jobs']=$this->_applicant_model->filter_jobs($categoryId,$job_location);
        $data['categories']=$this->Base_model->getAll('categories');
        $data['jobLocations']  = $this->_applicant_model->getJobLocations();


        $this->load->view('frontend/includes/header');
            
        $this->load->view('frontend/job_listing',$data);
        
        $this->load->view('frontend/includes/footer');
    }
}
