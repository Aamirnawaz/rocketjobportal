<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs extends CI_Controller {

    public $title = "Jobs List";

    public $path = "";

    public function __construct()
    {
        parent::__construct();

        $this->load->database();

        $this->load->helper(array('url','form'));

        $this->load->library(array('session' , 'user_agent','functions'));

        $this->load->model(array('Base_model'));

          //checking login session
          if(!$this->session->userdata('user_id')){
            redirect('admin/login');
        }
    }

    //------------ record list function

    public function index()
    {
      
        $data['companies']=$this->Base_model->getAll('companies');
        $data['categories']=$this->Base_model->getAll('categories');
        $data['record']=$this->Base_model->getAll('job_details');

        $this->load->view('jobs/jobsList',$data);

    }

    //----------- Set new data function

    public function set_data()
    {
        if($this->input->post()){
        // Saving company logo
        // $companyLogo = 'asset/companies_logos/avatar.jpg';

        // if (isset($_FILES['companyLogo']) && trim($_FILES["companyLogo"]["name"]) != '') {
        //     $ext = explode('.', $_FILES["companyLogo"]["name"]);
        //     $ext = end($ext);
        //     $companyLogo = 'companyLogo_'.rand(0, 9999999) . "_" . time() . "." . $ext;

        //     if (stristr('.jpeg|.JPEG|.jpg|.png|.PNG|.gif|.GIF|.JPG', $ext) == true) {
        //         if (move_uploaded_file($_FILES['companyLogo']['tmp_name'], "assets/companies_logos/" . $companyLogo) == true) {

        //             $this->functions->ImageResize("assets/companies_logos/" . $companyLogo, "assets/companies_logos/" . $companyLogo, 220, 220);
        //         }
        //     }
        // }

        $data = array(
            'job_title' => $this->input->post('job_title'),
            'job_nature' => $this->input->post('job_nature'),
            'job_location' => $this->input->post('job_location'),
            'job_salary' =>$this->input->post('job_salary'),
            // 'job_salary_range' => $this->input->post('job_salary_range'),
            'application_date' => date("Y-m-d", strtotime($this->input->post('application_date'))),
            'job_categoryId' => $this->input->post('job_categoryId'),
            // 'job_education' => $this->input->post('job_education'),
            // 'job_experience' => $this->input->post('job_experience'),
            // 'job_requirements' => $this->input->post('job_requirements'),
            'job_description' => $this->input->post('job_description'),
            'companyId' => $this->input->post('companyId'),
            'no_of_vacancies' => $this->input->post('no_of_vacancies'),
            // 'companyLogo'=>  $companyLogo,
        );

            
        

        $this->Base_model->insert('job_details',$data);

        $this->session->set_flashdata('flash_msg_yes', 'Record has been Added');

        redirect('Jobs');
    }
    }

    //------------ Edit record View function

    public function edit()
    {
     
        $data['companies']=$this->Base_model->getAll('companies');
        $data['categories']=$this->Base_model->getAll('categories');
        $data['record']=$this->Base_model->getAll('job_details');

        $data['edit_record']=$this->Base_model->getAll('job_details','',"job_id   = ".$this->uri->segment(3));

        $this->load->view('jobs/jobsList' , $data);


    }

    //----------- Edit record function

    public function edit_data()
    {

        // $companyLogo = 'asset/companies_logos/avatar.jpg';

        // if (isset($_FILES['companyLogo']) && trim($_FILES["companyLogo"]["name"]) != '') {
        //     $ext = explode('.', $_FILES["companyLogo"]["name"]);
        //     $ext = end($ext);
        //     $companyLogo = 'companyLogo_'.rand(0, 9999999) . "_" . time() . "." . $ext;

        //     if (stristr('.jpeg|.JPEG|.jpg|.png|.PNG|.gif|.GIF|.JPG', $ext) == true) {
        //         if (move_uploaded_file($_FILES['companyLogo']['tmp_name'], "assets/companies_logos/" . $companyLogo) == true) {

        //             $this->functions->ImageResize("assets/companies_logos/" . $companyLogo, "assets/companies_logos/" . $companyLogo, 520, 520);
        //         }
        //     }
        // }

        $data = array(
            'job_title' => $this->input->post('job_title'),
            'job_nature' => $this->input->post('job_nature'),
            'job_location' => $this->input->post('job_location'),
            'job_salary' =>$this->input->post('job_salary'),
            // 'job_salary_range' => $this->input->post('job_salary_range'),
            'application_date' => date("Y-m-d", strtotime($this->input->post('application_date'))),
            'job_categoryId' => $this->input->post('job_categoryId'),
            // 'job_education' => $this->input->post('job_education'),
            // 'job_experience' => $this->input->post('job_experience'),
            // 'job_requirements' => $this->input->post('job_requirements'),
            'job_description' => $this->input->post('job_description'),
            'companyId' => $this->input->post('companyId'),
            'no_of_vacancies' => $this->input->post('no_of_vacancies'),
            // 'companyLogo'=>  $companyLogo,
        );
        
        $this->Base_model->update('job_details',$data,"job_id  = ".$this->input->post('edit_id'), 'edit_id');

        $this->session->set_flashdata('flash_msg_yes', 'Record has been Updated');

        redirect('Jobs');

    }

    //----------- Delete function

    public function delete()
    {

        if($this->uri->segment(3)){
    
            //geting image name 
            $companyLogoName = $this->Base_model->getAll('job_details',$this->input->post(),"job_id  = ".$this->uri->segment(3));
            
            // unlink("assets/companies_logos/". $companyLogoName[0]['companyLogo']);

            $this->Base_model->delete('job_details',"job_id  = ".$this->uri->segment(3));

            $this->session->set_flashdata('flash_msg_yes', 'Record has been deleted');

            redirect('Jobs');
        }
    }

    //----------- Delete Bulk function

    public function delete_bulk()
    {

        
        if(count($_POST['table_records'])> 0){
            foreach($_POST['table_records'] as $id):

                $this->Base_model->delete('job_details',"job_id  = ".$id);
                
    
            endforeach;
    
            $this->session->set_flashdata('flash_msg_yes', 'Record has been deleted');
    
            redirect('Jobs');
        }
        else{
            $this->session->set_flashdata('flash_msg_no', 'No Record selected!');
    
            redirect('Jobs');
        }


       
    }
}