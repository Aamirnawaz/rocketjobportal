<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ShowFormController extends CI_Controller {

    public $order_path = 'Show Data in Form';

    private $_applicant_model;

    private $_login_model;

    public function __construct()
    {
        parent::__construct();
        
        $this->load->database();

        $this->load->helper(array('url','form'));

        $this->load->library(array('user_agent','session','upload','form_validation','encryption'));
        
        $this->load->model(array('Applicant_model','Login_model','Base_model'));
       
        $this->_applicant_model = new Applicant_model();

        $this->_login_model = new Login_model();

    }

    public function index(){
        $applicantID =$this->uri->segment(3);
        $form_type =  $this->uri->segment(4);
        if($form_type ==='enhanced_form'){
            $data['edit_record'] = $this->Base_model->getAll('job_applications','',"applicant_id=".$applicantID);
            $data['title'] = 'DBS / Right to Work';
            $this->load->view('frontend/includes/header');
            $this->load->view('show_form_data/formData',$data);
            $this->load->view('frontend/includes/footer');
        }else{
            $data['edit_record'] = $this->Base_model->getAll('job_applications','',"applicant_id=".$applicantID);
            $data['title'] = 'Standard Form';
            $this->load->view('frontend/includes/header');
            $this->load->view('show_form_data/standardFormData',$data);
            $this->load->view('frontend/includes/footer');
        }
        
    }

}