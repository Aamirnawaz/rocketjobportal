<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Companies extends CI_Controller {

    public $title = "Companies Details";

    public $path = "";

    public function __construct()
    {
        parent::__construct();

        $this->load->database();

        $this->load->helper(array('url','form'));

        $this->load->library(array('session' , 'user_agent'));

        $this->load->model(array('Base_model'));
        
        if(!$this->session->userdata('user_id')){
            redirect('admin/login');
        }
    }

    //------------ record list function

    public function index()
    {
      
        $data['record']=$this->Base_model->getAll('companies');

        $this->load->view('companies/companies_list',$data);

    }

    //----------- Set new data function

    public function set_data()
    {
 
        $this->Base_model->insert('companies',$this->input->post());

        $this->session->set_flashdata('flash_msg_yes', 'Record has been Added');

        redirect('Companies');
    }

    //------------ Edit record View function

    public function edit()
    {

        $data['record']=$this->Base_model->getAll('companies');

        $data['edit_record']=$this->Base_model->getAll('companies','',"companyId   = ".$this->uri->segment(3));

        $this->load->view('companies/companies_list' , $data);


    }

    //----------- Edit record function

    public function edit_data()
    {
        $this->Base_model->update('companies',$this->input->post(),"companyId  = ".$this->input->post('edit_id'), 'edit_id');

        $this->session->set_flashdata('flash_msg_yes', 'Record has been Updated');

        redirect('Companies');

    }

    //----------- Delete function

    public function delete()
    {
        if($this->uri->segment(3)){

            $this->Base_model->delete('companies',"companyId  = ".$this->uri->segment(3));

            $this->session->set_flashdata('flash_msg_yes', 'Record has been deleted');

            redirect('Companies');
        }
    }

    //----------- Delete Bulk function

    public function delete_bulk()
    {
        if(count($_POST['table_records'])> 0){
        foreach($_POST['table_records'] as $id):

            $this->Base_model->delete('companies',"companyId  = ".$id);

        endforeach;

        $this->session->set_flashdata('flash_msg_yes', 'Record has been deleted');

        redirect('Companies');
    }
    else{
        $this->session->set_flashdata('flash_msg_no', 'No Record selected!');

        redirect('Companies');
    }
}
}