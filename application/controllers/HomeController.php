<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

    public $order_path = 'orders';

    private $_applicant_model;

    private $_login_model;

    public function __construct()
    {
        parent::__construct();
        
        $this->load->database();

        $this->load->helper(array('url','form','cookie'));

        $this->load->library(array('user_agent','session','encryption'));
        
        $this->load->model(array('Applicant_model','Login_model','Base_model'));
       
        $this->_applicant_model = new Applicant_model();

        $this->_login_model = new Login_model();

    }


    // Login view function
	public function index()
	{
        if(!$this->session->userdata('user_id')){
        $this->session->set_userdata(array('logged_in'=>false));

        $this->load->view('login');
    }else{
        redirect('HomeController/dashboard');
    }

    }

      

    //check login
    public function is_login()
    {
        if($this->_login_model->login() == true)
        {
            redirect('HomeController/dashboard');
        }
        else
        {
            $this->session->set_flashdata('flash_msg_no', 'Invalid username or passeord');

            redirect('admin/login');
        }
    }


    public function application_list()
    {
        if($this->session->userdata('user_id')){
            $data['record'] =$this->_applicant_model->getAll();
            $data['categories']=$this->Base_model->getAll('categories');

            $this->load->view('applications/applicationsList',$data);     
        }else{
            redirect('admin/login');
        }
    }

  

    public function getFilterApplicationsList()
    {
        if($this->session->userdata('user_id')){

            $data['record']=$this->Base_model->getAllDataByDesc('job_applications','',"categoryId = ".$this->input->post('categoryId'));

            $this->load->view('applications/filters_applications',$data);

        }else{    
             redirect('admin/login');
        }
    }

    public function getAllapplications()
    {
        if($this->session->userdata('user_id')){

            $data['record']=$this->Base_model->getAll('job_applications');

            $this->load->view('applications/filters_applications',$data);

        }else{    
             redirect('admin/login');
        }
    }
    
    //applications view 
    public function dashboard()
    {
        
        if($this->session->userdata('user_id')){

        $this->application_list();

            }else{
                
            redirect('admin/login');
        }
    }

    public function profileview()
    {
        if($this->session->userdata('user_id')){
         
            $this->load->view('profile');
        
        }else{        
        
            redirect('admin/login');
        }

    }
     //profile view view 
     public function updateProfile()
     {  
        if($this->session->userdata('user_id')){         
        if($this->input->post('password') === $this->input->post('confirmPassword'))
        {
            $data = array(
                'password'=>md5($this->input->post('password'))
            );

            $this->Base_model->update('user',$data,'user_id ='.$this->session->userdata('user_id'));
            $this->session->set_flashdata('flash_msg_yes', 'Password changed successfully!');
            redirect('HomeController/profileView');
        }
        else
        {
            $this->session->set_flashdata('flash_msg_no', 'New password did not match with Confirm Password!'); 
            redirect('HomeController/profileView');
            
        }
         
        }else{        
        
            redirect('admin/login');
        }
     }


        public function delete()
        {
            if($this->session->userdata('user_id')){

            $applicant_id = $this->uri->segment(3);
                  
                    //geting image name 
            $applicant_resume = $this->Base_model->getAll('job_applications',$this->input->post(),"applicant_id  = ".$this->uri->segment(3));
            /**********Application log */
            $data = array(
                'applicant_id' =>  $this->uri->segment(3),
                'application_status' =>'deleted',
                'status_changed_by'=>$this->session->userdata('user_id'),
            );

            $this->Base_model->insert('applications_logs',$data);
            /**********Application log */

            unlink("assets/job_applications/". $applicant_resume[0]['resume_path']);


            $this->_applicant_model->delete($applicant_id);
            
            $this->session->set_flashdata('flash_msg_yes', 'Application has been Deleted successfully');  
            
            redirect('HomeController/dashboard');

            }else{

            redirect('admin/login');

            }

        }


        // Status functions
        public function status_pending()
        {
            if($this->session->userdata('user_id')){   
            $this->_applicant_model->status_called($this->uri->segment(3),'pending');
             
            $this->session->set_flashdata('flash_msg_yes', 'Application status has been changed successfully');  

           redirect('HomeController/dashboard');
            
            }else{        
            
                redirect('admin/login');
            }
        }

        public function status_called()
        {
            if($this->session->userdata('user_id')){ 

             /**********Application log */
             $data = array(
                'applicant_id' => $this->uri->segment(3),
                'application_status' =>'called',
                'status_changed_by'=>$this->session->userdata('user_id'),
            );

            $this->Base_model->insert('applications_logs',$data);
            /**********Application log */

             $this->_applicant_model->status_called($this->uri->segment(3),'called');
             
             $this->session->set_flashdata('flash_msg_yes', 'Application status has been changed successfully');  

            redirect('HomeController/dashboard');
            }else{        
                
                redirect('admin/login');
            }
        }


        
        public function status_linksent()
        {    
            if($this->session->userdata('user_id')){   
            
            $applicant_id = $this->input->post('applicant_id');
          $data = array(
            'applicant_form_hashing'=> md5($applicant_id)
          );
            
            $this->Base_model->update('job_applications',$data,"applicant_id  = ".$this->input->post('applicant_id'), 'edit_id');
            if($this->input->post('form_type')==='standard_form'){

                    $applicant_id = $this->input->post('applicant_id');
                    $this->_applicant_model->status_called($applicant_id,'mailsent');
                    $this->_applicant_model->save_form_type($applicant_id,$this->input->post('form_type'));
                    
                    // Sending Email
                    //Send Application confirmation email
                    $applicationData = $this->_applicant_model->getApplicantEmail($applicant_id);

                    /**********Application log */
                    $data = array(
                        'applicant_id' => $this->input->post('applicant_id'),
                        'application_status' =>'mailsent',
                        'status_changed_by'=>$this->session->userdata('user_id'),
                    );
        
                    $this->Base_model->insert('applications_logs',$data);
                    /**********Application log */
                    
                    $to =@$applicationData[0]['applicant_email'];
                    $subject = "Stage 2 - Job Application Process";

                    $message = '<HTML>
                    <BODY style="font-size: 10pt; font-family: Verdana, sans-serif;">
                    Dear Applicant, <br><br>As per our conversation, please see below for the link to complete the DBS form, where <br> 
                    necessary providing the relevant documentation.<br><br><br>
                    Any questions, please do not hesitate to contact us.<br><br><br>
                     Congratulations for passing onto the next stage of the recruitment process!! <br><br><br>
                     Complete the questions <a href="'.base_url().'/remaining-application/?details='.md5($this->input->post('applicant_id')).'">here</a> <br>
                    <br><br><br>
                     Kind Regards <br>
                     Rocket Jobs
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                          <table>
                          <tbody>
                          <tr>
                            <td style="font-size: 10pt; font-family: Verdana, sans-serif; border-right: 1px solid; border-right-color: #808080; width:180px; padding-right: 10px; vertical-align: top;" valign="top">
                              <img src="'.base_url('assets/img/logo/emailLogo.png').'" alt="photograph" width="116" border="0" style="border:0; height:auto; width:116px"/>
                              <p style="margin-top:30px; margin-bottom:0; line-height:16px">
                              <strong><span style="font-size: 12pt; font-family: Verdana, sans-serif; color:#262626;">RocketJobs <br></span></strong>
                              <span style="font-family: Verdana, sans-serif; font-size:9pt; color:#808080;">HR/Admin Department</span>
                              </p>
                              <p style="margin-top:30px; margin-bottom:0; line-height:16px">    
                            </td>

                            <td style="font-size: 10pt; color:#444444; font-family: Verdana, sans-serif; padding-bottom: 10px; padding-left: 20px; vertical-align: top; line-height:14px" valign="top">
                              <span style="color: #808080;"><strong>M</strong></span><span style="font-size: 9pt; font-family: Verdana, sans-serif; color:#808080;"> 02080883260<br></span>
                              <span style="color: #808080;"><strong>E</strong></span><span style="font-size: 9pt; font-family: Verdana, sans-serif; color:#808080;"> info@rocketjob.com <br></span>
                              <span  style="font-size: 10pt; font-family: Verdana, sans-serif; color: #808080;">Ground Floor, The Bower, Stockley Park, <br> 4 Roundwood Ave, London UB11 1AF</span> <br>
                              <a href="https://rocketjobs.co.uk/" target="_blank" rel="noopener" style="text-decoration:none;"><strong style="font-size: 9pt; font-family: Verdana, sans-serif; color: #262626;">RocketJobs</strong></a>
                            </td>
                          </tr>
                        </tbody>
                      
                      </table>
                  
                      <tr>
                        <td>
                          <p style="width:550px; font-size:9px; line-height:11px; COLOR: #808080; FONT-FAMILY: Verdana, sans-serif; text-align:justify">The content of this email is confidential and intended for the recipient specified in message only. It is strictly forbidden to share any part of this message with any third party, without a written consent of the sender. If you received this message by mistake, please reply to this message and follow with its deletion, so that we can ensure such a mistake does not occur in the future.</p>
                        </td>
                      </tr>
                      <tr>
                      </body>
                  </html>';

                    // Always set content-type when sending HTML email
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                    // More headers
                    $headers .= 'From: Rocketjobs<info@rocketjobs.co.uk>' . "\r\n";
                    // $headers .= 'Cc: myboss@example.com' . "\r\n";

                    mail($to,$subject,$message,$headers);



                    $this->session->set_flashdata('flash_msg_yes', 'Application status has been changed successfully');  
                     redirect('HomeController/dashboard');

            }else if($this->input->post('form_type')==='enhanced_form'){
                
                $applicant_id = $this->input->post('applicant_id');
                $this->_applicant_model->status_called($applicant_id,'mailsent');
                $this->_applicant_model->save_form_type($applicant_id,$this->input->post('form_type'));
                
                // Sending Email
                //Send Application confirmation email
                $applicationData = $this->_applicant_model->getApplicantEmail($applicant_id);
                
                /**********Application log */
                $data = array(
                    'applicant_id' => $this->input->post('applicant_id'),
                    'application_status' =>'mailsent',
                    'status_changed_by'=>$this->session->userdata('user_id'),
                );
    
                $this->Base_model->insert('applications_logs',$data);
                /**********Application log */


                $to =@$applicationData[0]['applicant_email'];
                $subject = "Stage 2 - Job Application Process";

                $message = '<HTML>
                <BODY style="font-size: 10pt; font-family: Verdana, sans-serif;">
                Dear Applicant, <br><br>As per our conversation, please see below for the link to complete the DBS form, where <br> 
                necessary providing the relevant documentation.<br><br><br>
                Any questions, please do not hesitate to contact us.<br><br><br>
                 Congratulations for passing onto the next stage of the recruitment process!! <br><br><br>
                 Complete the questions <a href="'.base_url().'/remaining-application/?details='.md5($this->input->post('applicant_id')).'">here</a> <br>
                <br><br><br>
                 Kind Regards <br>
                 Rocket Jobs
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                      <table>
                      <tbody>
                      <tr>
                        <td style="font-size: 10pt; font-family: Verdana, sans-serif; border-right: 1px solid; border-right-color: #808080; width:180px; padding-right: 10px; vertical-align: top;" valign="top">
                          <img src="'.base_url('assets/img/logo/emailLogo.png').'" alt="photograph" width="116" border="0" style="border:0; height:auto; width:116px"/>
                          <p style="margin-top:30px; margin-bottom:0; line-height:16px">
                          <strong><span style="font-size: 12pt; font-family: Verdana, sans-serif; color:#262626;">RocketJobs <br></span></strong>
                          <span style="font-family: Verdana, sans-serif; font-size:9pt; color:#808080;">HR/Admin Department</span>
                          </p>
                          <p style="margin-top:30px; margin-bottom:0; line-height:16px">    
                        </td>
              
                        <td style="font-size: 10pt; color:#444444; font-family: Verdana, sans-serif; padding-bottom: 10px; padding-left: 20px; vertical-align: top; line-height:14px" valign="top">
                          <span style="color: #808080;"><strong>M</strong></span><span style="font-size: 9pt; font-family: Verdana, sans-serif; color:#808080;"> 02080883260<br></span>
                          <span style="color: #808080;"><strong>E</strong></span><span style="font-size: 9pt; font-family: Verdana, sans-serif; color:#808080;"> info@rocketjob.com <br></span>
                          <span  style="font-size: 10pt; font-family: Verdana, sans-serif; color: #808080;">Ground Floor, The Bower, Stockley Park, <br> 4 Roundwood Ave, London UB11 1AF</span> <br>
                          <a href="https://rocketjobs.co.uk/" target="_blank" rel="noopener" style="text-decoration:none;"><strong style="font-size: 9pt; font-family: Verdana, sans-serif; color: #262626;">RocketJobs</strong></a>
                        </td>
                      </tr>
                    </tbody>
                  
                  </table>
              
                  <tr>
                    <td>
                      <p style="width:550px; font-size:9px; line-height:11px; COLOR: #808080; FONT-FAMILY: Verdana, sans-serif; text-align:justify">The content of this email is confidential and intended for the recipient specified in message only. It is strictly forbidden to share any part of this message with any third party, without a written consent of the sender. If you received this message by mistake, please reply to this message and follow with its deletion, so that we can ensure such a mistake does not occur in the future.</p>
                    </td>
                  </tr>
                  <tr>
                  </body>
              </html>';

                // Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                // More headers
                $headers .= 'From: Rocketjobs<info@rocketjobs.co.uk>' . "\r\n";
                // $headers .= 'Cc: myboss@example.com' . "\r\n";

                mail($to,$subject,$message,$headers);

                $this->session->set_flashdata('flash_msg_yes', 'Application status has been changed successfully');  
                 redirect('HomeController/dashboard');
            }else{
                echo 'Please choose form';exit;
            }

            }else{        
                    
                redirect('admin/login');
            }
            
        }

 

        public function status_done()
        {
            if($this->session->userdata('user_id')){   
            $this->_applicant_model->status_called($this->uri->segment(3),'done');
                /**********Application log */
                $data = array(
                    'applicant_id' => $this->uri->segment(3),
                    'application_status' =>'done',
                    'status_changed_by'=>$this->session->userdata('user_id'),
                );
    
                $this->Base_model->insert('applications_logs',$data);
                /**********Application log */


            $this->session->set_flashdata('flash_msg_yes', 'Application status has been changed successfully');  

            redirect('HomeController/dashboard');

            }else{        
                    
                redirect('admin/login');
            }
        }

        public function status_rejected()
        {

            if($this->session->userdata('user_id')){   

            $this->_applicant_model->status_called($this->uri->segment(3),'rejected');

                /**********Application log */
                $data = array(
                    'applicant_id' => $this->uri->segment(3),
                    'application_status' =>'rejected',
                    'status_changed_by'=>$this->session->userdata('user_id'),
                );
    
                $this->Base_model->insert('applications_logs',$data);
                /**********Application log */
            
            $this->session->set_flashdata('flash_msg_yes', 'Application status has been changed successfully');  
            
            redirect('HomeController/dashboard');

            }else{        
                        
                redirect('admin/login');
            }

        }

        //
        public function application_formDetails($applicant_id)
        {
            if($this->session->userdata('user_id')){   
            $data['record']=$this->Base_model->getAll('job_applications','',"applicant_id = ".$applicant_id);

            $this->load->view('applications/formDetails',$data);
            }else{        
                            
                redirect('admin/login');
            }
        }

     
        public function set_user_cookie()
        {
            $cookie_name = "rocketjobportal";
            $cookie_value = "user data";
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
        }

        public function logout()
        {
            $this->session->sess_destroy();
            
            redirect('admin/login');
        }

}
