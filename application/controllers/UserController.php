<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {

    public $title = "Manage Users";

    public $path = "";

    public function __construct()
    {
        parent::__construct();

        $this->load->database();

        $this->load->helper(array('url','form'));

        $this->load->library(array('session' , 'user_agent'));

        $this->load->model(array('Base_model'));

        if(!$this->session->userdata('user_id')){
            redirect('admin/login');
        }
        
    }

    //------------ record list function

    public function index()
    {
      if($this->session->userdata('user_role')==='admin'){

        $data['record']=$this->Base_model->getAll('user');

        $this->load->view('settings/manage_users',$data);
      }else{
          redirect('HomeController/Dashboard');
      }

    }

    //----------- Set new data function

    public function set_data()
    {
        
        $data = array(
            'name' => $this->input->post('name'),
            'username' => $this->input->post('username'),
            'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password')),
            'user_role' => $this->input->post('user_role'), 
            'status' => 1
        );


        $this->Base_model->insert('user',$data);

        $this->session->set_flashdata('flash_msg_yes', 'Record has been Added');

        redirect('UserController');
    }

    public function changeUserPassword($id){
        if($this->session->userdata('user_role')==='admin'){
       
            $this->load->view('settings/change_password', $id);
        }else{
            redirect('HomeController/Dashboard');
        }
    }

    public function updateUserPassword(){
        {      
            if($this->session->userdata('user_id')){    

            if($this->input->post('password') === $this->input->post('confirmPassword'))
                {
                    $data = array(
                        'password'=>md5($this->input->post('password'))
                    );
                    $this->Base_model->update('user',$data,'user_id ='.$this->input->post('user_id'));

                    $this->session->set_flashdata('flash_msg_yes', 'Password changed successfully!');

                    redirect('HomeController/profileView');
                
                }else{

                    $this->session->set_flashdata('flash_msg_no', 'New password did not match with Confirm Password!'); 

                    redirect('HomeController/profileView');
                }
                
                }else{        
                    redirect('admin/login');
                }
            }
            
        }
        
        // Block user
        public function block()
        {
            if($this->session->userdata('user_role')==='admin'){
            
                if($this->uri->segment(3)){
                    $data = array(
                        'status'=> 0
                    );
                    $this->Base_model->update('user',$data,'user_id ='.$this->uri->segment(3));
                    $this->session->set_flashdata('flash_msg_yes', 'User blocked successfully!'); 
                    redirect('UserController');
                }else{
                    $this->session->set_flashdata('flash_msg_no', 'User blocking Failed!'); 
                    redirect('UserController');
                }

            }else{
                redirect('HomeController/Dashboard');
            }
            

        }

        // UnBlock user
        public function unblock()
        {
            if($this->session->userdata('user_role')==='admin'){
                if($this->uri->segment(3)){
                    $data = array(
                        'status'=> 1
                    );
                    $this->Base_model->update('user',$data,'user_id ='.$this->uri->segment(3));
                    $this->session->set_flashdata('flash_msg_yes', 'User unblocked successfully!'); 
                    redirect('UserController');
                }else{
                    $this->session->set_flashdata('flash_msg_no', 'User unblocking Failed!'); 

                }

            }else{
                redirect('HomeController/Dashboard');
            }
            
        }

}