<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LogsController extends CI_Controller {

    public $title = "Application Logs";

    public $path = "";

    public function __construct()
    {
        parent::__construct();

        $this->load->database();

        $this->load->helper(array('url','form'));

        $this->load->library(array('session' , 'user_agent'));

        $this->load->model(array('Base_model'));

        if(!$this->session->userdata('user_id')){
            redirect('admin/login');
        }

     
    }

    //------------ Logs Details function

    public function index()
    {
        if($this->session->userdata('user_role')==='admin'){

            $data['record']=$this->Base_model->getAll('applications_logs');

            $data['applicant_list']=$this->Base_model->getAll('job_applications');

            $this->load->view('logs/logs_details',$data);

        }else{

            redirect('HomeController/dashboard');
        }

    }

    public function getFilterApplicationsLogs()
    {
        if($this->session->userdata('user_id')){

            $data['record']=$this->Base_model->getAll('applications_logs','',"applicant_id = ".$this->input->post('applicantId'));

            $this->load->view('logs/filter_logs_details',$data);

        }else{    
             redirect('admin/login');
        }
    }

    public function getAlllogs()
    {
        if($this->session->userdata('user_id')){

            $data['record']=$this->Base_model->getAll('applications_logs');

            $this->load->view('logs/filter_logs_details',$data);

        }else{    
             redirect('admin/login');
        }
    }

    
}

    ?>