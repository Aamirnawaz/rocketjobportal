<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Applicant_model extends CI_Model {

    function __construct()
    {
        parent::__construct();

    }

    function save_applicant($data)
    {
        $this->db->insert('job_applications', $data);
          if($this->db->affected_rows() > 0){
            return TRUE;
          }else{
            return FALSE;
          }
    }

    function getAll()
    {
      $this->db->select('applicant_id,first_name,applicant_email,categoryId,resume_path,status_changed_by,application_status');
      $this->db->from('job_applications');
      $this->db->order_by('applicant_id','DESC');
      $query = $this->db->get();
      return $query->result_array();
    }

    function delete($id)
    {
        $this->db->where('applicant_id',$id);
        
        $this->db->delete('job_applications');

    }

    function getJobLocations()
    {
      $this->db->select('job_id,job_location');

      $this->db->from('job_details');
      
      $this->db->group_by('job_location');

      $query = $this->db->get();

      return $query->result_array();
    }

    function featuered_jobs()
    {
      $this->db->select('*');

      $this->db->from('job_details');

      $this->db->order_by('job_salary','DESC');

      $this->db->limit(5);

      $query = $this->db->get();
      
      return $query->result_array();
      
    }
   
    function user_search_jobs($jobTitle,$jobLocation)
    {
      $this->db->select('*');

      $this->db->from('job_details');

      $this->db->like('job_title', $jobTitle, 'both'); 
      $this->db->like('job_location', $jobLocation, 'both'); 

      $query = $this->db->get();

      return $query->result_array();
    }

    // changing status functions
    function status_called($id,$value)
    {
      $this->db->set('application_status', $value);
      $this->db->set('status_changed_by', $this->session->userdata('user_id'));
      $this->db->where('applicant_id',$id);

      $this->db->update('job_applications');
    }

    function getApplicantEmail($applicant_id)
    {
      $this->db->select('applicant_id,applicant_email');

      $this->db->from('job_applications');

      $this->db->where('applicant_id',$applicant_id);

      $query = $this->db->get();
    
      return $query->result_array();
    }

    function save_form_type($id,$value)
    {
      $this->db->set('application_formType', $value);
      $this->db->where('applicant_id',$id);

      $this->db->update('job_applications');
    }

    
    // function get_categories_jobs()
    // {
    //   $this->db->select('count(job_categoryId) as totalJobs, job_categoryId');
    //   $this->db->from('job_details');
    //   $this->db->group_by('job_categoryId');
    //   $this->db->limit('8');

    //   $query = $this->db->get();
    //     return $query->result_array();
    // }

    function getCategoryWiseJobs($categoryId)
    {
      $this->db->select('*');

      $this->db->from('job_details');

      $this->db->where('job_categoryId',$categoryId);

      $query = $this->db->get();

      return $query->result_array();
    }

    function filter_jobs($categoryId,$job_location)
    {
      $this->db->select('*');

      $this->db->from('job_details');

      
      $this->db->like('job_location', $job_location, 'both'); 
      $this->db->like('job_categoryId', $categoryId); 

      $query = $this->db->get();

      return $query->result_array();
    }

    // public function applicantEmail()
    // {
    //   $this->db->select('applicant_email');
    //   $this->db->from('job_applications');
    //   $this->db->where('applicant_id');
    // }


    function getHasing($applicantHashing)
    {
      $this->db->select('applicant_id,applicant_form_hashing,application_formType');
      $this->db->from('job_applications');
      $this->db->where('applicant_form_hashing',$applicantHashing);
      $query = $this->db->get();
      return $query->result_array();
    }

}