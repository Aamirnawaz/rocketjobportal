<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base_model extends CI_Model {

    // Get All Records
    function getAll($tableName, $columnName='', $condition='', $toString='')
    {
            if (!$columnName) {
            $this->db->select('*');
        }else{
            $this->db->select($columnName);
        }

        $this->db->from($tableName);

        if ($condition){
            $this->db->where($condition);
        }

        $query = $this->db->get();

        if ($toString){
            $this->getQuery();
        }

        return $query->result_array();
    }

    function insert($tableName, $data, $toString='')
    {
        foreach ($data as $key=>$val) {
            $this->db->set($key,$val);
        }

        $this->db->insert($tableName);

        if ($toString){
            $this->getQuery();
        }

        return $this->db->insert_id();
    }

    // Update Records
    function update($tableName, $data, $condition, $excludedField='', $toString='')
    {
        foreach ($data as $key=>$val) {
            if ($key != $excludedField) {
                $this->db->set($key, $val);
            }
        }

        $this->db->where($condition);

        $this->db->update($tableName);

        if ($toString){
            $this->getQuery();
        }
    }

    // Delete Records
    function delete($tableName, $condition)
    {
        $this->db->where($condition);

        $this->db->delete($tableName);
    }

    // Return query as a string
    function getQuery()
    {
        die($this->db->last_query());
    }
	
	// Truncate Table
    function truncate($tableName)
    {
        $this->db->truncate($tableName);
    }

    // Check if there is a file
    function hasFile($fieldName)
    {

    }


    //update if already exists e;lse insert
    function insert_or_update($post_array)
    {
        $params_id = $post_array['params_id'];

        $this->db->where('params_id', $params_id);

        $q = $this->db->get('lab_tbl_test_parameters_values');

        $this->db->reset_query();

        if ( $q->num_rows() > 0 )
        {
           $this->db->where('params_id', $params_id)->update('lab_tbl_test_parameters_values', $post_array);

        } else {

            $this->db->set('params_id', $params_id)->insert('lab_tbl_test_parameters_values', $post_array);
        }

    }

    function getAllDataByDesc($tableName, $columnName='', $condition='', $toString='')
    {
      if (!$columnName) {
        $this->db->select('*');
        $this->db->order_by('applicant_id','DESC');
    }else{
        $this->db->select($columnName);
    }
    $this->db->from($tableName);
    if ($condition){
        $this->db->where($condition);
    }
    $query = $this->db->get();
    if ($toString){
        $this->getQuery();
    }
    return $query->result_array();
    }


}