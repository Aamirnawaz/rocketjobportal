<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Functions{

        // Resize img function

        public function ImageResize($srcImgPath, $dstImgPath, $resize_width, $resize_height)
        {

            $ext = explode('.', $srcImgPath);
            $ext = end($ext);



            if (stristr('jpeg|JPEG|jpg|jpg|jpg|JPG', $ext ) == true)


            {


                list($width_orig, $height_orig) = getimagesize($srcImgPath);


                $image_p = imagecreatetruecolor($resize_width, $resize_height);


                $image_n = imagecreatefromjpeg($srcImgPath);


                imagecopyresampled($image_p, $image_n, 0, 0, 0, 0, $resize_width, $resize_height, $width_orig, $height_orig);


                $resultResize = imagejpeg($image_p, $dstImgPath, 100);


            }


            elseif (stristr('png|PNG', $ext ) == true)


            {


                list($width_orig, $height_orig) = getimagesize($srcImgPath);


                $image_p = imagecreatetruecolor($resize_width, $resize_height);


                $image_n = imagecreatefrompng($srcImgPath);


                imagecopyresampled($image_p, $image_n, 0, 0, 0, 0, $resize_width, $resize_height, $width_orig, $height_orig);


                //$resultResize = imagepng($image_p, $dstImgPath, 100);


                $resultResize = imagejpeg($image_p, $dstImgPath, 100);


            }


            elseif (stristr('gif|GIF', $ext ) == true)


            {


                list($width_orig, $height_orig) = getimagesize($srcImgPath);


                $image_p = imagecreatetruecolor($resize_width, $resize_height);


                $image_n = imagecreatefromgif($srcImgPath);


                imagecopyresampled($image_p, $image_n, 0, 0, 0, 0, $resize_width, $resize_height, $width_orig, $height_orig);


                $resultResize = imagegif($image_p, $dstImgPath, 100);


            }





        }
}
