-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2020 at 03:43 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rocketjobportal_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `applications_logs`
--

CREATE TABLE `applications_logs` (
  `logs_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `application_status` varchar(11) NOT NULL,
  `status_changed_by` int(11) NOT NULL,
  `logs_time` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `applications_logs`
--

INSERT INTO `applications_logs` (`logs_id`, `applicant_id`, `application_status`, `status_changed_by`, `logs_time`) VALUES
(1, 1, 'called', 1, '2020-11-26 19:31:47'),
(2, 1, 'mailsent', 1, '2020-11-26 19:31:57'),
(3, 1, 'rejected', 1, '2020-11-26 19:33:19'),
(4, 2, 'called', 5, '2020-11-26 19:34:04'),
(5, 2, 'mailsent', 1, '2020-11-26 19:34:54'),
(6, 2, 'done', 1, '2020-11-26 19:35:36');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `categoryId` int(11) NOT NULL,
  `categoryName` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categoryId`, `categoryName`, `created_at`) VALUES
(1, 'Health care', '2020-11-26 06:50:16'),
(2, 'Cleaning and house keeping', '2020-11-26 06:50:32'),
(3, 'Multidrop delivery drivers', '2020-11-26 06:50:39'),
(4, 'Construction', '2020-11-26 06:50:46'),
(5, 'Warehouse operatives', '2020-11-26 06:50:54'),
(6, 'Security management', '2020-11-26 06:51:00');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `companyId` int(11) NOT NULL,
  `company_title` varchar(50) NOT NULL,
  `company_desc` text NOT NULL,
  `company_email` varchar(50) NOT NULL,
  `company_weblink` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`companyId`, `company_title`, `company_desc`, `company_email`, `company_weblink`) VALUES
(1, 'Rocket job', '<p>Rocket jobs is a job provider company, we provide a variety of jobs management services such as office cleaning services, maintenance and support services to businesses, Security management, Locums jobs closely working with NHS contractors and suppliers all over London. We have delivered high quality services in this Pandemic covid-19 time, therefore you can trust us.</p>\r\n', 'info@rocketjobs.co.uk/', 'https://rocketjobs.co.uk/');

-- --------------------------------------------------------

--
-- Table structure for table `job_applications`
--

CREATE TABLE `job_applications` (
  `applicant_id` int(11) NOT NULL,
  `application_formType` varchar(30) NOT NULL,
  `application_status` varchar(11) NOT NULL,
  `status_changed_by` int(11) NOT NULL,
  `title` varchar(5) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `first_middle_name` varchar(30) NOT NULL,
  `second_middle_name` varchar(30) NOT NULL,
  `third_middle_name` varchar(30) NOT NULL,
  `present_sur_name` varchar(30) NOT NULL,
  `surname_at_birth` varchar(30) NOT NULL,
  `usedUntil_surname` varchar(30) NOT NULL,
  `applicant_email` varchar(70) NOT NULL,
  `dob` date NOT NULL,
  `nationality` varchar(30) NOT NULL,
  `dual_nationality` varchar(5) NOT NULL,
  `birth_province` varchar(30) NOT NULL,
  `birth_country` varchar(30) NOT NULL,
  `birth_town` varchar(30) NOT NULL,
  `birth_state` varchar(50) NOT NULL,
  `applicant_contact` int(40) NOT NULL,
  `gender` varchar(8) NOT NULL,
  `mobile_contact` varchar(20) NOT NULL,
  `telephone_number` int(11) NOT NULL,
  `hasNationalityChanged` varchar(5) NOT NULL,
  `hasSurnameChanged` varchar(5) NOT NULL,
  `hasOtherSurName` varchar(30) NOT NULL,
  `knownByOtherName` varchar(5) NOT NULL,
  `anyConvictions` varchar(5) NOT NULL,
  `DBS_profile_no` varchar(5) NOT NULL,
  `view_DBS_certificate` varchar(5) NOT NULL,
  `Ni_number` varchar(5) NOT NULL,
  `passportNo` varchar(50) NOT NULL,
  `passport_issue_country` varchar(30) NOT NULL,
  `DOB_on_passport` date NOT NULL,
  `passport_nationality` varchar(30) NOT NULL,
  `passport_issue_date` date NOT NULL,
  `uk_insurance_no` varchar(30) NOT NULL,
  `uk_insurance_avaliable` varchar(5) NOT NULL,
  `hasValidPassport` varchar(5) NOT NULL,
  `DrivingLicenceNo` varchar(50) NOT NULL,
  `hasValidLicence` varchar(5) NOT NULL,
  `uk_driver_licence_no` varchar(50) NOT NULL,
  `DOB_on_driving_licence` date NOT NULL,
  `licence_type` varchar(30) NOT NULL,
  `licence_valid_from` date NOT NULL,
  `licence_issue_country` varchar(30) NOT NULL,
  `concent_thirdparty_email` varchar(50) NOT NULL,
  `request_paper_certificate` varchar(30) NOT NULL,
  `howManyAddress` int(11) NOT NULL,
  `currentAddress` text NOT NULL,
  `buildingName` varchar(25) NOT NULL,
  `streetName` text NOT NULL,
  `town` varchar(50) NOT NULL,
  `country` varchar(25) NOT NULL,
  `start_living_date` date NOT NULL,
  `postcode` varchar(15) NOT NULL,
  `dateMoveIn` datetime NOT NULL,
  `isEmailSent` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `resume_path` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `job_applications`
--

INSERT INTO `job_applications` (`applicant_id`, `application_formType`, `application_status`, `status_changed_by`, `title`, `first_name`, `middle_name`, `first_middle_name`, `second_middle_name`, `third_middle_name`, `present_sur_name`, `surname_at_birth`, `usedUntil_surname`, `applicant_email`, `dob`, `nationality`, `dual_nationality`, `birth_province`, `birth_country`, `birth_town`, `birth_state`, `applicant_contact`, `gender`, `mobile_contact`, `telephone_number`, `hasNationalityChanged`, `hasSurnameChanged`, `hasOtherSurName`, `knownByOtherName`, `anyConvictions`, `DBS_profile_no`, `view_DBS_certificate`, `Ni_number`, `passportNo`, `passport_issue_country`, `DOB_on_passport`, `passport_nationality`, `passport_issue_date`, `uk_insurance_no`, `uk_insurance_avaliable`, `hasValidPassport`, `DrivingLicenceNo`, `hasValidLicence`, `uk_driver_licence_no`, `DOB_on_driving_licence`, `licence_type`, `licence_valid_from`, `licence_issue_country`, `concent_thirdparty_email`, `request_paper_certificate`, `howManyAddress`, `currentAddress`, `buildingName`, `streetName`, `town`, `country`, `start_living_date`, `postcode`, `dateMoveIn`, `isEmailSent`, `job_id`, `categoryId`, `resume_path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'enhanced_form', 'rejected', 1, 'Mr', 'Suliman Munawar khan', 'nawaz', 'suliman', '', 'yousfzai', 'Solomon', '', '2020', 'sullaimaan@gmail.com', '2020-11-02', '', '', '', 'Canada', 'ISLAMABAD', 'Canada', 2147483647, 'male', '+923411027477', 2147483647, '', '', 'yes', 'yes', 'no', '', '', '', '4312234234', '', '2020-11-10', 'pakistan', '2020-11-12', '12345678', '', 'no', '12312312313', 'no', '', '2020-11-11', 'LTV', '2020-11-26', 'Canada', '', '', 0, 'islamabad', '', 'Rose Lane No 1 Khan Hostel New lalazar Rawalpindi', 'ISLAMABAD', 'Pakistan', '0000-00-00', '14000', '0000-00-00 00:00:00', 0, 1, 1, 'tahir_0.342072001606222237.pdf', '2020-11-24 12:50:37', NULL, '2020-11-24 12:50:37'),
(2, 'standard_form', 'done', 1, 'Mr', 'Suliman Munawar khan', 'khan', 'Suliman', 'munawar', 'khan', 'Suliman', '', '', 'sullaimaan@gmail.com', '2020-11-02', '', '', '', 'USA', 'ISLAMABAD', '', 2147483647, 'male', '+923411027477', 0, '', '', '', '', '', 'yes', 'yes', '', '4312234234', '', '0000-00-00', '', '0000-00-00', '', 'yes', 'yes', '12312312313', 'yes', '', '0000-00-00', '', '0000-00-00', 'Pakistan', 'sullaimaan@gmail.com', 'yes', 0, 'islamabad', '', 'Rose Lane No 1 Khan Hostel New lalazar Rawalpindi', 'ISLAMABAD', 'France', '0000-00-00', '14000', '0000-00-00 00:00:00', 0, 2, 4, 'Deliver a set number of parcels_0.888591001606223830.docx', '2020-11-24 13:17:10', NULL, '2020-11-24 13:17:10');

-- --------------------------------------------------------

--
-- Table structure for table `job_details`
--

CREATE TABLE `job_details` (
  `job_id` int(11) NOT NULL,
  `job_title` varchar(20) NOT NULL,
  `job_nature` varchar(25) NOT NULL,
  `job_categoryId` int(11) NOT NULL,
  `companyId` int(11) NOT NULL,
  `job_location` varchar(50) NOT NULL,
  `job_salary` int(30) NOT NULL,
  `job_salary_range` varchar(50) NOT NULL,
  `no_of_vacancies` int(11) NOT NULL,
  `job_description` text NOT NULL,
  `job_requirements` text NOT NULL,
  `job_education` varchar(60) NOT NULL,
  `job_experience` varchar(60) NOT NULL,
  `companyLogo` varchar(50) NOT NULL,
  `posted_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `application_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `job_details`
--

INSERT INTO `job_details` (`job_id`, `job_title`, `job_nature`, `job_categoryId`, `companyId`, `job_location`, `job_salary`, `job_salary_range`, `no_of_vacancies`, `job_description`, `job_requirements`, `job_education`, `job_experience`, `companyLogo`, `posted_date`, `application_date`) VALUES
(1, 'software  enginearin', 'part_time', 1, 1, 'Lahore,pakistan', 50000, '6500 - 7000', 0, '<p>Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,</p>\r\n', '<ul>\r\n	<li>Must have atlease 4 year of experience.</li>\r\n	<li>Have handson experience with Php and with its framwork.</li>\r\n	<li>Also having aware of frontend tools like js, jquery,react.</li>\r\n	<li>Angular will be plus.</li>\r\n</ul>\r\n', 'Bachelor of computer sceience', '4 year experience', 'companyLogo_3564897_1605283247.jpg', '2020-11-13 16:00:47', '2020-11-27 00:00:00'),
(2, 'Php Developer', 'part_time', 1, 1, 'Islamabad,pakistan', 870000, '6500 - 7000', 4, '<p>Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,</p>\r\n', '<ul>\r\n	<li>Must have atlease 4 year of experience.</li>\r\n	<li>Have handson experience with Php and with its framwork.</li>\r\n	<li>Also having aware of frontend tools like js, jquery,react.</li>\r\n	<li>Angular will be plus.</li>\r\n</ul>\r\n', 'Bachelor of computer sceience', '4 year experience', 'companyLogo_3564897_1605283247.jpg', '2020-11-13 16:00:47', '2020-11-27 00:00:00'),
(3, 'Php Developer', 'part_time', 2, 1, 'Islamabad,pakistan', 810000, '6500 - 7000', 4, '<p>Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,</p>\r\n', '<ul>\r\n	<li>Must have atlease 4 year of experience.</li>\r\n	<li>Have handson experience with Php and with its framwork.</li>\r\n	<li>Also having aware of frontend tools like js, jquery,react.</li>\r\n	<li>Angular will be plus.</li>\r\n</ul>\r\n', 'Bachelor of computer sceience', '4 year experience', 'companyLogo_3564897_1605283247.jpg', '2020-11-13 16:00:47', '2020-11-27 00:00:00'),
(4, 'software  enginearin', 'part_time', 1, 1, 'Lahore,pakistan', 60000, '6500 - 7000', 0, '<p>Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,</p>\r\n', '<ul>\r\n	<li>Must have atlease 4 year of experience.</li>\r\n	<li>Have handson experience with Php and with its framwork.</li>\r\n	<li>Also having aware of frontend tools like js, jquery,react.</li>\r\n	<li>Angular will be plus.</li>\r\n</ul>\r\n', 'Bachelor of computer sceience', '4 year experience', 'companyLogo_3564897_1605283247.jpg', '2020-11-13 16:00:47', '2020-11-27 00:00:00'),
(5, 'software  enginearin', 'part_time', 3, 1, 'Lahore,pakistan', 70000, '6500 - 7000', 0, '<p>Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,</p>\r\n', '<ul>\r\n	<li>Must have atlease 4 year of experience.</li>\r\n	<li>Have handson experience with Php and with its framwork.</li>\r\n	<li>Also having aware of frontend tools like js, jquery,react.</li>\r\n	<li>Angular will be plus.</li>\r\n</ul>\r\n', 'Bachelor of computer sceience', '4 year experience', 'companyLogo_3564897_1605283247.jpg', '2020-11-13 16:00:47', '2020-11-27 00:00:00'),
(6, 'software  enginearin', 'part_time', 1, 1, 'Lahore,pakistan', 75000, '6500 - 7000', 0, '<p>Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,Complete details description will be enterd here,</p>\r\n', '<ul>\r\n	<li>Must have atlease 4 year of experience.</li>\r\n	<li>Have handson experience with Php and with its framwork.</li>\r\n	<li>Also having aware of frontend tools like js, jquery,react.</li>\r\n	<li>Angular will be plus.</li>\r\n</ul>\r\n', 'Bachelor of computer sceience', '4 year experience', 'companyLogo_3564897_1605283247.jpg', '2020-11-13 16:00:47', '2020-11-27 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `user_role` enum('admin','operator') NOT NULL,
  `username` varchar(60) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `is_deleted` int(1) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `name`, `user_role`, `username`, `email`, `password`, `is_deleted`, `status`) VALUES
(1, 'admin', 'admin', 'admin', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 0, 1),
(5, 'sulman', 'operator', 'sulman', 'sulman@gmail.com', 'bded3af0f22f09228681f2d331a4c33b', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applications_logs`
--
ALTER TABLE `applications_logs`
  ADD PRIMARY KEY (`logs_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`companyId`);

--
-- Indexes for table `job_applications`
--
ALTER TABLE `job_applications`
  ADD PRIMARY KEY (`applicant_id`);

--
-- Indexes for table `job_details`
--
ALTER TABLE `job_details`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applications_logs`
--
ALTER TABLE `applications_logs`
  MODIFY `logs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `companyId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `job_applications`
--
ALTER TABLE `job_applications`
  MODIFY `applicant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `job_details`
--
ALTER TABLE `job_details`
  MODIFY `job_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
